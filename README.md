#OpenPortalServer开源Portal协议WEB认证服务器 SSH版本
作者：LeeSon
QQ:25901875
E-Mail:LeeSon@vip.qq.com
OpenPortal官方交流群 119688084

该软件是基于华为AC/BAS PORTAL协议的服务端程序，Java编写，开源。
最新源代码下载地址：https://github.com/lishuocool https://git.oschina.net/SoftLeeSon/

支持Huawei H3C Portal V1 V2协议PAP CHAP认证方式的Portal服务器

OpenPortalServer V3.1.0 2015-8-8

更新：

1.采用Struts2+Spring+Hibernate+Ehcache+jxl+ajax+Json 构架；

2.修复众多内部BUG，提高整体性能；

4.加入核心portal引擎，json接口；ui_XXXX.html范例

新手安装配置说明：

windows环境下：

1.首先保证已有JDK1.7环境，MySQL环境 ,tomcat7

2.解压路径无中文及空格

3.配置文件说明 \webapps\ROOT\WEB-INF\classes\jdbc.properties 首先修改该数据库配置文件 创建openportalserver数据库 UTF-8字符集 导入数据库文件OpenPortalServer.sql 后台账号：admin 密码:admin

4.配置AC设备 安装和配置Radius服务 如果使用AC模拟器进行模拟测试则可忽略这步 如果使用页面展示、本地接入用户认证方式 不用配置radius

5.运行 bin/startup.bat 快捷方式

6.浏览器http://服务器IP

7.如果使用AC模拟器测试用户名密码随意 如果真实环境（不用我废话了）

Linux环境: 安装jdk1.7 mysql tomcat7 将解压目录下的webapps目录替换

Portal认证服务核心引擎接口路径为 根目录的 html文件 采用GET提交，Json信息返回！

对接配置说明：

超时设置 3-5秒 日志记录 是否输出详细日志到文件 验证码设置 是否开启用户登陆的验证码 用户心跳 是否进行用户离线检测 超时重复次数 一次检查周期内 用户在线检测超时几次算已经下线 计费检测周期 间隔多长时间检测一次用户是否在线，余额是否够 认证方式 页面展示,本地接入用户,外接radius 自助注册开关 是否允许自助注册接入用户，默认每个新用户给10分钟的时长 设备账号 对应设备的local-user用户账号密码，在本地接入用户和页面展示 认证方式时必须配置，而且设备用默认domain 设备密码 对应设备的local-user用户账号密码，在本地接入用户和页面展示 认证方式时必须配置，而且设备用默认domain