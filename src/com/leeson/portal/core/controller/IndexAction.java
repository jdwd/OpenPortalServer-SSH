package com.leeson.portal.core.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.leeson.portal.core.model.Config;
import com.leeson.portal.core.model.OnlineMap;
import com.leeson.portal.core.service.InterfaceControl;
import com.leeson.portal.core.utils.SpringContextHelper;
import com.leeson.portal.manage.domain.LogRecord;
import com.leeson.portal.manage.service.LogRecordService;

/**
 * 登录判断
 * 
 * @author LeeSon QQ:25901875
 */
public class IndexAction extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1966047929923869408L;
	
	ApplicationContext ac =SpringContextHelper.getApplicationContext();

	public Config config = Config.getInstance();

	Logger logger = Logger.getLogger(IndexAction.class);

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		/**
		 * 本地用户
		 */
		if(Integer.parseInt(config.getAuth_interface())==1){
			HttpSession session = request.getSession();
			session.setAttribute("VerifyCode", config.getVerifyCode());
			request.getRequestDispatcher("/Login").forward(request,
					response);
			return;
			
			/**
			 * 第三方radius
			 */
		}else if(Integer.parseInt(config.getAuth_interface())==2){
			HttpSession session = request.getSession();
			session.setAttribute("VerifyCode", config.getVerifyCode());
			request.getRequestDispatcher("/Login").forward(request,
					response);
			return;
			
			/**
			 * APP接口
			 */
		}else if(Integer.parseInt(config.getAuth_interface())==3){
			request.getRequestDispatcher("/WEB-INF/jsp/indexAction/app.jsp").forward(request,
					response);
			return;
			
			/**
			 * 短信接口
			 */
		}else if(Integer.parseInt(config.getAuth_interface())==4){
			request.getRequestDispatcher("/WEB-INF/jsp/indexAction/sms.jsp").forward(request,
					response);
			return;
			
			/**
			 * 微信接口
			 */
		}else if(Integer.parseInt(config.getAuth_interface())==5){
			request.getRequestDispatcher("/WEB-INF/jsp/indexAction/weixin.jsp").forward(request,
					response);
			return;
			
			/**
			 * 
			 */
		}else {
			String ip=request.getRemoteAddr();// 获取客户端的ip
			Boolean info = InterfaceControl.Method("PORTAL_LOGIN", config.getBas_user(),
					config.getBas_pwd(), ip);
			request.setAttribute("ip", ip);
			if (info == true) {
				OnlineMap onlineMap=OnlineMap.getInstance();
				String[] loginInfo=new String[3];
				loginInfo[0]=config.getBas_user();
				onlineMap.getOnlineUserMap().put(ip,loginInfo);
				
				
				LogRecord logRecord=new LogRecord();
				Date nowDate=new Date();
				logRecord.setInfo("IP:"+ip+",用户：【展示】,登录成功!");
				logRecord.setRec_date(nowDate);
				LogRecordService logRecordService=(LogRecordService) ac.getBean("logRecordServiceImpl");
				logRecordService.save(logRecord);
				
				request.setAttribute("msg", "认证成功！");
				
			} else {
				request.setAttribute("msg", "认证失败！");
			}
			request.getRequestDispatcher("/WEB-INF/jsp/indexAction/show.jsp").forward(request,
					response);
			return;
		}
		

	}

	
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		/*
		 * 校验验证码 1. 从session中获取正确的验证码 2. 从表单中获取用户填写的验证码 3. 进行比较！ 4.
		 * 如果相同，向下运行，否则保存错误信息到request域，转发到login.jsp
		 */
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		
		
		
		
		
		
	}

}
