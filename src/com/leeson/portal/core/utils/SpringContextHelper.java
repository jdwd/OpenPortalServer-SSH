package com.leeson.portal.core.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * * ApplicationContext的帮助类 自动装载ApplicationContext
 * 
 */
public class SpringContextHelper implements ApplicationContextAware {
	private static ApplicationContext context;

	/** 注入ApplicationContext */
	@Override
	public void setApplicationContext(ApplicationContext context)
			throws BeansException {
		// 在加载Spring时自动获得context
		SpringContextHelper.context = context; // 此处若获取不到context的值，请查看包含<bean
											// id="springContext"
											// class="com.shine.spring.SpringContextHelper"></bean>
											// 配置的spring配置文件中<beans>中的配置

		// 若default-lazy-init="true"，则删除此属性即可，为不影响性能，只修改当前所用到配置文件即可

		System.out.println(SpringContextHelper.context);
	}
	
	public static ApplicationContext getApplicationContext(){
		return context;
	}

	public static Object getBean(String beanName) {
		return context.getBean(beanName);
	}
}