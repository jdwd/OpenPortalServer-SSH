package com.leeson.portal.core.service.load_test;


import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

import com.leeson.portal.core.service.utils.Authenticator;
import com.leeson.portal.core.service.utils.PortalUtil;

/**
 * 压力测试入口
 * 
 * @author LeeSon QQ:25901875
 * 
 */
public class TestREQ {
	
	private static Logger log = Logger.getLogger(TestREQ.class);
	public static void main(String[] args) {
		String Bas_IP="192.168.2.100";
		int bas_PORT=2000;
		int timeout_Sec=3;
		String ip="192.168.2.100";
		byte[] SerialNo = PortalUtil.SerialNo();// 报文序列号
		byte[] UserIP = new byte[4];
		String[] ips = ip.split("[.]");
		// 将ip地址加入字段UserIP
		for (int i = 0; i < 4; i++) {
			int m = NumberUtils.toInt(ips[i]);
			byte b = (byte) m;
			UserIP[i] = b;
		}
		String sharedSecret="iwsiqh";
		challenge(Bas_IP, bas_PORT, timeout_Sec, SerialNo, UserIP, sharedSecret);

	}

	public static void challenge(String Bas_IP, int bas_PORT,
			int timeout_Sec, byte[] SerialNo, byte[] UserIP, String sharedSecret) {
		DatagramSocket dataSocket = null;// 创建连接
		byte[] Req_Challenge = new byte[32];// 创建Req_Challenge包
		byte[] BBuff = new byte[16];
		byte[] Attrs = new byte[0];
		Req_Challenge[0] = (byte) 2;
		Req_Challenge[1] = (byte) 9;
		Req_Challenge[2] = (byte) 0;
		Req_Challenge[3] = (byte) 0;
		Req_Challenge[4] = SerialNo[0];
		Req_Challenge[5] = SerialNo[1];
		Req_Challenge[6] = (byte) 0;
		Req_Challenge[7] = (byte) 0;
		Req_Challenge[8] = UserIP[0];
		Req_Challenge[9] = UserIP[1];
		Req_Challenge[10] = UserIP[2];
		Req_Challenge[11] = UserIP[3];
		Req_Challenge[12] = (byte) 0;
		Req_Challenge[13] = (byte) 0;
		Req_Challenge[14] = (byte) 0;
		Req_Challenge[15] = (byte) 0;
		for (int i = 0; i < 16; i++) {
			BBuff[i] = Req_Challenge[i];
		}
		byte[] Authen = Authenticator.MK_Authen(BBuff, Attrs, sharedSecret);
		for (int i = 0; i < 16; i++) {
			Req_Challenge[16 + i] = Authen[i];
		}
		log.info("REQ " + PortalUtil.Getbyte2HexString(Req_Challenge));
		
			try {
				dataSocket = new DatagramSocket();
				DatagramPacket requestPacket = new DatagramPacket(Req_Challenge,
						32, InetAddress.getByName(Bas_IP), bas_PORT);// 创建发送数据包并发送给服务器
				dataSocket.send(requestPacket);
				dataSocket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
		
	}
	
}
