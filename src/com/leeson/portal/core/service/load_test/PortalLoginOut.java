package com.leeson.portal.core.service.load_test;

import com.leeson.portal.core.service.InterfaceControl;

/**
 * 压力测试调用
 * 
 * @author LeeSon QQ:25901875
 * 
 */
public class PortalLoginOut extends Thread {

	public void run() {

		InterfaceControl.Method("PORTAL_LOGINOUT", "leeson", "123456",
				"192.168.0.100");

	}

	public static void openServer() {

		new PortalLoginOut().start();

	}

}
