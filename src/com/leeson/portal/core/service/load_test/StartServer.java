package com.leeson.portal.core.service.load_test;

import org.apache.log4j.Logger;

import com.leeson.portal.core.controller.Login;
import com.leeson.portal.core.model.Config;

/**
 * 压力测试入口
 * 
 * @author LeeSon QQ:25901875
 * 
 */
public class StartServer {
	static Logger logger = Logger.getLogger(Login.class);

	public static void main(String[] args) {
		// TODO Auto-generated mewhile(true){
		Config cfg = Config.getInstance();
		cfg.setBas_ip("192.168.2.1000");
		cfg.setBas_port("2000");
		cfg.setPortal_port("50100");
		cfg.setSharedSecret("LeeSon");
		cfg.setAuthType("CHAP");
		cfg.setTimeoutSec("3");
		cfg.setPortalVer("2");

		// 设置调用次数
		int count = 1;

		LoadTest(count);

	}

	private static void LoadTest(int count) {
		for (int i = 0; i < count; i++) {
			new Thread() {
				public void run() {
					try {
						PortalLogin.openServer();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						logger.error("创建线程出错....", e);
					}
				};

			}.start();

			new Thread() {
				public void run() {
					try {
						PortalLoginOut.openServer();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						logger.error("创建线程出错....", e);
					}
				};

			}.start();
		}
	}

}
