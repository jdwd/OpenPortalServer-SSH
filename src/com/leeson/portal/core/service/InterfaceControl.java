package com.leeson.portal.core.service;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

import com.leeson.portal.core.model.Config;
import com.leeson.portal.core.model.PortalConst;
import com.leeson.portal.core.service.action.ReqInfo;
import com.leeson.portal.core.service.action.v1.chap.Chap_Auth_V1;
import com.leeson.portal.core.service.action.v1.chap.Chap_Challenge_V1;
import com.leeson.portal.core.service.action.v1.chap.Chap_Quit_V1;
import com.leeson.portal.core.service.action.v1.pap.PAP_Auth_V1;
import com.leeson.portal.core.service.action.v1.pap.PAP_Quit_V1;
import com.leeson.portal.core.service.action.v2.chap.Chap_Auth_V2;
import com.leeson.portal.core.service.action.v2.chap.Chap_Challenge_V2;
import com.leeson.portal.core.service.action.v2.chap.Chap_Quit_V2;
import com.leeson.portal.core.service.action.v2.pap.PAP_Auth_V2;
import com.leeson.portal.core.service.action.v2.pap.PAP_Quit_V2;
import com.leeson.portal.core.service.utils.PortalUtil;

/**
 * 供外部调用接口
 * 
 * @author LeeSon QQ:25901875
 * 
 */
public class InterfaceControl {

	private static Logger log = Logger.getLogger(InterfaceControl.class);

	private static Config config = Config.getInstance();
	/**
	 * basIp AC通信IP地址 basPort AC socket端口号 sharedSecret BAS和Portal
	 * Server之间的共享密钥secret 相当于签名 authType 定义认证方式 有PAP/CHAP两种 timeoutSec 定义请求超时时间
	 * 单位秒 portalVer portal协议版本 目前有1.0和2.0
	 */
	public static String basIp = config.getBas_ip();
	public static String bas = config.getBas();
	// final static int basPort = NumberUtils.toInt((String)
	// config.getBas_port());
	public static int basPort = Integer.parseInt(config.getBas_port());
	public static String sharedSecret = config.getSharedSecret();
	public static String authType = config.getAuthType();
	// final static int timeoutSec = NumberUtils.toInt((String)
	// config.getTimeoutSec());
	public static int timeoutSec = Integer.parseInt(config.getTimeoutSec());
	// final static int portalVer = NumberUtils.toInt((String)
	// config.getPortalVer());
	public static int portalVer = Integer.parseInt(config.getPortalVer());
	public static int debug = Integer.parseInt(config.getDebug());

	public static Boolean Method(String Action, String userName,
			String passWord, String ip) {
		
		short SerialNo_Short = (short) (1 + Math.random() * 32767);
		
//		if(debug==1){
//			SerialNo_Short = (short)1;
//			}
		
		byte[] SerialNo = PortalUtil.SerialNo(SerialNo_Short);// 报文序列号
		byte[] UserIP = new byte[4];
		String[] ips = ip.split("[.]");
		// 将ip地址加入字段UserIP
		for (int i = 0; i < 4; i++) {
			int m = NumberUtils.toInt(ips[i]);
			byte b = (byte) m;
			UserIP[i] = b;
		}

		// PAP=============================================================
		if (authType.equals(PortalConst.PAP) && portalVer == 1) {// V1 PAP
			if(config.getDebug().equals("1")){
				log.info("使用Portal V1协议，PAP认证方式！！");
			}
			
			if (Action.equals(PortalConst.PORTAL_LOGIN)) {
				if(bas.equals(PortalConst.H3C)){
					if(config.getDebug().equals("1")){
						log.info("H3C设备！！");
					}
					
					// 创建Ack_info_V2包
					byte[] Ack_info = ReqInfo.reqInfo(basIp, basPort,timeoutSec, SerialNo, UserIP, sharedSecret);
					// 如果出错直接返回错误信息
					if (Ack_info.length == 1) {
						return false;
					}
				}
				return PAP_Auth_V1.auth(basIp, basPort, timeoutSec, userName,
						passWord, SerialNo, UserIP);
			} else {
				return PAP_Quit_V1.quit(0, basIp, basPort, timeoutSec,
						SerialNo, UserIP);
			}
		} else if (authType.equals(PortalConst.PAP) && portalVer == 2) {// V2
																		// PAP
			if(config.getDebug().equals("1")){
				log.info("使用Portal V2协议，PAP认证方式！！");
			}
			
			if (Action.equals(PortalConst.PORTAL_LOGIN)) {
				if(bas.equals(PortalConst.H3C)){
					if(config.getDebug().equals("1")){
						log.info("H3C设备！！");
					}
					
					// 创建Ack_info_V2包
					byte[] Ack_info = ReqInfo.reqInfo(basIp, basPort,timeoutSec, SerialNo, UserIP, sharedSecret);
					// 如果出错直接返回错误信息
					if (Ack_info.length == 1) {
						return false;
					}
				}
				return PAP_Auth_V2.auth(basIp, basPort, timeoutSec, userName,
						passWord, SerialNo, UserIP, sharedSecret);
			} else {
				return PAP_Quit_V2.quit(0, basIp, basPort, timeoutSec,
						SerialNo, UserIP, sharedSecret);
			}
		}

		// CHAP=============================================================
		else if (authType.equals(PortalConst.CHAP) && portalVer == 2) {// V2
																		// CHAP
			
				return Portal_V2(Action, userName, passWord, basIp, basPort,
						timeoutSec, SerialNo, UserIP, sharedSecret, SerialNo_Short, ip);
			
			
		} else if (authType.equals(PortalConst.CHAP) && portalVer == 1) {// V1
			
				return Portal_V1(Action, userName, passWord, basIp, basPort,
						timeoutSec, SerialNo, UserIP);
			
			
		} else {
			if(config.getDebug().equals("1")){
				log.info("参数错误,认证方式或版本号错误!!!");
			}
			
			return false;
		}
	}

	
	private static boolean Portal_V2(String Action, String userName,
			String passWord, String basIp, int basPort, int timeoutSec,
			byte[] SerialNo, byte[] UserIP, String sharedSecret ,short SerialNo_Short,String ip) {
		if(config.getDebug().equals("1")){
			log.info("使用Portal V2协议，Chap认证方式！！");
		}
		
		byte[] ReqID = new byte[2];
		if (Action.equals(PortalConst.PORTAL_LOGIN)) {
			
			if(bas.equals(PortalConst.H3C)){
				if(config.getDebug().equals("1")){
					log.info("H3C设备！！");
				}
				
				// 创建Ack_info_V2包
//				byte[] Ack_info = PacketSendReceiveTest.reqInfo(basIp, basPort, timeoutSec, SerialNo_Short, ip, sharedSecret);
				byte[] Ack_info = ReqInfo.reqInfo(basIp, basPort,timeoutSec, SerialNo, UserIP, sharedSecret);
				// 如果出错直接返回错误信息
				if (Ack_info.length == 1) {
					return false;
				}
			}
			
						
						
						
			byte[] Challenge = new byte[16];
			
			// 创建Ack_Challenge_V2包
			byte[] Ack_Challenge_V2 = Chap_Challenge_V2.challenge(basIp,
					basPort, timeoutSec, SerialNo, UserIP, sharedSecret);
			// 如果出错直接返回错误信息
			if (Ack_Challenge_V2.length == 1) {
				Chap_Quit_V2.quit(1, basIp, basPort, timeoutSec, SerialNo,
						UserIP, ReqID, sharedSecret);// 发送超时回复报文
				return false;
			} else {
				ReqID[0] = Ack_Challenge_V2[6];
				ReqID[1] = Ack_Challenge_V2[7];
				for (int i = 0; i < 16; i++) {
					Challenge[i] = Ack_Challenge_V2[34 + i];
				}
				if(config.getDebug().equals("1")){
					log.info("获得Challenge："
							+ PortalUtil.Getbyte2HexString(Challenge));
				}
				
				// 创建Ack_Challenge_V2包
				byte[] Ack_Auth_V2 = Chap_Auth_V2.auth(basIp, basPort,
						timeoutSec, userName, passWord, SerialNo, UserIP,
						ReqID, Challenge, sharedSecret);
				if ((int) (Ack_Auth_V2[0] & 0xFF) != 20
						&& (int) (Ack_Auth_V2[0] & 0xFF) != 22) {
					Chap_Quit_V2.quit(2, basIp, basPort, timeoutSec, SerialNo,
							UserIP, ReqID, sharedSecret);// 发送超时回复报文
					return false;
				} else {
					return true;
				}
			}
		} else {
			return Chap_Quit_V2.quit(0, basIp, basPort, timeoutSec, SerialNo,
					UserIP, ReqID, sharedSecret);
		}
	}

	private static boolean Portal_V1(String Action, String userName,
			String passWord, String basIp, int basPort, int timeoutSec,
			byte[] SerialNo, byte[] UserIP) {
		if(config.getDebug().equals("1")){
			log.info("使用Portal V1协议，Chap认证方式！！");
		}
		
		byte[] ReqID = new byte[2];
		if (Action.equals(PortalConst.PORTAL_LOGIN)) {
			
			if(bas.equals(PortalConst.H3C)){
				if(config.getDebug().equals("1")){
					log.info("H3C设备！！");
				}
				
				// 创建Ack_info_V2包
				byte[] Ack_info = ReqInfo.reqInfo(basIp, basPort,timeoutSec, SerialNo, UserIP, sharedSecret);
				// 如果出错直接返回错误信息
				if (Ack_info.length == 1) {
					return false;
				}
			}
			
			byte[] Challenge = new byte[16];
			// 创建Ack_Challenge_V1包
			byte[] Ack_Challenge_V1 = Chap_Challenge_V1.Action(basIp, basPort,
					timeoutSec, SerialNo, UserIP);
			// 如果出错直接返回错误信息
			if (Ack_Challenge_V1.length == 1) {
				Chap_Quit_V1.quit(1, basIp, basPort, timeoutSec, SerialNo,
						UserIP, ReqID);// 发送超时回复报文
				return false;
			} else {
				ReqID[0] = Ack_Challenge_V1[6];
				ReqID[1] = Ack_Challenge_V1[7];
				for (int i = 0; i < 16; i++) {
					Challenge[i] = Ack_Challenge_V1[18 + i];
				}
				if(config.getDebug().equals("1")){
					log.info("获得Challenge："
							+ PortalUtil.Getbyte2HexString(Challenge));
				}
				
				// 创建Ack_Challenge_V1包
				byte[] Ack_Auth_V1 = Chap_Auth_V1.auth(basIp, basPort,
						timeoutSec, userName, passWord, SerialNo, UserIP,
						ReqID, Challenge);
				if ((int) (Ack_Auth_V1[0] & 0xFF) != 20
						&& (int) (Ack_Auth_V1[0] & 0xFF) != 22) {
					Chap_Quit_V1.quit(2, basIp, basPort, timeoutSec, SerialNo,
							UserIP, ReqID);// 发送超时回复报文
					return false;
				} else {
					return true;
				}
			}
		} else {
			return Chap_Quit_V1.quit(0, basIp, basPort, timeoutSec, SerialNo,
					UserIP, ReqID);
		}
	}
}
