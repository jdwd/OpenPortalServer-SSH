package com.leeson.portal.core.model;

import java.io.Serializable;
import java.util.HashMap;

/**
 * 
 * This class is used for 单例模式构建配置信息
 * 
 * @author LeeSon QQ:25901875
 * @version 1.0, 2015年2月10日 上午12:21:15
 */
public class OnlineMap implements Serializable{
	


	private static final long serialVersionUID = 5220960469752563771L;
	
	
	private HashMap<String, String[]> OnlineUserMap=new HashMap<String, String[]>();

	private static OnlineMap instance = new OnlineMap();

	private OnlineMap() {
	}

	public static OnlineMap getInstance() {
		return instance;
	}

	public static void setInstance(OnlineMap instance) {
		OnlineMap.instance = instance;
	}

	public HashMap<String, String[]> getOnlineUserMap() {
		return OnlineUserMap;
	}

	public void setOnlineUserMap(HashMap<String, String[]> onlineUserMap) {
		OnlineUserMap = onlineUserMap;
	}
	
	
	
	
	

}
