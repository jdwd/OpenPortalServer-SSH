package com.leeson.portal.core.model;

import java.io.Serializable;

/**
 * 
 * This class is used for 单例模式构建配置信息
 * 
 * @author LeeSon QQ:25901875
 * @version 1.0, 2015年2月10日 上午12:21:15
 */
public class Config implements Serializable{
	
	private static final long serialVersionUID = -7834052616912881869L;
	
	private Long id;
	private String bas_ip;
	private String bas_port;
	private String portalVer;
	private String authType;
	private String timeoutSec;
	private String sharedSecret;
	private String portal_port;
	private String bas;
	private String debug;
	private String verifyCode;
	private String userHeart;
	private String userHeartCount;
	private String userHeartTime;
	private String auth_interface;
	private String bas_user;
	private String bas_pwd;
    private String accountAdd;
	
	private static Config instance = new Config();

	private Config() {
	}

	public static Config getInstance() {
		return instance;
	}

	public static void setInstance(Config instance) {
		Config.instance = instance;
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getBas_ip() {
		return bas_ip;
	}

	public void setBas_ip(String bas_ip) {
		this.bas_ip = bas_ip;
	}

	public String getBas_port() {
		return bas_port;
	}

	public void setBas_port(String bas_port) {
		this.bas_port = bas_port;
	}

	public String getPortalVer() {
		return portalVer;
	}

	public void setPortalVer(String portalVer) {
		this.portalVer = portalVer;
	}

	public String getAuthType() {
		return authType;
	}

	public void setAuthType(String authType) {
		this.authType = authType;
	}

	public String getTimeoutSec() {
		return timeoutSec;
	}

	public void setTimeoutSec(String timeoutSec) {
		this.timeoutSec = timeoutSec;
	}

	public String getSharedSecret() {
		return sharedSecret;
	}

	public void setSharedSecret(String sharedSecret) {
		this.sharedSecret = sharedSecret;
	}

	public String getPortal_port() {
		return portal_port;
	}

	public void setPortal_port(String portal_port) {
		this.portal_port = portal_port;
	}

	public String getBas() {
		return bas;
	}

	public void setBas(String bas) {
		this.bas = bas;
	}

	public String getDebug() {
		return debug;
	}

	public void setDebug(String debug) {
		this.debug = debug;
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

	public String getAuth_interface() {
		return auth_interface;
	}

	public void setAuth_interface(String auth_interface) {
		this.auth_interface = auth_interface;
	}
	
	

	public String getUserHeart() {
		return userHeart;
	}

	public void setUserHeart(String userHeart) {
		this.userHeart = userHeart;
	}

	public String getUserHeartCount() {
		return userHeartCount;
	}

	public void setUserHeartCount(String userHeartCount) {
		this.userHeartCount = userHeartCount;
	}

	public String getUserHeartTime() {
		return userHeartTime;
	}

	public void setUserHeartTime(String userHeartTime) {
		this.userHeartTime = userHeartTime;
	}

	public String getBas_user() {
		return bas_user;
	}

	public void setBas_user(String bas_user) {
		this.bas_user = bas_user;
	}

	public String getBas_pwd() {
		return bas_pwd;
	}

	public void setBas_pwd(String bas_pwd) {
		this.bas_pwd = bas_pwd;
	}

	public String getAccountAdd() {
		return accountAdd;
	}

	public void setAccountAdd(String accountAdd) {
		this.accountAdd = accountAdd;
	}

	@Override
	public String toString() {
		return "Config [id=" + id + ", bas_ip=" + bas_ip + ", bas_port="
				+ bas_port + ", portalVer=" + portalVer + ", authType="
				+ authType + ", timeoutSec=" + timeoutSec + ", sharedSecret="
				+ sharedSecret + ", portal_port=" + portal_port + ", bas="
				+ bas + ", debug=" + debug + ", verifyCode=" + verifyCode
				+ ", userHeart=" + userHeart + ", userHeartCount="
				+ userHeartCount + ", userHeartTime=" + userHeartTime
				+ ", auth_interface=" + auth_interface + ", bas_user="
				+ bas_user + ", bas_pwd=" + bas_pwd + ", accountAdd="
				+ accountAdd + "]";
	}
	
	

	

}
