/*  
 * Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
 * LeeSon  QQ:25901875
 */
package com.leeson.portal.core.listener;

import java.util.HashMap;
import java.util.Iterator;

import com.leeson.portal.core.model.Config;
import com.leeson.portal.core.model.OnlineMap;

public class CopyOfCheckOnlineQuartzTask{

	public void execute() {
		// 执行的定时器任务
		
		Config config=Config.getInstance();
		if(Integer.parseInt(config.getUserHeart())==1){
			
		System.out.println("用户心跳已开启！！！！！");
		OnlineMap onlineMap = OnlineMap.getInstance();
		HashMap<String, String[]> OnlineUserMap = onlineMap.getOnlineUserMap();
		Iterator<String> iterator = OnlineUserMap.keySet().iterator();
		while (iterator.hasNext()) {
			Object o = iterator.next();
			String host = o.toString();
			String[] loginInfo = OnlineUserMap.get(host);
			String username = loginInfo[0];
			System.out.println(host + "====" + username);

			new CheckOnline(host,username).start();
		}
		}else{
			System.out.println("用户心跳已关闭！！！！！");
		}

	}

	
	

}
