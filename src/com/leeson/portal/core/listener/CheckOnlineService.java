/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.core.listener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.leeson.portal.core.model.OnlineMap;

public class CheckOnlineService implements ServletContextListener {
	
	
	private static com.leeson.portal.core.model.Config config = com.leeson.portal.core.model.Config.getInstance();
	private static Logger log = Logger.getLogger(CheckOnlineService.class);
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		log.info("在线用户时长检测服务 关闭");
	}

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		if(config.getDebug().equals("1")){
			log.info("在线用户时长检测服务已启动：20秒后开始，每" + config.getUserHeartTime()+"秒检测一次!!");
		}
		 Runnable runnable = new Runnable() {
		      public void run() {
		        // task to run goes here
		    	 
		  			
		    	
		    			if(config.getDebug().equals("1")){
		    				log.info("开始检测用户时长！！");
		    			}
		    			OnlineMap onlineMap = OnlineMap.getInstance();
		    			HashMap<String, String[]> OnlineUserMap = onlineMap.getOnlineUserMap();
		    			Iterator<String> iterator = OnlineUserMap.keySet().iterator();
		    			while (iterator.hasNext()) {
		    				Object o = iterator.next();
		    				String host = o.toString();
		    				String[] loginInfo = OnlineUserMap.get(host);
		    				String username = loginInfo[0];
		    				if(config.getDebug().equals("1")){
			    				log.info("检测在线用户："+host + "：：：：" + username);
			    			}

		    				new CheckOnline(host,username).start();
		    			}
		    			
		    	  
		      }
		    };
		    ScheduledExecutorService service = Executors
		                    .newSingleThreadScheduledExecutor();
		    service.scheduleAtFixedRate(runnable, 20, Integer.parseInt(config.getUserHeartTime()), TimeUnit.SECONDS);
	
	
	
	


		
		
	}
	
	
	
	
	
	

}
