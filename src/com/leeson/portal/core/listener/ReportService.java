/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.core.listener;


import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.leeson.portal.manage.domain.Config;
import com.leeson.portal.core.service.ReportServer;
import com.leeson.portal.core.utils.SpringContextHelper;
import com.leeson.portal.manage.service.ConfigService;

public class ReportService implements ServletContextListener {
	
	
	private static com.leeson.portal.core.model.Config config = com.leeson.portal.core.model.Config.getInstance();
	private static Logger log = Logger.getLogger(ReportService.class);
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		log.info("OpenPortalServer 关闭");
	}

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		
		InitConfig4DB(servletContextEvent, config);
		
		new Thread() {

			public void run() {

				try {
					ReportServer.openServer();

				} catch (Exception e) {
					log.info("OpenPortalServer 服务启动失败！");
					e.printStackTrace();
				}
			};

		}.start();
	}
	
	
	
	
	private void InitConfig4DB(ServletContextEvent servletContextEvent, com.leeson.portal.core.model.Config cfg){

		// 获取容器与相关的Service对象
//		ApplicationContext ac = WebApplicationContextUtils.getWebApplicationContext(servletContextEvent.getServletContext());
				
//		ConfigService configService = (ConfigService) ac.getBean("configServiceImpl");
		
		ConfigService configService =(ConfigService) SpringContextHelper.getBean("configServiceImpl");
		
		List<Config> configs=configService.findByCondition("where id = 1");
		Config config=configs.get(0);
		cfg.setBas_ip(config.getBas_ip());
		cfg.setBas_port(config.getBas_port());
		cfg.setPortal_port(config.getPortal_port());
		cfg.setSharedSecret(config.getSharedSecret());
		cfg.setAuthType(config.getAuthType());
		cfg.setTimeoutSec(config.getTimeoutSec());
		cfg.setPortalVer(config.getPortalVer());
		cfg.setBas(config.getBas());
		cfg.setDebug(config.getIsdebug());
		cfg.setVerifyCode(config.getVerifyCode());
		cfg.setUserHeart(config.getUserHeart());
		cfg.setUserHeartCount(config.getUserHeartCount());
		cfg.setUserHeartTime(config.getUserHeartTime());
		cfg.setAuth_interface(config.getAuth_interface());
		cfg.setBas_user(config.getBas_user());
		cfg.setBas_pwd(config.getBas_pwd());
		cfg.setAccountAdd(config.getAccountAdd());

		if(cfg.getDebug().equals("1")){
			log.info("初始化参数，读取配置文件：" + config);
		}
		
	}
	
	
	
	
	
	
//	private void InitConfig(ServletContextEvent servletContextEvent, com.leeson.portal.core.model.Config cfg){
//
//		
//		String cfgPath = servletContextEvent.getServletContext().getRealPath("/"); // 获取服务器的页面绝对路径
//		log.info("读取配置文件：" + cfgPath + "config.properties");
//		FileInputStream fis = null;
//		Properties config = new Properties();
//		File file = new File(cfgPath + "config.properties");
//		try {
//			fis = new FileInputStream(file);
//		} catch (FileNotFoundException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//			log.info("config.properties 配置文件不存在！！");
//			servletContextEvent.getServletContext().setAttribute("msg", "config.properties 配置文件不存在！！");
//			return;
//		}
//
//		try {
//			config.load(fis);
//			
//
//			cfg.setBas_ip(config.getProperty("bas_ip"));
//			cfg.setBas_port(config.getProperty("bas_port"));
//			cfg.setPortal_port(config.getProperty("portal_port"));
//			cfg.setSharedSecret(config.getProperty("sharedSecret"));
//			cfg.setAuthType(config.getProperty("authType"));
//			cfg.setTimeoutSec(config.getProperty("timeoutSec"));
//			cfg.setPortalVer(config.getProperty("portalVer"));
//			cfg.setBas(config.getProperty("bas"));
//			cfg.setDebug(config.getProperty("debug"));
//			cfg.setVerifyCode(config.getProperty("verifyCode"));
//			
//
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			log.info("config.properties 数据库配置文件读取失败！！");
//			servletContextEvent.getServletContext().setAttribute("msg", "config.properties 数据库配置文件读取失败！！");
//			return;
//		} finally {
//			try {
//				fis.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//		
//		if(cfg.getDebug().equals("1")){
//			log.info("初始化参数，读取配置文件：" + config);
//		}
//		
//	}
	
}
