/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.base;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.transaction.annotation.Transactional;

import com.leeson.portal.manage.domain.PageBean;

@Transactional
@SuppressWarnings("unchecked")
public abstract class BaseServiceImpl<T> implements BaseService<T>,Serializable{
	
	private static final long serialVersionUID = -4608849173458983903L;

	@Resource
	protected BaseDao baseDao;
	
	private Class<T> t;

	public BaseServiceImpl() {
		// 使用反射技术得到T的真实类型
		ParameterizedType pt = (ParameterizedType) this.getClass().getGenericSuperclass(); // 获取当前new的对象的 泛型的父类 类型
		this.t = (Class<T>) pt.getActualTypeArguments()[0]; // 获取第一个类型参数的真实类型
		System.out.println("clazz ---> " + t);
	}
	
	
	@Override
	public List<T> list() {
		return (List<T>) baseDao.findAll(t);
	}

	@Override
	public void delete(Long id) {
		baseDao.delete(t,id);
	}

	@Override
	public void save(T entity) {
		baseDao.save(entity);
	}

	@Override
	public void update(T entity) {
		baseDao.update(entity);
	}

	@Override
	public T getById(Long id) {
		return baseDao.getById(t,id);
	}

	@Override
	public List<T> getByIds(Long[] Ids) {
		return baseDao.getByIds(t,Ids);
	}

	@Override
	public List<T> findByCondition(String condition) {
		return baseDao.findByCondition(t,condition);
	}
	
	@Override
	public PageBean findByConditionWithPage(int pageNum, int pageSize,
			String condition) {
		return baseDao.findByConditionWithPage(t,pageNum,pageSize,condition);
	}
	@Override
	public PageBean findByConditionWithPageDate(int pageNum, int pageSize,
			String condition, java.sql.Date sql_begin_time,
			java.sql.Date sql_end_time) {
		return baseDao.findByConditionWithPageDate(t,pageNum,pageSize,condition,sql_begin_time,sql_end_time);
	}
	@Override
	public PageBean findByConditionWithCount(String condition) {
		return baseDao.findByConditionWithCount(t,condition);
	}
	@Override
	public int findCountByCondition(String condition){
		return baseDao.findCountByCondition(t, condition);
	}

}
