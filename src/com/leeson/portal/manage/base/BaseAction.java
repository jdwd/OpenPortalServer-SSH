package com.leeson.portal.manage.base;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

import javax.annotation.Resource;

import com.leeson.portal.manage.service.AccountService;
import com.leeson.portal.manage.service.CardCategoryService;
import com.leeson.portal.manage.service.CardService;
import com.leeson.portal.manage.service.ConfigService;
import com.leeson.portal.manage.service.DepartmentService;
import com.leeson.portal.manage.service.LinkRecordService;
import com.leeson.portal.manage.service.LogRecordService;
import com.leeson.portal.manage.service.MessageService;
import com.leeson.portal.manage.service.PrivilegeService;
import com.leeson.portal.manage.service.RoleService;
import com.leeson.portal.manage.service.UserService;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("unchecked")
public abstract class BaseAction<T> extends ActionSupport implements ModelDriven<T>,Serializable{

	private static final long serialVersionUID = -449660829783506070L;
	// =============== ModelDriven的支持 ==================

	protected T model;

	public BaseAction() {
		try {
			// 通过反射获取model的真实类型
			ParameterizedType pt = (ParameterizedType) this.getClass().getGenericSuperclass();
			Class<T> clazz = (Class<T>) pt.getActualTypeArguments()[0];
			// 通过反射创建model的实例
			model = clazz.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public T getModel() {
		return model;
	}

	// =============== Service实例的声明 ==================
		@Resource
		protected RoleService roleService;
		@Resource
		protected DepartmentService departmentService;
		@Resource
		protected UserService userService;
		@Resource
		protected PrivilegeService privilegeService;
		@Resource
		protected ConfigService configService;
		@Resource
		protected AccountService accountService;
		@Resource
		protected LogRecordService logRecordService;
		@Resource
		protected MessageService messageService;
		@Resource
		protected LinkRecordService linkRecordService;
		@Resource
		protected CardCategoryService cardCategoryService;
		@Resource
		protected CardService cardService;
	

}
