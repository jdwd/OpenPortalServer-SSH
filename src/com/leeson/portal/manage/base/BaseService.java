/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.base;

import java.util.List;

import com.leeson.portal.manage.domain.PageBean;

public interface BaseService<T> {
	
	List<T> list();

	void delete(Long id);

	void save(T entity);
	
	void update(T entity);

	T getById(Long id);
	
	List<T> getByIds(Long[] Ids);

	List<T> findByCondition(String condition);
	
	PageBean findByConditionWithPage(int pageNum, int pageSize,
			String condition);
	
	PageBean findByConditionWithPageDate(int pageNum, int pageSize,
			String condition, java.sql.Date sql_begin_time,
			java.sql.Date sql_end_time);
	
	PageBean findByConditionWithCount(String condition);

	int findCountByCondition(String condition);
}
