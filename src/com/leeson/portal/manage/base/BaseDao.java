/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.base;

import java.sql.Date;
import java.util.List;

import org.hibernate.Session;

import com.leeson.portal.manage.domain.PageBean;

public interface BaseDao {
	Session getSession();
	<T> void save(T t);
	<T> void delete(Class<T> t,Long id);
	<T> void update(T t);
	<T> T getById(Class<T> t,Long id);
	<T> List<T> getByIds(Class<T> t,Long[] ids);
	<T> List<T> findAll(Class<T> t);
	<T> List<T> findByCondition(Class<T> t,String condition);
	<T> PageBean findByConditionWithPage(Class<T> t,int pageNum,int pageSize,String condition);
	<T> PageBean findByConditionWithPageDate(Class<T> t,int pageNum,int pageSize,String condition,Date beginDate,Date endDate);
	<T> PageBean findByConditionWithCount(Class<T> t,String condition);
	<T> int findCountByCondition(Class<T> t,String condition);

}
