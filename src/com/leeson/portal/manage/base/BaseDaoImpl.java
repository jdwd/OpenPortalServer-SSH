/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.base;

import java.io.Serializable;
import java.sql.Date;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.leeson.portal.manage.domain.PageBean;

@Repository
@SuppressWarnings("unchecked")
public class BaseDaoImpl implements BaseDao,Serializable {

	private static final long serialVersionUID = 841173590633065588L;
	
	@Resource
	private SessionFactory sessionFactory;
	

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
//------------------------------------------------------------------------
	
	@Override
	public <T> void save(T t) {
		getSession().save(t);
	}
	@Override
	public <T> void delete(Class<T> t, Long id) {
		Object obj = getById(t,id);
		if (obj != null) {
			getSession().delete(obj);
		}
	}
	@Override
	public <T> void update(T t) {
		getSession().update(t);
	}
	@Override
	public <T> T getById(Class<T> t, Long id) {
		if(id==null){
			return null;
		}else {
			return (T) getSession().get(t, id);
		}
	}
	@Override
	public <T> List<T> getByIds(Class<T> t, Long[] ids) {
		if (ids == null || ids.length == 0) {
			return Collections.EMPTY_LIST;
		} else {
		return getSession().createQuery(//
				"FROM "+t.getSimpleName()+" WHERE id IN (:ids)")//
				.setParameterList("ids", ids)//
				.setCacheable(true)//激活查询缓存 
				.list();
		}
	}
	@Override
	public <T> List<T> findAll(Class<T> t) {
		return getSession().createQuery(//
				"FROM " + t.getSimpleName())//
				.setCacheable(true)//激活查询缓存 
				.list();
	}
	@Override
	public <T> List<T> findByCondition(Class<T> t, String condition) {
		return getSession().createQuery(//
				"FROM " + t.getSimpleName() + " " + condition)
				.setCacheable(true)//激活查询缓存 
				.list();
	}

	@Override
	public <T> PageBean findByConditionWithPage(Class<T> t, int pageNum,
			int pageSize, String condition) {
		List<T> recordList=getSession().createQuery(//
				"FROM " + t.getSimpleName() + " " + condition)
				.setFirstResult((pageNum-1)*pageSize)
				.setMaxResults(pageSize)
				.setCacheable(true)//激活查询缓存 
				.list();
		
		Long recordCount=(Long) getSession().createQuery(//
				"SELECT COUNT(*) FROM " + t.getSimpleName() + " " + condition).setCacheable(true).uniqueResult();
		return new PageBean(pageNum, pageSize, recordCount.intValue(), recordList);
	}
	
	@Override
	public <T> PageBean findByConditionWithPageDate(Class<T> t, int pageNum,
			int pageSize, String condition, Date beginDate, Date endDate) {
		List<T> recordList=getSession().createQuery(//
				"FROM " + t.getSimpleName() + " " + condition)
				.setTimestamp("beginDate",beginDate)
				.setTimestamp("endDate",endDate)
				.setFirstResult((pageNum-1)*pageSize)
				.setMaxResults(pageSize)
				.setCacheable(true)//激活查询缓存 
				.list();
		
		Long recordCount=(Long) getSession().createQuery(//
				"SELECT COUNT(*) FROM " + t.getSimpleName() + " " + condition)
				.setTimestamp("beginDate",beginDate)
				.setTimestamp("endDate",endDate)
				.setCacheable(true)//激活查询缓存 
				.uniqueResult();
		return new PageBean(pageNum, pageSize, recordCount.intValue(), recordList);
	}

	@Override
	public <T> PageBean findByConditionWithCount(Class<T> t, String condition) {
		List<T> recordList=getSession().createQuery(//
				"FROM " + t.getSimpleName() + " " + condition)
				.setCacheable(true)//激活查询缓存 
				.list();
		Long recordCount=(Long) getSession().createQuery(//
				"SELECT COUNT(*) FROM " + t.getSimpleName() + " " + condition).setCacheable(true).uniqueResult();
		return new PageBean(1, 1, recordCount.intValue(), recordList);
	}

	@Override
	public <T> int findCountByCondition(Class<T> t, String condition) {
		Long count= (Long) getSession().createQuery(//
				"SELECT COUNT(*) FROM " + t.getSimpleName() + " " + condition).setCacheable(true).uniqueResult();
		if(count==null){
			count=(long) 0;
		}
		Integer recordCount=Integer.parseInt(String.valueOf(count));  
		return recordCount;
	}

	

}
