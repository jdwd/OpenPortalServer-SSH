/*  
 * Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
 * LeeSon  QQ:25901875
 */
package com.leeson.portal.manage.view.action;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.leeson.portal.manage.base.BaseAction;
import com.leeson.portal.manage.domain.Department;
import com.leeson.portal.manage.domain.PageBean;
import com.leeson.portal.manage.utils.DepartmentUtils;
import com.opensymphony.xwork2.ActionContext;

@Controller
@Scope("prototype")
public class DepartmentAction extends BaseAction<Department> {

	private static final long serialVersionUID = -4096068296817072648L;

	private Long parentId;
	private int pageNum=1;  //当前页
    private int pageSize=10;  //每页多少条记录
	private String[] choose;
	
	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String[] getChoose() {
		return choose;
	}

	public void setChoose(String[] choose) {
		this.choose = choose;
	}

	

	/**
	 * 列表
	 * 
	 * @return
	 */
//	public String list() {
//		List<Department> departmentList;
//		// departmentList=departmentService.list();
//		if (parentId == null) {
//			departmentList = departmentService.findTopList();
//		} else {
//			departmentList = departmentService.findChildrenList(parentId);
//			Department parent = departmentService.getById(parentId);
//			ActionContext.getContext().put("parent", parent);
//		}
//		ActionContext.getContext().put("departmentList", departmentList);
//		return "list";
//	}

	
	
	/**
	 * 列表（带分页）
	 * 
	 * @return
	 */
	public String list() {
		Department searchModel = new Department();
		StringBuffer sb = new StringBuffer("where 1=1 ");
		
		if(model.getName()!=null&&!model.getName().equals("")){
			sb.append(" and name like '%"+model.getName()+"%' ");
			searchModel.setName(model.getName());
		}
		if(model.getId()!=null&&!model.getId().equals("")){
			sb.append(" and id='"+model.getId()+"'");
			searchModel.setId(model.getId());
		}
		if(parentId !=null&&!parentId.equals("")){
			sb.append(" and parentId="+parentId+" ");
			this.setParentId(parentId);
			Department parent = departmentService.getById(parentId);
			ActionContext.getContext().put("parent", parent);
		}
		if(model.getDescription()!=null&&!model.getDescription().equals("")){
			sb.append(" and description like '%"+model.getDescription()+"%' ");
			searchModel.setDescription(model.getDescription());
		}
		
		
		PageBean pageBean=departmentService.findByConditionWithPage(pageNum,pageSize,sb.toString());
		ActionContext.getContext().getValueStack().push(pageBean);
		ActionContext.getContext().getValueStack().push(searchModel);
		
		List<Department> topList = departmentService.findTopList();
		List<Department> departmentList = DepartmentUtils
				.getAllDepartments(topList);
		ActionContext.getContext().put("departmentList", departmentList);
		
		return "list";
	}
	
	
	
	/**
	 * 删除
	 * 
	 * @return
	 */
	public String delete() {
		departmentService.delete(model.getId());
		return "toList";
	}

	
	/**
	 * 删除多选
	 * 
	 * @return
	 */
	public String deleteChoose() {
		if (choose != null && choose.length > 0) {
			for (int i = 0; i < choose.length; i++) {
				departmentService.delete(Long.parseLong(choose[i]));
			}
		}
			
		return "toList";
	}
	
	/**
	 * 添加页面
	 * 
	 * @return
	 */
	public String addUI() {
		// List<Department> departmentList=departmentService.list();
		List<Department> topList = departmentService.findTopList();
		List<Department> departmentList = DepartmentUtils
				.getAllDepartments(topList);
		ActionContext.getContext().put("departmentList", departmentList);
		return "saveUI";
	}

	/**
	 * 添加
	 * 
	 * @return
	 */
	public String add() {
		model.setParent(departmentService.getById(parentId));
		departmentService.save(model);
		return "toList";
	}

	/**
	 * 修改页面
	 * 
	 * @return
	 */
	public String editUI() {
		// List<Department> departmentList=departmentService.list();
		List<Department> topList = departmentService.findTopList();
		List<Department> departmentList = DepartmentUtils
				.getAllDepartments(topList);
		ActionContext.getContext().put("departmentList", departmentList);
		Department department = departmentService.getById(model.getId());
		ActionContext.getContext().getValueStack().push(department);
		if (department.getParent() != null) {
			parentId = department.getParent().getId();
			// ActionContext.getContext().put("parentId", parentId);
			// //因为parentId是属性（有set/get方法）已经放入栈顶，不用再添加
		}

		return "saveUI";
	}

	/**
	 * 修改
	 * 
	 * @return
	 */
	public String edit() {
		// model.setParent(departmentService.getById(parentId));
		// departmentService.update(model);
		// return "toList";

		// 1，从数据库取出原对象
		Department department = departmentService.getById(model.getId());

		// 2，设置要修改的属性
		department.setName(model.getName());
		department.setDescription(model.getDescription());
		department.setParent(departmentService.getById(parentId)); // 设置所属的上级部门

		// 3，更新到数据库中
		departmentService.update(department);

		return "toList";
	}

}
