/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.view.action;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.leeson.portal.core.model.Config;
import com.leeson.portal.core.model.OnlineMap;
import com.leeson.portal.manage.base.BaseAction;
import com.leeson.portal.manage.domain.LogRecord;
import com.leeson.portal.manage.domain.User;
import com.opensymphony.xwork2.ActionContext;

@Controller
@Scope("prototype")
public class HomeAction extends BaseAction<LogRecord> {

	private static final long serialVersionUID = -8869380195221278403L;
	
	public String top() throws Exception {
		User user = (User) ActionContext.getContext().getSession().get("user"); // 当前登录用户
		String toid=user.getId().toString();
		String state=String.valueOf(1);
		StringBuffer sb = new StringBuffer("where 1=1 ");
		sb.append(" and ( toid='"+toid+"' or fromid='"+toid+"' ) ");
		sb.append(" and state='"+state+"'");
//		PageBean pageBean=messageService.findByConditionWithCount(sb.toString());
		int msgCount=messageService.findCountByCondition(sb.toString());
		ActionContext.getContext().put("msgCount", msgCount);
		return "top";
	}
	public String left() throws Exception {
		return "left";
	}
	public String right() throws Exception {
//		List<LogRecord> temp=logRecordService.findByCondition("order by id desc");
		List<LogRecord> operationRecords=logRecordService.findByConditionWithPage(1, 5, "order by id desc").getRecordList();
//		List<LogRecord> operationRecords = new ArrayList<LogRecord>();
//		for(int i=0;i<temp.size();i++){
//			operationRecords.add(temp.get(i));
//			if(i>5){
//				break;
//			}
//		}

		
//		List<Account> accounts=accountService.list();
//		List<Account> lockaccounts=accountService.findByCondition("Where state = '0' ");
//		int lock_count=lockaccounts.size();
		int lock_count=accountService.findCountByCondition("Where state = '0' ");
//		int acc_count=accounts.size();
		int acc_count=accountService.findCountByCondition("");
		
		int online_count=0;
		
		HashMap<String, String[]> OnlineUserMap = OnlineMap.getInstance().getOnlineUserMap();
		Iterator<String> iterator = OnlineUserMap.keySet().iterator();
		while (iterator.hasNext()) {
			iterator.next();
			online_count++;
		}
		
		ActionContext.getContext().put("config", Config.getInstance().getAuth_interface());
		ActionContext.getContext().put("operationRecords", operationRecords);
		int outline_count=acc_count-online_count;
		if(outline_count<0){
			outline_count=0;
		}
		ActionContext.getContext().put("outline_count", outline_count);
		ActionContext.getContext().put("online_count", online_count);
		ActionContext.getContext().put("acc_count", acc_count);
		ActionContext.getContext().put("lock_count", lock_count);
		ActionContext.getContext().put("true_count", acc_count-lock_count);
		return "right";
	}
	public String bottom() throws Exception {
		return "bottom";
	}
	public String index() throws Exception {
		return "index";
	}

}
