/*  
 * Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
 * LeeSon  QQ:25901875
 */
package com.leeson.portal.manage.view.action;

import java.util.Date;
import java.util.List;



import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.leeson.portal.manage.base.BaseAction;
import com.leeson.portal.manage.domain.Message;
import com.leeson.portal.manage.domain.PageBean;
import com.leeson.portal.manage.domain.User;

@Controller
@Scope("prototype")
public class MessageAction extends BaseAction<Message> {

	
	private static final long serialVersionUID = 1450107947464440082L;
	private Long userId;
	private int pageNum=1;  //当前页
    private int pageSize=10;  //每页多少条记录
	private String[] choose;
	
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String[] getChoose() {
		return choose;
	}

	public void setChoose(String[] choose) {
		this.choose = choose;
	}

	/**
	 * 列表（带分页）
	 * 
	 * @return
	 */
	public String listIn() {
		User user = (User) ActionContext.getContext().getSession().get("user"); // 当前登录用户
		String toid=user.getId().toString();
		Message searchModel = new Message();
		StringBuffer sb = new StringBuffer("where 1=1 ");
		sb.append(" and  toid='"+toid+"' ");
		sb.append(" and  delin<>'1' ");
		if(model.getId()!=null&&!model.getId().equals("")){
			sb.append(" and id='"+model.getId()+"'");
			searchModel.setId(model.getId());
		}
		if(model.getState()!=null&&!model.getState().equals("")){
			sb.append(" and state='"+model.getState()+"'");
			searchModel.setState(model.getState());
		}
		if(model.getTitle()!=null&&!model.getTitle().equals("")){
			sb.append(" and title like '%"+model.getTitle()+"%' ");
			searchModel.setTitle(model.getTitle());
		}
		if(model.getDescription()!=null&&!model.getDescription().equals("")){
			sb.append(" and description like '%"+model.getDescription()+"%' ");
			searchModel.setDescription(model.getDescription());
		}
		
//		List<Account> userList = accountService.findByCondition(sb.toString());
//		ActionContext.getContext().put("userList", userList);
		
		PageBean pageBean=messageService.findByConditionWithPage(pageNum,pageSize,sb.toString());
		ActionContext.getContext().getValueStack().push(pageBean);
		ActionContext.getContext().getValueStack().push(searchModel);
		return "listIn";
	}
	/**
	 * 列表（带分页）
	 * 
	 * @return
	 */
	public String listOut() {
		User user = (User) ActionContext.getContext().getSession().get("user"); // 当前登录用户
		String toid=user.getId().toString();
		Message searchModel = new Message();
		StringBuffer sb = new StringBuffer("where 1=1 ");
		sb.append(" and fromid='"+toid+"'  ");
		sb.append(" and  delout<>'1' ");
		if(model.getId()!=null&&!model.getId().equals("")){
			sb.append(" and id='"+model.getId()+"'");
			searchModel.setId(model.getId());
		}
		if(model.getState()!=null&&!model.getState().equals("")){
			sb.append(" and state='"+model.getState()+"'");
			searchModel.setState(model.getState());
		}
		if(model.getTitle()!=null&&!model.getTitle().equals("")){
			sb.append(" and title like '%"+model.getTitle()+"%' ");
			searchModel.setTitle(model.getTitle());
		}
		if(model.getDescription()!=null&&!model.getDescription().equals("")){
			sb.append(" and description like '%"+model.getDescription()+"%' ");
			searchModel.setDescription(model.getDescription());
		}
		
//		List<Account> userList = accountService.findByCondition(sb.toString());
//		ActionContext.getContext().put("userList", userList);
		
		PageBean pageBean=messageService.findByConditionWithPage(pageNum,pageSize,sb.toString());
		ActionContext.getContext().getValueStack().push(pageBean);
		ActionContext.getContext().getValueStack().push(searchModel);
		return "listOut";
	}

	/**
	 * 删除
	 * 
	 * @return
	 */
	public String deleteIn() {
//		messageService.delete(model.getId());
		Message message = messageService.getById(model.getId());
		message.setDelin("1");
		messageService.update(message);
		return "toListIn";
	}

	/**
	 * 删除多选
	 * 
	 * @return
	 */
	public String deleteChooseIn() {
		if (choose != null && choose.length > 0) {
			for (int i = 0; i < choose.length; i++) {
//				messageService.delete(Long.parseLong(choose[i]));
				Message message = messageService.getById(Long.parseLong(choose[i]));
				message.setDelin("1");
				messageService.update(message);
			}
		}
			
		return "toListIn";
	}
	
	/**
	 * 删除
	 * 
	 * @return
	 */
	public String deleteOut() {
//		messageService.delete(model.getId());
		Message message = messageService.getById(model.getId());
		message.setDelout("1");
		messageService.update(message);
		return "toListOut";
	}

	/**
	 * 删除多选
	 * 
	 * @return
	 */
	public String deleteChooseOut() {
		if (choose != null && choose.length > 0) {
			for (int i = 0; i < choose.length; i++) {
//				messageService.delete(Long.parseLong(choose[i]));
				Message message = messageService.getById(Long.parseLong(choose[i]));
				message.setDelout("1");
				messageService.update(message);
			}
		}
			
		return "toListOut";
	}
	
	
	

	/**
	 * 添加页面
	 * 
	 * @return
	 */
	public String addUI() {
		// 准备数据, departmentList
		List<User> userList = userService.list();
		
		ActionContext.getContext().put("userList", userList);
		// 准备数据, roleList
		
		return "saveUI";
	}

	/**
	 * 添加
	 * 
	 * @return
	 */
	public String add() {
		String ip = ServletActionContext.getRequest().getRemoteAddr();// 获取客户端的ip
		User user = (User) ActionContext.getContext().getSession().get("user"); // 当前登录用户
		String fromid=user.getId().toString();
		model.setIp(ip);
		model.setFromid(fromid);
		model.setToid(userId.toString());
		model.setDate(new Date());
		model.setState("1");
		model.setFromname(user.getLoginName());
		model.setToname(userService.getById(userId).getLoginName());
		model.setDelin("0");
		model.setDelout("0");
		messageService.save(model);
		ServletActionContext.getRequest().setAttribute("msg", "消息发送成功！");
		return "toListOut";
	}

	

	

	
	
	public String editState(){
		Message message = messageService.getById(model.getId());
		if(message.getState().equals("1")){
			message.setState("0");
		}else{
			message.setState("1");
		}
		messageService.update(message);
		return "toListIn";
	}
	
	
	public String sendUI(){
		
		return "sendUI";
	}
	
	public String sendOK(){
		String ip = ServletActionContext.getRequest().getRemoteAddr();// 获取客户端的ip
		model.setIp(ip);
		model.setFromid("0");
		model.setToid("1");
		model.setDate(new Date());
		model.setState("1");
		model.setToname("admin");
		model.setFromname("游客");
		model.setDelin("0");
		model.setDelout("0");
		
		messageService.save(model);
		ServletActionContext.getRequest().setAttribute("msg", "消息发送成功！");
		return "sendOK";
	}

}
