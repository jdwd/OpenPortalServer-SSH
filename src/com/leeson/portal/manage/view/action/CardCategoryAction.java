/*  
 * Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
 * LeeSon  QQ:25901875
 */
package com.leeson.portal.manage.view.action;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.leeson.portal.manage.base.BaseAction;
import com.leeson.portal.manage.domain.CardCategory;
import com.leeson.portal.manage.domain.PageBean;

@Controller
@Scope("prototype")
public class CardCategoryAction extends BaseAction<CardCategory> {

	private static final long serialVersionUID = -4352363275480562839L;
	
	private int pageNum=1;  //当前页
    private int pageSize=10;  //每页多少条记录
	private String[] choose;
	
	
	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String[] getChoose() {
		return choose;
	}

	public void setChoose(String[] choose) {
		this.choose = choose;
	}

	/**
	 * 列表（带分页）
	 * 
	 * @return
	 */
	public String list() {
		CardCategory searchModel = new CardCategory();
		StringBuffer sb = new StringBuffer("where 1=1 ");
		if(model.getName()!=null&&!model.getName().equals("")){
			sb.append(" and name like '%"+model.getName()+"%' ");
			searchModel.setName(model.getName());
		}
		if(model.getId()!=null&&!model.getId().equals("")){
			sb.append(" and id='"+model.getId()+"'");
			searchModel.setId(model.getId());
		}
		if(model.getDescription()!=null&&!model.getDescription().equals("")){
			sb.append(" and description like '%"+model.getDescription()+"%' ");
			searchModel.setDescription(model.getDescription());
		}
		if(model.getState()!=null&&!model.getState().equals("")){
			sb.append(" and state='"+model.getState()+"'");
			searchModel.setState(model.getState());
		}
		
		PageBean pageBean=cardCategoryService.findByConditionWithPage(pageNum,pageSize,sb.toString());
		ActionContext.getContext().getValueStack().push(pageBean);
		ActionContext.getContext().getValueStack().push(searchModel);
		return "list";
	}

	/**
	 * 删除
	 * 
	 * @return
	 */
	public String delete() {
		cardCategoryService.delete(model.getId());
		return "toList";
	}

	/**
	 * 删除多选
	 * 
	 * @return
	 */
	public String deleteChoose() {
		if (choose != null && choose.length > 0) {
			for (int i = 0; i < choose.length; i++) {
				cardCategoryService.delete(Long.parseLong(choose[i]));
			}
		}
			
		return "toList";
	}
	
	
	/**
	 * 添加页面
	 * 
	 * @return
	 */
	public String addUI() {
		
		return "saveUI";
	}

	/**
	 * 添加
	 * 
	 * @return
	 */
	public String add() {
		
		// 保存到数据库
		cardCategoryService.save(model);
		return "toList";
	}

	/**
	 * 修改页面
	 * 
	 * @return
	 */
	public String editUI() {
		
		// 准备回显的数据
		CardCategory cardCategory = cardCategoryService.getById(model.getId());
		ActionContext.getContext().getValueStack().push(cardCategory);
		return "saveUI";
	}

	/**
	 * 修改
	 * 
	 * @return
	 */
	public String edit() {
		// 1，从数据库中取出原对象
		CardCategory cardCategory = cardCategoryService.getById(model.getId());
		

		// 2，设置要修改的属性
		
		cardCategory.setName(model.getName());
		cardCategory.setDescription(model.getDescription());
		cardCategory.setState(model.getState());
		cardCategory.setTime(model.getTime());
		// 3，更新到数据库
		cardCategoryService.update(cardCategory);

		return "toList";
	}

	
	
	

}
