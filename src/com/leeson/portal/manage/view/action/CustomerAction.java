/*  
 * Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
 * LeeSon  QQ:25901875
 */
package com.leeson.portal.manage.view.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.leeson.portal.core.model.OnlineMap;
import com.leeson.portal.manage.base.BaseAction;
import com.leeson.portal.manage.domain.Account;
import com.leeson.portal.manage.domain.Card;
import com.leeson.portal.manage.domain.LinkRecord;
import com.leeson.portal.manage.domain.PageBean;
import com.leeson.portal.manage.utils.MyUtils;

@Controller
@Scope("prototype")
public class CustomerAction extends BaseAction<Account> {

	private static final long serialVersionUID = -4296032514753579198L;
	private com.leeson.portal.core.model.Config config = com.leeson.portal.core.model.Config.getInstance();
	private int pageNum=1;  //当前页
    private int pageSize=10;  //每页多少条记录
	
	
	private String cardKey;
	private String linkIP;
	private String uid;
	private String payType;
	private String categoryType;
	private String query_begin_time;
	private String query_end_time;
	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"); 
	private Date begin_time;
	private Date end_time;
	java.sql.Date sql_begin_time=java.sql.Date.valueOf("1970-1-1");
	java.sql.Date sql_end_time=java.sql.Date.valueOf("9999-12-31");
	
	public String getQuery_begin_time() {
		return query_begin_time;
	}

	public void setQuery_begin_time(String query_begin_time) {
		this.query_begin_time = query_begin_time;
	}

	public String getQuery_end_time() {
		return query_end_time;
	}

	public void setQuery_end_time(String query_end_time) {
		this.query_end_time = query_end_time;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getLinkIP() {
		return linkIP;
	}

	public void setLinkIP(String linkIP) {
		this.linkIP = linkIP;
	}

	public String getCardKey() {
		return cardKey;
	}

	public void setCardKey(String cardKey) {
		this.cardKey = cardKey;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 连接记录查询页面
	 * 
	 * @return
	 */
	public String linkRecord() {
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=request.getSession();
		String username=(String) session.getAttribute("username");
		String password=(String) session.getAttribute("password");
		String ip=(String) session.getAttribute("ip");
		
		if(username==null||password==null||ip==null){
			username=(String) session.getAttribute("Cusername");
			password=(String) session.getAttribute("Cpassword");
			ip=(String) session.getAttribute("Cip");
		}
		if(username==null||password==null||ip==null){
			ActionContext.getContext().put("msg", "请先登录！");
			return "loginUI";
		}
		
		LinkRecord searchModel = new LinkRecord();
		StringBuffer sb = new StringBuffer("where 1=1 ");
		sb.append(" and uid='"+uid+"'");
		searchModel.setUid(Long.valueOf(uid));
		
		if(model.getState()!=null&&!model.getState().equals("")){
			sb.append(" and state='"+model.getState()+"'");
			searchModel.setState(model.getState());
		}
		if(linkIP!=null&&!linkIP.equals("")){
			sb.append(" and ip='"+linkIP+"'");
			searchModel.setIp(linkIP);
		}
		if(query_begin_time!=null&&!query_begin_time.equals("")){
			try {
				begin_time= format.parse(query_begin_time);
				sql_begin_time=new java.sql.Date(begin_time.getTime());
				ActionContext.getContext().put("query_begin_time", query_begin_time);
			} catch (ParseException e) {
				ActionContext.getContext().put("msg", "日期格式错误！");
				return "linkRecord";
			} 
		}
		if(query_end_time!=null&&!query_end_time.equals("")){
			try {
				end_time= format.parse(query_end_time);
				sql_end_time=new java.sql.Date(end_time.getTime());
				ActionContext.getContext().put("query_end_time", query_end_time);
			} catch (ParseException e) {
				ActionContext.getContext().put("msg", "日期格式错误！");
				return "linkRecord";
			} 
		}
		
		sb.append(" and startDate <= :endDate and startDate >= :beginDate ");	

		PageBean pageBean=linkRecordService.findByConditionWithPageDate(pageNum,pageSize,sb.toString(),sql_begin_time,sql_end_time);
		ActionContext.getContext().getValueStack().push(pageBean);
		ActionContext.getContext().getValueStack().push(searchModel);
		ActionContext.getContext().put("uid", uid);
		ActionContext.getContext().put("id", uid);
		return "linkRecord";
	}
	
	
	/**
	 * 连接记录查询页面
	 * 
	 * @return
	 */
	public String payRecord() {
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=request.getSession();
		String username=(String) session.getAttribute("username");
		String password=(String) session.getAttribute("password");
		String ip=(String) session.getAttribute("ip");
		
		if(username==null||password==null||ip==null){
			username=(String) session.getAttribute("Cusername");
			password=(String) session.getAttribute("Cpassword");
			ip=(String) session.getAttribute("Cip");
		}
		if(username==null||password==null||ip==null){
			ActionContext.getContext().put("msg", "请先登录！");
			return "loginUI";
		}
		Card searchModel = new Card();
		StringBuffer sb = new StringBuffer("where 1=1 ");
		sb.append(" and accountId='"+uid+"'");
		searchModel.setAccountId(Long.parseLong(uid));
		sb.append(" and state='"+String.valueOf(2)+"'");
		searchModel.setState(String.valueOf(2));
		
		if(payType!=null&&!payType.equals("")){
			sb.append(" and payType='"+payType+"'");
			searchModel.setPayType(payType);
		}
		if(categoryType!=null&&!categoryType.equals("")){
			sb.append(" and categoryType='"+categoryType+"'");
			searchModel.setCategoryType(categoryType);
		}
		if(query_begin_time!=null&&!query_begin_time.equals("")){
			try {
				begin_time= format.parse(query_begin_time);
				sql_begin_time=new java.sql.Date(begin_time.getTime());
				ActionContext.getContext().put("query_begin_time", query_begin_time);
			} catch (ParseException e) {
				ActionContext.getContext().put("msg", "日期格式错误！");
				return "payRecord";
			} 
		}
		if(query_end_time!=null&&!query_end_time.equals("")){
			try {
				end_time= format.parse(query_end_time);
				sql_end_time=new java.sql.Date(end_time.getTime());
				ActionContext.getContext().put("query_end_time", query_end_time);
			} catch (ParseException e) {
				ActionContext.getContext().put("msg", "日期格式错误！");
				return "payRecord";
			} 
		}
		
		sb.append(" and payDate <= :endDate and payDate >= :beginDate ");	
			
		
		
		PageBean pageBean=cardService.findByConditionWithPageDate(pageNum,pageSize,sb.toString(),sql_begin_time,sql_end_time);
		ActionContext.getContext().getValueStack().push(pageBean);
		ActionContext.getContext().getValueStack().push(searchModel);
		ActionContext.getContext().put("uid", uid);
		ActionContext.getContext().put("id", uid);
		return "payRecord";
	}
	
	/**
	 * 充值页面
	 * 
	 * @return
	 */
	public String payUI() {
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=request.getSession();
		String username=(String) session.getAttribute("username");
		String password=(String) session.getAttribute("password");
		String ip=(String) session.getAttribute("ip");
		
		if(username==null||password==null||ip==null){
			username=(String) session.getAttribute("Cusername");
			password=(String) session.getAttribute("Cpassword");
			ip=(String) session.getAttribute("Cip");
		}
		if(username==null||password==null||ip==null){
			ActionContext.getContext().put("msg", "请先登录！");
			return "loginUI";
		}
		
		Account account=accountService.getById(model.getId());
		ActionContext.getContext().getValueStack().push(account);
		
		
		return "payUI";
	}
	
	
	/**
	 * 充值页面
	 * 
	 * @return
	 */
	public String pay() {
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=request.getSession();
		String username=(String) session.getAttribute("username");
		String password=(String) session.getAttribute("password");
		String ip=(String) session.getAttribute("ip");
		
		if(username==null||password==null||ip==null){
			username=(String) session.getAttribute("Cusername");
			password=(String) session.getAttribute("Cpassword");
			ip=(String) session.getAttribute("Cip");
		}
		if(username==null||password==null||ip==null){
			ActionContext.getContext().put("msg", "请先登录！");
			return "loginUI";
		}
		
		Card card=cardService.pay(cardKey);	
		if(card==null){
			ActionContext.getContext().put("msg", "充值卡CD-KEY错误或已经使用！！");
			return "payUI";
		}
		
		int payType=Integer.parseInt(card.getPayType());
		Long payTime=card.getPayTime();
		
		Account user = accountService.getById(model.getId());
		int state=Integer.parseInt(user.getState());
		Long oldDate=user.getDate().getTime();
		Long oldTime=user.getTime();
		Long now=new Date().getTime();
		Long newDate;
		
		if(state==0){
			user.setState(String.valueOf(payType));
		}
		//买断
		if(payType==3){
			//如果记录日期小于现在
			if(oldDate<new Date().getTime()){
				newDate=now+payTime;
			}else{
				newDate=oldDate+payTime;
			}
			user.setDate(new Date(newDate));
			user.setState(String.valueOf(payType));
		}
		//计时
		if(payType==2){
			user.setTime(oldTime+payTime);
		}
		if(state==1){
			user.setState(String.valueOf(state));
		}
		accountService.update(user);
		card.setAccountId(model.getId());
		card.setAccountName(model.getLoginName());
		card.setState(String.valueOf(2));
		card.setPayDate(new Date());
		cardService.update(card);
		
		
		String isOnline;
		if(OnlineMap.getInstance().getOnlineUserMap().containsKey(ip)){
			isOnline="在线";
		}else {
			isOnline="离线";
		}
		Account account = accountService.getById(model.getId());
		ActionContext.getContext().getValueStack().push(account);
		ActionContext.getContext().put("isOnline", isOnline);
		ActionContext.getContext().put("msg", "充值成功！！");
		return "toIndex";
	}

	/**
	 * 修改页面
	 * 
	 * @return
	 */
	public String editUI() {
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=request.getSession();
		String username=(String) session.getAttribute("username");
		String password=(String) session.getAttribute("password");
		String ip=(String) session.getAttribute("ip");
		
		if(username==null||password==null||ip==null){
			username=(String) session.getAttribute("Cusername");
			password=(String) session.getAttribute("Cpassword");
			ip=(String) session.getAttribute("Cip");
		}
		if(username==null||password==null||ip==null){
			ActionContext.getContext().put("msg", "请先登录！");
			return "loginUI";
		}
		
		
		Account account=accountService.getById(model.getId());
		ActionContext.getContext().getValueStack().push(account);
		
		
		return "saveUI";
	}

	/**
	 * 修改
	 * 
	 * @return
	 */
	public String edit() {
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=request.getSession();
		String username=(String) session.getAttribute("username");
		String password=(String) session.getAttribute("password");
		String ip=(String) session.getAttribute("ip");
		
		if(username==null||password==null||ip==null){
			username=(String) session.getAttribute("Cusername");
			password=(String) session.getAttribute("Cpassword");
			ip=(String) session.getAttribute("Cip");
		}
		if(username==null||password==null||ip==null){
			ActionContext.getContext().put("msg", "请先登录！");
			return "loginUI";
		}
		// 1，从数据库中取出原对象
		Account account = accountService.getById(model.getId());

        String ps=model.getPassword();
		
		if(!(account.getPassword().equals(ps))&&!(ps.equals("")||ps==null)){
			String md5Digest = DigestUtils.md5Hex(model.getPassword());
			account.setPassword(md5Digest);
		}
		// 2，设置要修改的属性
		
		account.setName(model.getName());
		account.setGender(model.getGender());
		account.setPhoneNumber(model.getPhoneNumber());
		account.setEmail(model.getEmail());
		account.setDescription(model.getDescription());
		
		

		// 3，更新到数据库
		accountService.update(account);
		
		
		
		
		String isOnline;
		if(OnlineMap.getInstance().getOnlineUserMap().containsKey(ip)){
			isOnline="在线";
		}else {
			isOnline="离线";
		}
		
		ActionContext.getContext().getValueStack().push(account);
		ActionContext.getContext().put("isOnline", isOnline);
		ActionContext.getContext().put("msg", "修改成功！");

		return "toIndex";
	}

	

	/**
	 * 登陆页面
	 * 
	 * @return
	 */
	public String loginUI() {
		return "loginUI";
	}

	/**
	 * 登陆
	 * 
	 * @return
	 */
	public String login() {
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=request.getSession();
		String username=(String) session.getAttribute("username");
		String password=(String) session.getAttribute("password");
		String ip=(String) session.getAttribute("ip");
		
		if(username==null||password==null||ip==null){
			username=(String) session.getAttribute("Cusername");
			password=(String) session.getAttribute("Cpassword");
			ip=(String) session.getAttribute("Cip");
		}
		if(username==null||password==null||ip==null){
			username=model.getLoginName();
			password=model.getPassword();
			ip=request.getRemoteAddr();
		}
		if(username==null||password==null||ip==null){
			return "loginUI";
		}
		
		if((username.equals("")||username==null)||(password.equals("")||password==null)||(ip.equals("")||ip==null)){
			
			ActionContext.getContext().put("msg", "用户名或密码为空！");
			return "loginUI";
		}
		
		Account account= accountService.findByLoginNameAndPassword(username, password);
		if (account == null) {
			ActionContext.getContext().put("msg", "用户名或密码不正确！");
			return "loginUI";
		} else {
			String isOnline;
			if(OnlineMap.getInstance().getOnlineUserMap().containsKey(ip)){
				isOnline="在线";
			}else {
				isOnline="离线";
			}
				
			
			session.setAttribute("Cusername", username);
			session.setAttribute("Cpassword", password);
			session.setAttribute("Cip", ip);
			ActionContext.getContext().getValueStack().push(account);
			ActionContext.getContext().put("isOnline", isOnline);
			ActionContext.getContext().put("msg", "登录成功！");
			return "toIndex";
		}

	}

	/**
	 * 注销
	 * 
	 * @return
	 */
	public String logout() {
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=request.getSession();
		session.removeAttribute("Cusername");
		session.removeAttribute("Cpassword");
		session.removeAttribute("Cip");
		ActionContext.getContext().put("msg", "用户已退出！");
		return "loginUI";
	}

	/**
	 * 添加页面
	 * 
	 * @return
	 */
	public String addUI() {
		if(config.getAccountAdd().equals(String.valueOf(1))){
			return "addUI";
		}else {
			ActionContext.getContext().put("msg", "当前不允许自助注册新用户，请联系管理员！");
			return "loginUI";
		}
		
	}
	
	
	
	/**
	 * 添加
	 * 
	 * @return
	 */
	public String add() {
		if(!MyUtils.checkUserName(model.getLoginName())){
			model.setLoginName("用户名不符合规范");
			ActionContext.getContext().getValueStack().push(model);
			ActionContext.getContext().put("msg", "用户名不符合规范！！");
			return "addUI";
		}
		if(!accountService.checkLoginName(model.getLoginName())){
			model.setLoginName("用户名已经存在");
			ActionContext.getContext().getValueStack().push(model);
			ActionContext.getContext().put("msg", "用户名已经存在");
			return "addUI";
		}
		// >> 设置默认密码为1234（要使用MD5摘要）
		String md5Digest = DigestUtils.md5Hex("1234");
		if(!(model.getPassword().equals("")||model.getPassword()==null)){
			md5Digest=DigestUtils.md5Hex(model.getPassword());
		}
		model.setPassword(md5Digest);
		model.setDate(new Date());
		int intTime=1000*60*10;   //默认给10分钟
		model.setTime((long)intTime);  
		model.setState(String.valueOf(2));

		// 保存到数据库
		accountService.save(model);
		
		
		ActionContext.getContext().put("msg", "新用户:  "+model.getLoginName()+" 注册成功！");
		return "loginUI";
	}

}
