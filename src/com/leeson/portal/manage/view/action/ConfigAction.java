/*  
 * Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
 * LeeSon  QQ:25901875
 */
package com.leeson.portal.manage.view.action;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.leeson.portal.manage.domain.Config;
import com.leeson.portal.manage.base.BaseAction;
import com.opensymphony.xwork2.ActionContext;

@Controller
@Scope("prototype")
public class ConfigAction extends BaseAction<Config> {

	private static final long serialVersionUID = -4096068296817072648L;

	
	/**
	 * 修改页面
	 * 
	 * @return
	 */
	public String show() {
		// List<Department> departmentList=departmentService.list();
		List<Config> configs=configService.findByCondition("where id = 1");
		Config config=configs.get(0);
		ActionContext.getContext().getValueStack().push(config);
		return "getConfig";
	}

	/**
	 * 修改
	 * 
	 * @return
	 */
	public String saveConfig() {

		// 1，从数据库取出原对象
		Config config=configService.getById(model.getId());

		// 2，设置要修改的属性
		config.setBas_ip(model.getBas_ip());
		config.setBas_port(model.getBas_port());
		config.setPortal_port(model.getPortal_port());
		config.setSharedSecret(model.getSharedSecret());
		config.setAuthType(model.getAuthType());
		config.setTimeoutSec(model.getTimeoutSec());
		config.setPortalVer(model.getPortalVer());
		config.setBas(model.getBas());
		config.setIsdebug(model.getIsdebug());
		config.setVerifyCode(model.getVerifyCode());
		config.setUserHeart(model.getUserHeart());
		config.setUserHeartCount(model.getUserHeartCount());
		config.setUserHeartTime(model.getUserHeartTime());
		config.setAuth_interface(model.getAuth_interface());
		config.setBas_user(model.getBas_user());
		config.setBas_pwd(model.getBas_pwd());
		config.setAccountAdd(model.getAccountAdd());

		// 3，更新到数据库中
		configService.update(config);
		
		com.leeson.portal.core.model.Config cfg = com.leeson.portal.core.model.Config.getInstance();
		cfg.setBas_ip(config.getBas_ip());
		cfg.setBas_port(config.getBas_port());
		cfg.setPortal_port(config.getPortal_port());
		cfg.setSharedSecret(config.getSharedSecret());
		cfg.setAuthType(config.getAuthType());
		cfg.setTimeoutSec(config.getTimeoutSec());
		cfg.setPortalVer(config.getPortalVer());
		cfg.setBas(config.getBas());
		cfg.setDebug(config.getIsdebug());
		cfg.setVerifyCode(config.getVerifyCode());
		cfg.setUserHeart(config.getUserHeart());
		cfg.setUserHeartCount(config.getUserHeartCount());
		cfg.setUserHeartTime(config.getUserHeartTime());
		cfg.setAuth_interface(config.getAuth_interface());
		cfg.setBas_user(config.getBas_user());
		cfg.setBas_pwd(config.getBas_pwd());
		cfg.setAccountAdd(config.getAccountAdd());
		System.out.println("重新加载设置====================================================");
		return "getConfig";
	}

}
