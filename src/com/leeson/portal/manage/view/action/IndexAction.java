/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.view.action;



import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.leeson.portal.core.model.Config;
import com.leeson.portal.core.model.OnlineMap;
import com.leeson.portal.core.service.InterfaceControl;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

@Controller
@Scope("prototype")
public class IndexAction extends ActionSupport {

	private static final long serialVersionUID = -8869380195221278403L;
	private Config config=Config.getInstance();
	
	public String index_choose() throws Exception {
		/**
		 * 本地用户
		 */
		if(Integer.parseInt(config.getAuth_interface())==1){
			return "local";
			
			/**
			 * 第三方radius
			 */
		}else if(Integer.parseInt(config.getAuth_interface())==2){
			return "index";
			
			/**
			 * APP接口
			 */
		}else if(Integer.parseInt(config.getAuth_interface())==3){
			return "app";
			
			/**
			 * 短信接口
			 */
		}else if(Integer.parseInt(config.getAuth_interface())==4){
			return "sms";
			
			/**
			 * 微信接口
			 */
		}else if(Integer.parseInt(config.getAuth_interface())==5){
			return "weixin";
			
			/**
			 * 
			 */
		}else {
			HttpServletRequest request=ServletActionContext.getRequest();
			String ip=request.getRemoteAddr();// 获取客户端的ip
			Boolean info = InterfaceControl.Method("PORTAL_LOGIN", config.getBas_user(),
					config.getBas_pwd(), ip);
			if (info == true) {
				OnlineMap onlineMap=OnlineMap.getInstance();
				
				//将用户和IP加入在线列表
				String[] loginInfo=new String[3];
				loginInfo[0]=config.getBas_user();
				
				onlineMap.getOnlineUserMap().put(ip,loginInfo);
				ActionContext.getContext().getSession().put("ip", ip);
				ActionContext.getContext().getSession().put("msg", "认证成功！");
			} else {
				ActionContext.getContext().getSession().put("msg", "认证失败！");
			}
			return "show";
		}
		
	}
	

}
