/*  
 * Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
 * LeeSon  QQ:25901875
 */
package com.leeson.portal.manage.view.action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import net.sf.json.JSONObject;

import com.leeson.portal.core.service.InterfaceControl;
import com.leeson.portal.manage.base.BaseAction;
import com.leeson.portal.manage.domain.Account;

@Controller
@Scope("prototype")
public class AjaxInterFaceAction extends BaseAction<Account> {

	private static final long serialVersionUID = 6060689481082866366L;

	public String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	
	public String Login() {
		
		HttpServletRequest request = ServletActionContext.getRequest();    
        String username=request.getParameter("usr");    
        String password=request.getParameter("pwd");
        String ip=request.getRemoteAddr();
        Boolean info = InterfaceControl.Method("PORTAL_LOGIN", username,
				password, ip);
        // 用一个Map做例子
     	Map<String, String> map = new HashMap<String, String>();
     	
        if(info){
        	map.put("ret", "0");
        	map.put("i", ip);
        	map.put("e", "1");
        	map.put("msg", "认证成功！");
        	map.put("l", "www.baidu.com");
        	
        }else{
        	map.put("ret", "1");
        	map.put("i", ip);
        	map.put("e", "1");
        	map.put("msg", "认证失败！或者你已经在线！下线请访问：ok.html");
        	map.put("l", "www.baidu.com");
        }


		

		// 将要返回的map对象进行json处理
		JSONObject jo = JSONObject.fromObject(map);

		// 调用json对象的toString方法转换为字符串然后赋值给result
		this.result = jo.toString();

		return SUCCESS;

	}
	public String LoginOut() {
		
		HttpServletRequest request = ServletActionContext.getRequest();    
		String username= "test";
		String password= "test";
		String ip=request.getRemoteAddr();
		Boolean info = InterfaceControl.Method("PORTAL_LOGINOUT",
				username, password, ip);
		// 用一个Map做例子
		Map<String, String> map = new HashMap<String, String>();
		
		if(info){
			map.put("ret", "0");
			map.put("msg", "下线成功！");
			
		}else{
			map.put("ret", "1");
			map.put("msg", "下线失败！请稍后再试！或者你已经离线！");
		}
		
		
		
		
		// 将要返回的map对象进行json处理
		JSONObject jo = JSONObject.fromObject(map);
		
		// 调用json对象的toString方法转换为字符串然后赋值给result
		this.result = jo.toString();
		
		return SUCCESS;
		
	}

}
