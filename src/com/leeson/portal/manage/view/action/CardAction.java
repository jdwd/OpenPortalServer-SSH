/*  
 * Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
 * LeeSon  QQ:25901875
 */
package com.leeson.portal.manage.view.action;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.leeson.portal.manage.base.BaseAction;
import com.leeson.portal.manage.domain.Card;
import com.leeson.portal.manage.domain.CardCategory;
import com.leeson.portal.manage.domain.PageBean;

@Controller
@Scope("prototype")
public class CardAction extends BaseAction<Card> {

	private static final long serialVersionUID = 4607613828683416523L;
	
	private int pageNum=1;  //当前页
    private int pageSize=10;  //每页多少条记录
	private String[] choose;
	
	private Long cardCategoryId;
	private int cardCount=1;
	
	
	public Long getCardCategoryId() {
		return cardCategoryId;
	}

	public void setCardCategoryId(Long cardCategoryId) {
		this.cardCategoryId = cardCategoryId;
	}

	public int getCardCount() {
		return cardCount;
	}

	public void setCardCount(int cardCount) {
		this.cardCount = cardCount;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String[] getChoose() {
		return choose;
	}

	public void setChoose(String[] choose) {
		this.choose = choose;
	}

	/**
	 * 列表（带分页）
	 * 
	 * @return
	 */
	public String list() {
		Card searchModel = new Card();
		StringBuffer sb = new StringBuffer("where 1=1 ");
		if(model.getAccountName()!=null&&!model.getAccountName().equals("")){
			sb.append(" and accountName like '%"+model.getAccountName()+"%' ");
			searchModel.setAccountName(model.getAccountName());
		}
		if(model.getName()!=null&&!model.getName().equals("")){
			sb.append(" and name like '%"+model.getName()+"%' ");
			searchModel.setName(model.getName());
		}
		if(model.getDescription()!=null&&!model.getDescription().equals("")){
			sb.append(" and description like '%"+model.getDescription()+"%' ");
			searchModel.setDescription(model.getDescription());
		}
		if(model.getState()!=null&&!model.getState().equals("")){
			sb.append(" and state='"+model.getState()+"'");
			searchModel.setState(model.getState());
		}
		if(model.getPayType()!=null&&!model.getPayType().equals("")){
			sb.append(" and payType='"+model.getPayType()+"'");
			searchModel.setPayType(model.getPayType());
		}
		if(model.getCategoryType()!=null&&!model.getCategoryType().equals("")){
			sb.append(" and categoryType='"+model.getCategoryType()+"'");
			searchModel.setCategoryType(model.getCategoryType());
		}
		
		PageBean pageBean=cardService.findByConditionWithPage(pageNum,pageSize,sb.toString());
		ActionContext.getContext().getValueStack().push(pageBean);
		ActionContext.getContext().getValueStack().push(searchModel);
		return "list";
	}

	/**
	 * 删除
	 * 
	 * @return
	 */
	public String delete() {
		cardService.delete(model.getId());
		return "toList";
	}

	/**
	 * 删除多选
	 * 
	 * @return
	 */
	public String deleteChoose() {
		if (choose != null && choose.length > 0) {
			for (int i = 0; i < choose.length; i++) {
				cardService.delete(Long.parseLong(choose[i]));
			}
		}
			
		return "toList";
	}
	
	
	/**
	 * 添加页面
	 * 
	 * @return
	 */
	public String addUI() {
		List<CardCategory> cardCategoryList = cardCategoryService.list();
		ActionContext.getContext().put("cardCategoryList", cardCategoryList);
		return "saveUI";
	}

	/**
	 * 添加
	 * 
	 * @return
	 */
	public String add() {
		String name=model.getName();
		String description=model.getDescription();
		CardCategory cardCategory=cardCategoryService.getById(cardCategoryId);
		if(model.getName()==null||model.getName().equals("")){
			name=cardCategory.getName();
		}
		if(model.getDescription()==null||model.getDescription().equals("")){
			description=cardCategory.getDescription();
		}
		String categoryType=cardCategory.getState();
		String payType;
		if(categoryType.equals(String.valueOf(0))){
			payType=String.valueOf(2);
		}else {
			payType=String.valueOf(3);
		}
		Long time=cardCategory.getTime();
		Long payTime=(long) 0;
		if(categoryType.equals(String.valueOf(0))){
			payTime=time*1000*60*60;
		}else if (categoryType.equals(String.valueOf(1))) {
			payTime=time*1000*60*60*24;
		}else if (categoryType.equals(String.valueOf(2))) {
			payTime=time*1000*60*60*24*31;
		}else if (categoryType.equals(String.valueOf(3))) {
			payTime=time*1000*60*60*24*31*12;
		}

		// 保存到数据库
		cardService.creatCard(name,description,payTime,payType,categoryType,cardCount);
		return "toList";
	}

	
	
	public String out(){
		return "out";
	}
	
	

}
