/*  
 * Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
 * LeeSon  QQ:25901875
 */
package com.leeson.portal.manage.view.action;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.leeson.portal.manage.base.BaseAction;
import com.leeson.portal.manage.domain.PageBean;
import com.leeson.portal.manage.domain.Privilege;
import com.leeson.portal.manage.utils.PrivilegeUtils;
import com.opensymphony.xwork2.ActionContext;

@Controller
@Scope("prototype")
public class PrivilegeAction extends BaseAction<Privilege> {

	private static final long serialVersionUID = -4096068296817072648L;

	
	
	private Long parentId;
	private int pageNum=1;  //当前页
    private int pageSize=10;  //每页多少条记录
	private String[] choose;

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String[] getChoose() {
		return choose;
	}

	public void setChoose(String[] choose) {
		this.choose = choose;
	}
	
	
	/**
	 * 列表
	 * 
	 * @return
	 */
	public String listView() {
		List<Privilege> topList = privilegeService.findTopList();
		List<Privilege> privilegeList = PrivilegeUtils
				.getAllPrivileges(topList);
		ActionContext.getContext().put("privilegeList", privilegeList);
		return "tree";
	}
	
	/**
	 * 列表（带分页）
	 * 
	 * @return
	 */
	public String list() {
		Privilege searchModel = new Privilege();
		StringBuffer sb = new StringBuffer("where 1=1 ");
		
		if(model.getName()!=null&&!model.getName().equals("")){
			sb.append(" and name like '%"+model.getName()+"%' ");
			searchModel.setName(model.getName());
		}
		if(model.getId()!=null&&!model.getId().equals("")){
			sb.append(" and id='"+model.getId()+"'");
			searchModel.setId(model.getId());
		}
		if(parentId !=null&&!parentId.equals("")){
			sb.append(" and parentId="+parentId+" ");
			this.setParentId(parentId);
			Privilege parent = privilegeService.getById(parentId);
			ActionContext.getContext().put("parent", parent);
		}
		if(model.getUrl()!=null&&!model.getUrl().equals("")){
			sb.append(" and url like '%"+model.getUrl()+"%' ");
			searchModel.setUrl(model.getUrl());
		}
		sb.append(" ORDER BY position");
		
		PageBean pageBean=privilegeService.findByConditionWithCount(sb.toString());
		ActionContext.getContext().getValueStack().push(pageBean);
		ActionContext.getContext().getValueStack().push(searchModel);
		
		List<Privilege> topList = privilegeService.findTopList();
		List<Privilege> privilegeList = PrivilegeUtils
				.getAllPrivileges(topList);
		ActionContext.getContext().put("privilegeList", privilegeList);
		
		return "list";
	}

	/**
	 * 删除
	 * 
	 * @return
	 */
	public String delete() {
		privilegeService.delete(model.getId());
		return "toList";
	}

	
	/**
	 * 删除多选
	 * 
	 * @return
	 */
	public String deleteChoose() {
		if (choose != null && choose.length > 0) {
			for (int i = 0; i < choose.length; i++) {
				privilegeService.delete(Long.parseLong(choose[i]));
			}
		}
			
		return "toList";
	}
	
	
	/**
	 * 添加页面
	 * 
	 * @return
	 */
	public String addUI() {
		// List<Privilege> PrivilegeList=PrivilegeService.list();
		List<Privilege> topList = privilegeService.findTopList();
		List<Privilege> privilegeList = PrivilegeUtils
				.getAllPrivileges(topList);
		ActionContext.getContext().put("privilegeList", privilegeList);
		return "saveUI";
	}

	/**
	 * 添加
	 * 
	 * @return
	 */
	public String add() {
		model.setParent(privilegeService.getById(parentId));
		privilegeService.save(model);
		return "toList";
	}

	/**
	 * 修改页面
	 * 
	 * @return
	 */
	public String editUI() {
		// List<Privilege> PrivilegeList=PrivilegeService.list();
		List<Privilege> topList = privilegeService.findTopList();
		List<Privilege> privilegeList = PrivilegeUtils
				.getAllPrivileges(topList);
		ActionContext.getContext().put("privilegeList", privilegeList);
		Privilege privilege = privilegeService.getById(model.getId());
		ActionContext.getContext().getValueStack().push(privilege);
		if (privilege.getParent() != null) {
			parentId = privilege.getParent().getId();
			// ActionContext.getContext().put("parentId", parentId);
			// //因为parentId是属性（有set/get方法）已经放入栈顶，不用再添加
		}

		return "saveUI";
	}

	/**
	 * 修改
	 * 
	 * @return
	 */
	public String edit() {
		// 1，从数据库取出原对象
		Privilege Privilege = privilegeService.getById(model.getId());
		// 2，设置要修改的属性
		Privilege.setName(model.getName());
		Privilege.setUrl(model.getUrl());
		Privilege.setParent(privilegeService.getById(parentId)); // 设置所属的上级部门
		// 3，更新到数据库中
		privilegeService.update(Privilege);

		return "toList";
	}
	
	/**
	 * 上移
	 * 
	 * @return
	 */
	public String moveUp() {
		privilegeService.moveUp(model.getId());
		return "tree";
	}
	/**
	 * 下移
	 * 
	 * @return
	 */
	public String moveDown() {
		privilegeService.moveDown(model.getId());
		return "tree";
	}

}
