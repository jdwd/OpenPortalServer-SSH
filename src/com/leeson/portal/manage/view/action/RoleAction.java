/*  
 * Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
 * LeeSon  QQ:25901875
 */
package com.leeson.portal.manage.view.action;

import java.util.HashSet;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.leeson.portal.manage.base.BaseAction;
import com.leeson.portal.manage.domain.PageBean;
import com.leeson.portal.manage.domain.Privilege;
import com.leeson.portal.manage.domain.Role;
import com.opensymphony.xwork2.ActionContext;

@Controller
@Scope("prototype")
public class RoleAction extends BaseAction<Role> {

	private static final long serialVersionUID = -4096068296817072648L;

	private int pageNum=1;  //当前页
    private int pageSize=10;  //每页多少条记录
	private String[] choose;
	
	
	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String[] getChoose() {
		return choose;
	}

	public void setChoose(String[] choose) {
		this.choose = choose;
	}
	
	
	private Long[] privilegeIds;
	
	
	public Long[] getPrivilegeIds() {
		return privilegeIds;
	}

	public void setPrivilegeIds(Long[] privilegeIds) {
		this.privilegeIds = privilegeIds;
	}

	/**
	 * 列表
	 * 
	 * @return
	 */
	public String list1() {
		List<Role> roleList = roleService.list();
		ActionContext.getContext().put("roleList", roleList);
		return "list";
	}
	
	/**
	 * 列表（带分页）
	 * 
	 * @return
	 */
	public String list() {
		Role searchModel = new Role();
		StringBuffer sb = new StringBuffer("where 1=1 ");
		if(model.getName()!=null&&!model.getName().equals("")){
			sb.append(" and name like '%"+model.getName()+"%' ");
			searchModel.setName(model.getName());
		}
		if(model.getId()!=null&&!model.getId().equals("")){
			sb.append(" and id='"+model.getId()+"'");
			searchModel.setId(model.getId());
		}
		if(model.getDescription()!=null&&!model.getDescription().equals("")){
			sb.append(" and description like '%"+model.getDescription()+"%' ");
			searchModel.setDescription(model.getDescription());
		}
		
		
		PageBean pageBean=roleService.findByConditionWithPage(pageNum,pageSize,sb.toString());
		ActionContext.getContext().getValueStack().push(pageBean);
		ActionContext.getContext().getValueStack().push(searchModel);
		return "list";
	}

	

	/**
	 * 删除
	 * 
	 * @return
	 */
	public String delete() {
		roleService.delete(model.getId());
		return "toList";
	}

	/**
	 * 删除多选
	 * 
	 * @return
	 */
	public String deleteChoose() {
		if (choose != null && choose.length > 0) {
			for (int i = 0; i < choose.length; i++) {
				roleService.delete(Long.parseLong(choose[i]));
			}
		}
			
		return "toList";
	}
	
	/**
	 * 添加页面
	 * 
	 * @return
	 */
	public String addUI() {

		return "saveUI";
	}

	/**
	 * 添加
	 * 
	 * @return
	 */
	public String add() {
		roleService.save(model);
		return "toList";
	}

	/**
	 * 修改页面
	 * 
	 * @return
	 */
	public String editUI() {
		Role role = roleService.getById(model.getId());
		ActionContext.getContext().getValueStack().push(role);
		return "saveUI";
	}

	/**
	 * 修改
	 * 
	 * @return
	 */
	public String edit() {
		// roleService.update(model);
		// return "toList";

		// 1，从数据库中获取原对象
		Role role = roleService.getById(model.getId());

		// 2，设置要修改的属性
		role.setName(model.getName());
		role.setDescription(model.getDescription());

		// 3，更新到数据库
		roleService.update(role);

		return "toList";
	}
	
	/**
	 * 设置权限页面
	 * 
	 * @return
	 */
	public String setPrivilegeUI() {
		Role role = roleService.getById(model.getId());
		ActionContext.getContext().getValueStack().push(role);
		if(role.getPrivileges()!=null){
			int index=0;
			privilegeIds=new Long[role.getPrivileges().size()];
			for(Privilege priv:role.getPrivileges()){
				privilegeIds[index++]=priv.getId();
			}
		}
		List<Privilege> privilegeList=privilegeService.list();
		ActionContext.getContext().put("privilegeList", privilegeList);
		return "setPrivilegeUI";
	}

	/**
	 * 设置权限
	 * 
	 * @return
	 */
	public String setPrivilege() {
		// roleService.update(model);
		// return "toList";

		// 1，从数据库中获取原对象
		Role role = roleService.getById(model.getId());

		// 2，设置要修改的属性
		List<Privilege> privilegeList=privilegeService.getByIds(privilegeIds);
		role.setPrivileges(new HashSet<Privilege>(privilegeList));

		// 3，更新到数据库
		roleService.update(role);

		return "toList";
	}

}
