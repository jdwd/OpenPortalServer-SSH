/*  
 * Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
 * LeeSon  QQ:25901875
 */
package com.leeson.portal.manage.view.action;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import net.sf.json.JSONObject;

import com.leeson.portal.core.model.OnlineMap;
import com.leeson.portal.manage.base.BaseAction;
import com.leeson.portal.manage.domain.Account;
import com.leeson.portal.manage.domain.LogRecord;
import com.leeson.portal.manage.domain.User;
import com.opensymphony.xwork2.ActionContext;

@Controller
@Scope("prototype")
public class AjaxAction extends BaseAction<Account> {

	private static final long serialVersionUID = 6060689481082866366L;

	public String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String onlineCount() {

		// 用一个Map做例子
		Map<String, Integer> map = new HashMap<String, Integer>();

		int online_count = 0;

		HashMap<String, String[]> OnlineUserMap = OnlineMap.getInstance()
				.getOnlineUserMap();
		Iterator<String> iterator = OnlineUserMap.keySet().iterator();
		while (iterator.hasNext()) {
			iterator.next();
			online_count++;
		}

		map.put("online_count", online_count);

		// 将要返回的map对象进行json处理
		JSONObject jo = JSONObject.fromObject(map);

		// 调用json对象的toString方法转换为字符串然后赋值给result
		this.result = jo.toString();

		return SUCCESS;

	}

	public String fenleiCount() {

		// 用一个Map做例子
		Map<String, Integer> map = new HashMap<String, Integer>();

//		List<Account> accounts = accountService.list();
//		List<Account> lockaccounts = accountService
//				.findByCondition("Where state = '0' ");
//		int lock_count = lockaccounts.size();
		int lock_count = accountService.findCountByCondition("Where state = '0' ");
//		int acc_count = accounts.size();
		int acc_count = accountService.findCountByCondition("");

		int online_count = 0;

		HashMap<String, String[]> OnlineUserMap = OnlineMap.getInstance()
				.getOnlineUserMap();
		Iterator<String> iterator = OnlineUserMap.keySet().iterator();
		while (iterator.hasNext()) {
			iterator.next();
			online_count++;
		}

		map.put("online_count", online_count);
		map.put("acc_count", acc_count);
		map.put("lock_count", lock_count);
		map.put("true_count", acc_count - lock_count);
		int outline_count=acc_count-online_count;
		if(outline_count<0){
			outline_count=0;
		}
		map.put("outline_count", outline_count);
		

		// 将要返回的map对象进行json处理
		JSONObject jo = JSONObject.fromObject(map);

		// 调用json对象的toString方法转换为字符串然后赋值给result
		this.result = jo.toString();

		return SUCCESS;

	}
	
	public String getLogs() {
		
//		List<LogRecord> temp=logRecordService.findByCondition("order by id desc");
//		
//		Map<String, String> operationRecords=new HashMap<String, String>();
//		for(int i=0;i<temp.size();i++){
//			operationRecords.put(temp.get(i).getInfo(), temp.get(i).getRec_date().toString());
//			if(i>5){
//				break;
//			}
//		}
		List<LogRecord> operationRecords=logRecordService.findByConditionWithPage(1, 5, "order by id desc").getRecordList();
		
		
		
		
		
		// 将要返回的map对象进行json处理
		JSONObject jo = JSONObject.fromObject(operationRecords);
		
		// 调用json对象的toString方法转换为字符串然后赋值给result
		this.result = jo.toString();
		
		return SUCCESS;
		
	}
	
	
	
	
public String msgCount() {
		
	User user = (User) ActionContext.getContext().getSession().get("user"); // 当前登录用户
	String toid=user.getId().toString();
	String state=String.valueOf(1);
	StringBuffer sb = new StringBuffer("where 1=1 ");
	sb.append(" and  toid='"+toid+"' ");
	sb.append(" and  delin<>'1' ");
	sb.append(" and state='"+state+"'");
//	PageBean pageBean=messageService.findByConditionWithCount(sb.toString());
//	int msgCount=pageBean.getRecordCount();
	int msgCount=messageService.findCountByCondition(sb.toString());
	
		
		Map<String, Integer> map=new HashMap<String, Integer>();
		map.put("msgCount", msgCount);
		// 将要返回的map对象进行json处理
		JSONObject jo = JSONObject.fromObject(map);
		
		// 调用json对象的toString方法转换为字符串然后赋值给result
		this.result = jo.toString();
		
		return SUCCESS;
		
	}

}
