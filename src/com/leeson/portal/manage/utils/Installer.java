package com.leeson.portal.manage.utils;

import javax.annotation.Resource;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.leeson.portal.manage.domain.Config;
import com.leeson.portal.manage.domain.Privilege;
import com.leeson.portal.manage.domain.User;

@Component
public class Installer {

	@Resource
	private SessionFactory sessionFactory;

	/**
	 * 执行安装
	 */
	@Transactional
	public void install() {
		Session session = sessionFactory.getCurrentSession();

		// ==============================================================
		// 保存超级管理员用户
		User user = new User();
		user.setLoginName("admin");
		user.setName("超级管理员");
		user.setPassword(DigestUtils.md5Hex("admin"));
		session.save(user); // 保存
		
		// ==============================================================
		// 初始化主配置
		Config config=new Config();
		config.setBas_ip("192.168.2.100");
		config.setBas_port("2000");
		config.setSharedSecret("LeeSon");
		config.setIsdebug("1");
		config.setPortalVer("2");
		config.setTimeoutSec("3");
		config.setAuthType("1");
		config.setBas("0");
		config.setPortal_port("50100");
		config.setVerifyCode("0");
		config.setUserHeart("1");
		config.setUserHeartCount("3");
		config.setUserHeartTime("10");
		config.setAuth_interface("1");
		config.setBas_user("LeeSon");
		config.setBas_pwd("LeeSon");
		config.setAccountAdd("1");
		session.save(config);
				
				

		// ==============================================================
		// 保存权限数据
		Privilege menu, menu1, menu2, menu3, menu4, menu5, menu6;

		// --------------------
		menu = new Privilege("系统管理", null, null, 1);
		menu1 = new Privilege("角色管理", "/role_list", menu, 11);
		menu2 = new Privilege("分类管理", "/department_list", menu, 12);
		menu3 = new Privilege("用户管理", "/user_list", menu, 13);
		menu4 = new Privilege("权限管理", "/privilege_list", menu, 14);
		menu5 = new Privilege("对接设置", "/config_show", menu, 15);
		session.save(menu);
		session.save(menu1);
		session.save(menu2);
		session.save(menu3);
		session.save(menu4);
		session.save(menu5);

		session.save(new Privilege("角色列表", "/role_list", menu1, 111));
		session.save(new Privilege("角色删除", "/role_delete", menu1, 112));
		session.save(new Privilege("角色添加", "/role_add", menu1, 113));
		session.save(new Privilege("角色修改", "/role_edit", menu1, 114));
		session.save(new Privilege("角色权限设置", "/role_setPrivilege", menu1, 115));

		session.save(new Privilege("分类列表", "/department_list", menu2, 121));
		session.save(new Privilege("分类删除", "/department_delete", menu2, 122));
		session.save(new Privilege("分类添加", "/department_add", menu2, 123));
		session.save(new Privilege("分类修改", "/department_edit", menu2, 124));

		session.save(new Privilege("用户列表", "/user_list", menu3, 131));
		session.save(new Privilege("用户删除", "/user_delete", menu3, 132));
		session.save(new Privilege("用户添加", "/user_add", menu3, 133));
		session.save(new Privilege("用户修改", "/user_edit", menu3, 134));
		session.save(new Privilege("初始化密码", "/user_initPassword", menu3, 135));
		
		session.save(new Privilege("权限列表", "/privilege_list", menu4, 141));
		session.save(new Privilege("权限删除", "/privilege_delete", menu4, 142));
		session.save(new Privilege("权限添加", "/privilege_add", menu4, 143));
		session.save(new Privilege("权限修改", "/privilege_edit", menu4, 144));
		session.save(new Privilege("权限上移", "/privilege_moveUp", menu4, 145));
		session.save(new Privilege("权限下移", "/privilege_moveDown", menu4, 146));
		
		session.save(new Privilege("对接设置信息", "/config_show", menu5, 151));
		session.save(new Privilege("对接设置保存", "/config_saveConfig", menu5, 152));

		// --------------------
		menu = new Privilege("接入账户管理", null, null, 2);
		menu1 = new Privilege("账户列表", "/account_list", menu, 21);
		menu2 = new Privilege("账户添加", "/account_addUI", menu, 22);
		menu3 = new Privilege("账户导入", "/account_in", menu, 23);
		menu4 = new Privilege("账户导出", "/account_out", menu, 24);
		menu5 = new Privilege("连接记录", "/linkRecord_list", menu, 25);
		menu6 = new Privilege("在线列表", "/linkRecord_listOnline", menu, 26);
		session.save(menu);
		session.save(menu1);
		session.save(menu2);
		session.save(menu3);
		session.save(menu4);
		session.save(menu5);
		session.save(menu6);
		session.save(new Privilege("账户列表", "/account_list", menu1, 211));
		session.save(new Privilege("账户删除", "/account_delete", menu1, 212));
		session.save(new Privilege("账户添加", "/account_add", menu1, 213));
		session.save(new Privilege("账户修改", "/account_edit", menu1, 214));
		session.save(new Privilege("账户充值", "/account_pay", menu1, 215));
		session.save(new Privilege("连接记录", "/linkRecord_list", menu1, 216));
		session.save(new Privilege("连接记录列表", "/linkRecord_list", menu5, 251));
		session.save(new Privilege("连接记录删除", "/linkRecord_delete", menu5, 252));
		session.save(new Privilege("用户下线", "/linkRecord_kick", menu6, 261));

		// --------------------
		menu = new Privilege("充值卡管理", null, null, 3);
		menu1 = new Privilege("充值卡分类", "/cardCategory_list", menu, 31);
		menu2 = new Privilege("充值卡列表", "/card_list", menu, 32);
		menu3 = new Privilege("充值卡添加", "/card_addUI", menu, 33);
		session.save(menu);
		session.save(menu1);
		session.save(menu2);
		session.save(menu3);
		session.save(new Privilege("分类列表", "/cardCategory_list", menu1, 311));
		session.save(new Privilege("分类删除", "/cardCategory_delete", menu1, 312));
		session.save(new Privilege("分类添加", "/cardCategory_add", menu1, 313));
		session.save(new Privilege("分类修改", "/cardCategory_edit", menu1, 314));
		session.save(new Privilege("充值卡列表", "/card_list", menu2, 321));
		session.save(new Privilege("充值卡删除", "/card_delete", menu2, 322));
		session.save(new Privilege("充值卡添加", "/card_add", menu2, 323));
		session.save(new Privilege("充值卡修改", "/card_edit", menu2, 324));
		
		// --------------------
				menu = new Privilege("消息管理", null, null, 4);
				menu1 = new Privilege("收件箱", "/message_listIn", menu, 41);
				menu2 = new Privilege("发件箱", "/message_listOut", menu, 42);
				menu3 = new Privilege("发送消息", "/message_addUI", menu, 43);
				session.save(menu);
				session.save(menu1);
				session.save(menu2);
				session.save(menu3);
				session.save(new Privilege("消息列表", "/message_listIn", menu1, 411));
				session.save(new Privilege("消息删除", "/message_deleteIn", menu1, 412));
				session.save(new Privilege("发送消息", "/message_add", menu1, 413));
				session.save(new Privilege("修改状态", "/message_edit", menu1, 414));
				session.save(new Privilege("消息列表", "/message_listOut", menu2, 421));
				session.save(new Privilege("消息删除", "/message_deleteOut", menu2, 422));
				session.save(new Privilege("发送消息", "/message_add", menu2, 423));
				
	}

	public static void main(String[] args) {
		ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
		Installer installer = (Installer) ac.getBean("installer");
		installer.install();
	}
}
