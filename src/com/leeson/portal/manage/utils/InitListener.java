package com.leeson.portal.manage.utils;

import java.util.Collection;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.leeson.portal.core.utils.SpringContextHelper;
import com.leeson.portal.manage.domain.Privilege;
import com.leeson.portal.manage.service.PrivilegeService;

public class InitListener implements ServletContextListener {


	public void contextInitialized(ServletContextEvent sce) {
		// 获取容器与相关的Service对象
//		ApplicationContext ac = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
//		PrivilegeService privilegeService = (PrivilegeService) ac.getBean("privilegeServiceImpl");
		
		
		PrivilegeService privilegeService = (PrivilegeService) SpringContextHelper.getBean("privilegeServiceImpl");

		// 准备数据：topPrivilegeList
		List<Privilege> topPrivilegeList = privilegeService.findTopList();
		sce.getServletContext().setAttribute("topPrivilegeList", topPrivilegeList);
		System.out.println("------------> 已准备数据topPrivilegeList <------------");
		
		// 准备数据：allPrivilegeUrls
		Collection<String> allPrivilegeUrls=privilegeService.getAllPrivilegeUrls();
		sce.getServletContext().setAttribute("allPrivilegeUrls", allPrivilegeUrls);
		System.out.println("------------> 已准备数据allPrivilegeUrls <------------");
		
	}

	public void contextDestroyed(ServletContextEvent arg0) {

	}
}
