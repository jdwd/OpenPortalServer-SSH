/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 权限
* This class is used for ...  
* @author LeeSon  QQ:25901875
* @version 1.0, 2015年4月22日 上午12:15:56
 */
public class Privilege implements Serializable{
	
	private static final long serialVersionUID = -6469502344433581587L;
	private Long id;
	private String name;
	private String url;
	private Set<Role> roles=new HashSet<Role>();
	private Privilege parent;
	private Set<Privilege> children=new HashSet<Privilege>();
	private int position;
	
	
	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public Privilege() {
	}

	public Privilege(String name, String url, Privilege parent) {
		this.name = name;
		this.url = url;
		this.parent = parent;
	}
	
	public Privilege(String name, String url, Privilege parent,int position) {
		this.name = name;
		this.url = url;
		this.parent = parent;
		this.position = position;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public Privilege getParent() {
		return parent;
	}
	public void setParent(Privilege parent) {
		this.parent = parent;
	}
	public Set<Privilege> getChildren() {
		return children;
	}
	public void setChildren(Set<Privilege> children) {
		this.children = children;
	}
	
	
	
	

}
