package com.leeson.portal.manage.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 
* This class is used for ...  接入用户
* @author LeeSon  QQ:25901875
* @version 1.0, 2015年7月31日 上午9:15:38
 */
public class LinkRecord implements Serializable {
	
	private static final long serialVersionUID = -2226378006223532460L;
	
	private Long id;
    private String ip;
    private String loginName;
    private String state;
    private Date startDate;
    private Date endDate;
    private Long time;
    private Long uid;
    
    
	public LinkRecord() {
		super();
	}

	public LinkRecord(String ip, String loginName, String state,
			Date startDate, Long uid) {
		super();
		this.ip = ip;
		this.loginName = loginName;
		this.state = state;
		this.startDate = startDate;
		this.uid = uid;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Long getTime() {
		return time;
	}
	public void setTime(Long time) {
		this.time = time;
	}
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	@Override
	public String toString() {
		return "LinkRecord [id=" + id + ", ip=" + ip + ", loginName="
				+ loginName + ", state=" + state + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", time=" + time + ", uid=" + uid
				+ "]";
	}
	
    
	
	
	
	
}
