package com.leeson.portal.manage.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 
* This class is used for ...  接入用户
* @author LeeSon  QQ:25901875
* @version 1.0, 2015年7月31日 上午9:15:38
 */
public class Card implements Serializable {
	
	private static final long serialVersionUID = 4887899881356181134L;
	
	private Long id;
	private String name; // 名称
	private String description; // 说明
	private String cdKey; 
	private Long payTime;
	private String payType; //#{2:'计时',3:'买断'}
	private String categoryType;  //#{0:'包时卡',1:'日卡',2:'月卡',3:'年卡'}
	private String state;  //#{0:'新卡',1:'已售出',2:'已激活'}
	private String accountName;
	private Long accountId;
	private Date payDate;
	
	
	public Date getPayDate() {
		return payDate;
	}
	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCdKey() {
		return cdKey;
	}
	public void setCdKey(String cdKey) {
		this.cdKey = cdKey;
	}
	public Long getPayTime() {
		return payTime;
	}
	public void setPayTime(Long payTime) {
		this.payTime = payTime;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	

}
