package com.leeson.portal.manage.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 
* This class is used for ...  接入用户
* @author LeeSon  QQ:25901875
* @version 1.0, 2015年7月31日 上午9:15:38
 */
public class LogRecord implements Serializable {
	
	
	private static final long serialVersionUID = 1403910370733836702L;
	
	private Long id;
    private String info;
	private Date rec_date;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public Date getRec_date() {
		return rec_date;
	}
	public void setRec_date(Date rec_date) {
		this.rec_date = rec_date;
	}
	
	
	
}
