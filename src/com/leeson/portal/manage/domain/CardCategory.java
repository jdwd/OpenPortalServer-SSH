package com.leeson.portal.manage.domain;

import java.io.Serializable;

/**
 * 
* This class is used for ...  接入用户
* @author LeeSon  QQ:25901875
* @version 1.0, 2015年7月31日 上午9:15:38
 */
public class CardCategory implements Serializable {
	
	private static final long serialVersionUID = 4995733685779578305L;
	
	private Long id;
	private String name; // 名称
	private String description; // 说明
	private Long time;
	private String state;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getTime() {
		return time;
	}
	public void setTime(Long time) {
		this.time = time;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
	

}
