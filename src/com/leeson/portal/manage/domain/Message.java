package com.leeson.portal.manage.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 
* This class is used for ...  接入用户
* @author LeeSon  QQ:25901875
* @version 1.0, 2015年7月31日 上午9:15:38
 */
public class Message implements Serializable {
	
	private static final long serialVersionUID = -3575427116482662495L;
	
	private Long id;
	private String fromid;
	private String toid;
    private String title; 
	private String description;
	private Date date;
	private String state;
	private String ip;
	private String toname;
	private String fromname;
	private String delin;
	private String delout;
	
	
	
	
	public String getDelin() {
		return delin;
	}
	public void setDelin(String delin) {
		this.delin = delin;
	}
	public String getDelout() {
		return delout;
	}
	public void setDelout(String delout) {
		this.delout = delout;
	}
	public String getToname() {
		return toname;
	}
	public void setToname(String toname) {
		this.toname = toname;
	}
	public String getFromname() {
		return fromname;
	}
	public void setFromname(String fromname) {
		this.fromname = fromname;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFromid() {
		return fromid;
	}
	public void setFromid(String fromid) {
		this.fromid = fromid;
	}
	public String getToid() {
		return toid;
	}
	public void setToid(String toid) {
		this.toid = toid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
	
	
}
