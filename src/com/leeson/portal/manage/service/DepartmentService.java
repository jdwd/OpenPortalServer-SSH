/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.service;

import java.util.List;

import com.leeson.portal.manage.base.BaseService;
import com.leeson.portal.manage.domain.Department;

public interface DepartmentService  extends BaseService<Department>{

	List<Department> findTopList();

	List<Department> findChildrenList(long parentId);

}
