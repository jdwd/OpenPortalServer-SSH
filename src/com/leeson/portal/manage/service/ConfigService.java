/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.service;

import com.leeson.portal.manage.domain.Config;
import com.leeson.portal.manage.base.BaseService;

public interface ConfigService  extends BaseService<Config>{

}
