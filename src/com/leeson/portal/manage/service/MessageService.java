/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.service;

import com.leeson.portal.manage.base.BaseService;
import com.leeson.portal.manage.domain.Message;

public interface MessageService extends BaseService<Message>{
	
}
