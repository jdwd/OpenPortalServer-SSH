/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.service.impl;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

import com.leeson.portal.manage.base.BaseServiceImpl;
import com.leeson.portal.manage.domain.Account;
import com.leeson.portal.manage.service.AccountService;

@Service
public class AccountServiceImpl extends BaseServiceImpl<Account> implements AccountService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5924327439200883651L;

	@Override
	public void initPassword(Long id) {
		Account account=getById(id);
		String md5Digest = DigestUtils.md5Hex("1234");
		account.setPassword(md5Digest);
		update(account);
	}

	@Override
	public Account findByLoginNameAndPassword(String loginName, String password) {
		String md5Digest = DigestUtils.md5Hex(password);
		List<Account> users=findByCondition("where loginName='"
				+ loginName + "' and password='"
				+ md5Digest + "'");
		if(users.size()==0){
			return null;
		}else{
			return users.get(0);
		}
		
	}

	@Override
	public Account login(String loginName, String password) {
		String md5Digest = DigestUtils.md5Hex(password);
		List<Account> users=findByCondition("where loginName='"
				+ loginName + "' and password='"
				+ md5Digest + "' and state<>'0'");
		if(users.size()==0){
			return null;
		}else{
			return users.get(0);
		}
	}

	@Override
	public boolean checkLoginName(String loginName) {
		List<Account> users=findByCondition("where loginName='"
				+ loginName + "' ");
		if(users.size()==0){
			return true;
		}else{
			return false;
		}
	}



}
