/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.leeson.portal.manage.base.BaseServiceImpl;
import com.leeson.portal.manage.domain.Department;
import com.leeson.portal.manage.service.DepartmentService;

@Service
public class DepartmentServiceImpl extends BaseServiceImpl<Department> implements DepartmentService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2870704673030817309L;

	@Override
	public List<Department> findTopList() {
		return findByCondition("where parentId IS NULL");
	}

	@Override
	public List<Department> findChildrenList(long parentId) {
		return findByCondition("where parentId = "+parentId);
	}

	
}
