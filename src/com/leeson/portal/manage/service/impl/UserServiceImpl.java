/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.service.impl;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

import com.leeson.portal.manage.base.BaseServiceImpl;
import com.leeson.portal.manage.domain.User;
import com.leeson.portal.manage.service.UserService;

@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5924327439200883651L;

	@Override
	public void initPassword(Long id) {
		User user=getById(id);
		String md5Digest = DigestUtils.md5Hex("1234");
		user.setPassword(md5Digest);
		update(user);
	}

	@Override
	public User findByLoginNameAndPassword(String loginName, String password) {
		String md5Digest = DigestUtils.md5Hex(password);
		List<User> users=findByCondition("where loginName='"
				+ loginName + "' and password='"
				+ md5Digest + "'");
		if(users.size()==0){
			return null;
		}else{
			return users.get(0);
		}
		
	}

	@Override
	public boolean checkLoginName(String loginName) {
		List<User> users=findByCondition("where loginName='"
				+ loginName + "' ");
		if(users.size()==0){
			return true;
		}else{
			return false;
		}
	}


}
