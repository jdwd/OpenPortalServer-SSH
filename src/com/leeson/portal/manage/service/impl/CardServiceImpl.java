/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.service.impl;


import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.leeson.portal.manage.base.BaseServiceImpl;
import com.leeson.portal.manage.domain.Card;
import com.leeson.portal.manage.service.CardService;

@Service
public class CardServiceImpl extends BaseServiceImpl<Card> implements CardService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8990118899986690866L;

	@Override
	public void creatCard(String name, String description, Long payTime,
			String payType, String categoryType, int cardCount) {
		
		for(int i=0;i<cardCount;i++){
			Card newCard=new Card();
			newCard.setCategoryType(categoryType);
			newCard.setCdKey(UUID.randomUUID().toString());
			newCard.setDescription(description);
			newCard.setName(name);
			newCard.setPayTime(payTime);
			newCard.setPayType(payType);
			newCard.setState(String.valueOf(0));
			save(newCard);
			
		}
	}

	@Override
	public Card pay(String cardKey) {
		List<Card> cards=findByCondition("where cdKey='"
				+ cardKey + "' and state<>'2' ");
		if(cards.size()==0){
			return null;
		}else{
			return cards.get(0);
		}
	}

	

	
	

}
