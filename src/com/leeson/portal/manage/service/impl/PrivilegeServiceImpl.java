/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.service.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.springframework.stereotype.Service;

import com.leeson.portal.manage.base.BaseServiceImpl;
import com.leeson.portal.manage.domain.Privilege;
import com.leeson.portal.manage.service.PrivilegeService;

@Service
public class PrivilegeServiceImpl extends BaseServiceImpl<Privilege> implements PrivilegeService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5193740438586107383L;

	@Override
	public void save(Privilege entity) {
		super.save(entity);
		entity.setPosition(entity.getId().intValue());
	}
	
	@Override
	public List<Privilege> findTopList() {
		return findByCondition("where parentId IS NULL ORDER BY position");
	}

	@Override
	public Collection<String> getAllPrivilegeUrls() {
		List<Privilege> privileges=findByCondition("WHERE url IS NOT NULL");
		Collection<String> allPriviUrls=new HashSet<String>(privileges.size());
		for (Privilege privilege : privileges) {
			allPriviUrls.add(privilege.getUrl());
		}
		return allPriviUrls;
	}

	@Override
	public List<Privilege> findChildrenList(long parentId) {
		return findByCondition("where parentId = " + parentId
				+ " ORDER BY position");
	}

	@Override
	public void moveUp(long id) {
		// 找出相关的Forum
		Privilege forum = getById(id); // 当前要移动的Forum
		Privilege other = null;
				try {
					other = (Privilege) baseDao.getSession().createQuery(// 我上面的那个Forum
							"from Privilege WHERE position<? and parentId=? ORDER BY position DESC")//
							.setParameter(0, forum.getPosition())//
							.setParameter(1, forum.getParent().getId())//
							.setFirstResult(0)//
							.setMaxResults(1)//
							.uniqueResult();
				} catch (Exception e) {
					other = (Privilege) baseDao.getSession().createQuery(// 我上面的那个Forum
							"from Privilege position<? and parentId IS NULL ORDER BY position DESC")//
							.setParameter(0, forum.getPosition())//
							.setFirstResult(0)//
							.setMaxResults(1)//
							.uniqueResult();
				}
				
				// 最上面的不能上移
				if (other == null) {
					return;
				}

				// 交换position的值
				int temp = forum.getPosition();
				forum.setPosition(other.getPosition());
				other.setPosition(temp);

				// 更新到数据中（可以不写，因为对象现在是持久化状态）
				baseDao.getSession().update(forum);
				baseDao.getSession().update(other);
	}

	@Override
	public void moveDown(long id) {
		// 找出相关的Forum
		Privilege forum = getById(id); // 当前要移动的Forum
		Privilege other = null;
				try {
					other = (Privilege) baseDao.getSession().createQuery(// 我上面的那个Forum
							"from Privilege WHERE position>? and parentId=? ORDER BY position DESC")//
							.setParameter(0, forum.getPosition())//
							.setParameter(1, forum.getParent().getId())//
							.setFirstResult(0)//
							.setMaxResults(1)//
							.uniqueResult();
				} catch (Exception e) {
					other = (Privilege) baseDao.getSession().createQuery(// 我上面的那个Forum
							"from Privilege WHERE position>? and parentId IS NULL ORDER BY position DESC")//
							.setParameter(0, forum.getPosition())//
							.setFirstResult(0)//
							.setMaxResults(1)//
							.uniqueResult();
				}

				// 最下面的不能下移
				if (other == null) {
					return;
				}

				// 交换position的值
				int temp = forum.getPosition();
				forum.setPosition(other.getPosition());
				other.setPosition(temp);

				// 更新到数据中（可以不写，因为对象现在是持久化状态）
				baseDao.getSession().update(forum);
				baseDao.getSession().update(other);
	}
}
