/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.service.impl;

import org.springframework.stereotype.Service;

import com.leeson.portal.manage.base.BaseServiceImpl;
import com.leeson.portal.manage.domain.LogRecord;
import com.leeson.portal.manage.service.LogRecordService;

@Service
public class LogRecordServiceImpl extends BaseServiceImpl<LogRecord> implements LogRecordService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 927470298106537823L;

	
}
