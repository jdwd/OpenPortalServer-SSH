/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.service.impl;


import org.springframework.stereotype.Service;

import com.leeson.portal.manage.base.BaseServiceImpl;
import com.leeson.portal.manage.domain.CardCategory;
import com.leeson.portal.manage.service.CardCategoryService;

@Service
public class CardCategoryServiceImpl extends BaseServiceImpl<CardCategory> implements CardCategoryService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -958100551718831890L;

	

}
