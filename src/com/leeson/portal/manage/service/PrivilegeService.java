/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.manage.service;

import java.util.Collection;
import java.util.List;

import com.leeson.portal.manage.base.BaseService;
import com.leeson.portal.manage.domain.Privilege;

public interface PrivilegeService extends BaseService<Privilege>{

	/**
	 * 查询所有顶级的权限
	 * 
	 * @return
	 */
	List<Privilege> findTopList();
	
	/**
	 * 查询所有权限对应的URL集合（不重复）
	 * @return
	 */
	Collection<String> getAllPrivilegeUrls();
	
	List<Privilege> findChildrenList(long parentId);
	
	void moveUp(long id);
	void moveDown(long id);

}
