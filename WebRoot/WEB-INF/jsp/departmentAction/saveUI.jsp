<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>用户分类添加</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>

<script type="text/javascript">
    KE.show({
        id : 'content7',
        cssPath : './index.css'
    });
  </script>
  
<script type="text/javascript">
$(document).ready(function(e) {
    $(".select1").uedSelect({
		width : 345			  
	});
	$(".select2").uedSelect({
		width : 167  
	});
	$(".select3").uedSelect({
		width : 100
	});
});
</script>
</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/home_right.action">首页</a></li>
    <li><a href="${pageContext.request.contextPath}/department_list.action">用户分类管理</a></li>
    <li><a href="${pageContext.request.contextPath}/department_addUI.action">系统用户分类添加</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
    <li><a href="#tab1" class="selected">系统用户分类添加</a></li>
  	</ul>
    </div> 
    
  	<div id="tab1" class="tabson">
    
    <div class="formtext"><b>本页面为系统用户分类添加页面！</b></div>
    
    <s:form action="department_%{id == null ? 'add' : 'edit'}">
        <s:hidden name="id"></s:hidden>
        
    <ul class="forminfo">
    
    <li><label>上级分类<b>*</b></label>
    <div class="vocation">
    <s:select list="departmentList"  cssClass="select1" 
                        listKey="id" listValue="name" 
                        headerKey="" headerValue="请选择上级分类" 
                        name="parentId" cssclass="SelectStyle"/>
   </div>
    【不选择则为顶级分类】</li>
    
    <li><label>分类名称<b>*</b></label>
    <s:textfield  name="name" cssClass="dfinput" style="width:400px;"/></li>
   
    
    
    <li><label>详细信息<b></b></label>
    <s:textarea name="description" cssClass="textinput"></s:textarea></li>
    
    
    
    
    
    <li><label>&nbsp;</label>
    <s:submit cssClass="btn" value="保存" name="保存"></s:submit></li>
    </ul>
    </s:form>
    </div> 
    
    
    </div> 
 
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    
    
    
    </div>


</body>

</html>