<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值卡分类</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>


  
<script type="text/javascript">
$(document).ready(function(e) {
    $(".select1").uedSelect({
		width : 345			  
	});
	$(".select2").uedSelect({
		width : 167  
	});
	$(".select3").uedSelect({
		width : 100
	});
	
	
	
	
	$(function(){                            
        $("#time").keydown(function(e){

// 注意此处不要用keypress方法，否则不能禁用　Ctrl+V 与　Ctrl+V,具体原因请自行查找keyPress与keyDown区分，十分重要，请细查

                if ($.browser.msie) {  // 判断浏览器

                       if ( ((event.keyCode > 47) && (event.keyCode < 58)) || (event.keyCode == 8) ) {
                    	   // 判断键值  

                              return true;  
                        } else {  
                              return false;  
                       }
                 } else {  
                    if ( ((e.which > 47) && (e.which < 58)) || (e.which == 8) || (event.keyCode == 17) || ((event.keyCode >= 96) && (event.keyCode <= 105)) ) {  
                             return true;  
                     } else {  
                             return false;  
                     }  
                 }}).focus(function() {
                         this.style.imeMode='disabled';   // 禁用输入法,禁止输入中文字符

        });
});
	
	
});
</script>
</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/home_right.action">首页</a></li>
    <li><a href="${pageContext.request.contextPath}/cardCategory_list.action">充值卡分类管理</a></li>
    <li><a href="${pageContext.request.contextPath}/cardCategory_addUI.action">充值卡分类添加</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
    <li><a href="#tab1" class="selected">充值卡分类编辑</a></li>
  	</ul>
    </div> 
    
  	<div id="tab1" class="tabson">
    
    <div class="formtext">
    
    <b>本页面为充值卡分类编辑页面！</b>
    
    </div>
    
    <s:form action="cardCategory_%{id == null ? 'add' : 'edit'}">
       <s:hidden name="id"></s:hidden>
        
    <ul class="forminfo">
    
    <li><label>名称<b>*</b></label>
    <s:textfield  name="name" cssClass="dfinput" style="width:400px;"/></li>
    
    <li><label>类型<b>*</b></label>
    <div class="vocation">
    <s:select name="state" cssClass="select1" 
    list="#{0:'包时卡',1:'日卡',2:'月卡',3:'年卡'}"  listKey="key" listValue="value"  headerKey="0" headerValue="请选择类型"/>
    </div>
    </li>
    
    <li><label>计数<b>*</b></label>
    <s:textfield  name="time" id="time" cssClass="dfinput" style="width:400px;"/></li>
    
    <li><label>详细信息<b></b></label>
    <s:textarea name="description" cssClass="textinput"></s:textarea></li>
    
    <li><label>&nbsp;</label>
    <s:submit cssClass="btn" value="保存" name="保存"></s:submit></li>
    </ul>
    </s:form>
    </div> 
    
    
    </div> 
 
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    
    
    
    </div>


</body>

</html>