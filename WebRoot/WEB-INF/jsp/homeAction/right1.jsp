<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>主界面</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jsapi.js"></script>
<script type="text/javascript" src="js/format+zh_CN,default,corechart.I.js"></script>		
<script type="text/javascript" src="js/jquery.gvChart-1.0.1.min.js"></script>
<script type="text/javascript" src="js/jquery.ba-resize.min.js"></script>

<script type="text/javascript">
		gvChartInit();
		jQuery(document).ready(function(){

		jQuery('#myTable5').gvChart({
				chartType: 'PieChart',
				gvSettings: {
					vAxis: {title: 'No of players'},
					hAxis: {title: 'Month'},
					width: 650,
					height: 250
					}
			});
		});
		</script>
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/home_right.action">首页</a></li>
    <li><a href="${pageContext.request.contextPath}/home_right.action">后台主界面</a></li>
    </ul>
    </div>
    
    
    <div class="mainbox">
    
    <div class="mainleft">
    
    
    <div class="leftinfo">
    <div class="listtitle"><a href="${pageContext.request.contextPath}/home_right.action" class="more1">刷新</a>数据统计</div>
        
    <div class="maintj">  
    <table id='myTable5'>
				<caption>用户情况统计</caption>
				<thead>
					<tr>
						<th></th>
						<s:if test="#config==1">
						<th>在线：${online_count }</th>
						<th>离线：${outline_count }</th>
						</s:if>
						<s:else>
						<th>外接在线用户：${online_count }</th>
						<th></th>
						</s:else>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</thead>
					<tbody>
					<tr>
						<th>2015</th>
						<s:if test="#config==1">
						<td>${online_count }</td>
						<td>${outline_count }</td>
						</s:if>
						<s:else>
						<td>${online_count }</td>
						<td>0</td>
						</s:else>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
				</tbody>
			</table>  
    </div>
    
    </div>
    <!--leftinfo end-->
    
    
    <div class="leftinfos">
    
   
    <div class="infoleft">
    
    <div class="listtitle"><a href="${pageContext.request.contextPath}/home_right.action" class="more1">刷新</a>用户日志</div>    
    <ul class="newlist">
    <s:iterator value="#operationRecords">
    <li><a href="#">${info }</a><b>${rec_date }</b></li>
    </s:iterator>
    
    
    </ul>   
    
    </div>
    
    
    <div class="inforight">
    <div class="listtitle"><a href="${pageContext.request.contextPath}/home_right.action" class="more1">刷新</a>快捷图标</div>
    
    <ul class="tooli">
    <li><a href="${pageContext.request.contextPath}/config_show.action"><span><img src="images/d01.png" /></span><p>系统设置</p></a></li>
    <li><a href="${pageContext.request.contextPath}/department_list.action"><span><img src="images/d02.png" /></span><p>部门管理</p></a></li>
    <li><a href="${pageContext.request.contextPath}/role_list.action"><span><img src="images/d03.png" /></span><p>角色管理</p></a></li>
    <li><a href="${pageContext.request.contextPath}/user_list.action"><span><img src="images/d04.png" /></span><p>系统用户</p></a></li>
    <li><a href="${pageContext.request.contextPath}/user_list.action"><span><img src="images/d05.png" /></span><p>接入用户</p></a></li>
    <li><a href="${pageContext.request.contextPath}/home_right.action"><span><img src="images/d06.png" /></span><p>刷新数据</p></a></li>
    <li><a href="${pageContext.request.contextPath}/user_logout.action" target="_parent"><span><img src="images/d07.png" /></span><p>退出</p></a></li>    
    </ul>
    
    </div>
    
    
    </div>
    
    
    </div>
    <!--mainleft end-->
    
    
    <div class="mainright">
    
    
    <div class="dflist">
    <div class="listtitle"><a href="${pageContext.request.contextPath}/home_right.action" class="more1">刷新</a>认证日志</div>    
    <ul class="newlist">
    <s:iterator value="#logRecords">
    <li><a href="#">${info }</a></li>
    </s:iterator>
    </ul>        
    </div>
    
    
    <div class="dflist1">
    <div class="listtitle"><a href="${pageContext.request.contextPath}/home_right.action" class="more1">刷新</a>本地接入用户统计</div>    
    <ul class="newlist">
    <li><i>系统用户总数： </i> ${acc_count }人</li>
    <s:if test="#config==1">
    <li><i>在线： </i> ${online_count }人</li>
    <li><i>离线： </i> ${outline_count }人</li>
    </s:if>
    <li><i>锁定用户： </i> ${lock_count }人</li>
    <li><i>正常用户： </i> ${true_count }人</li>  
    <s:else><li><i>外接在线用户： </i> ${online_count }人</li></s:else> 
    </ul>        
    </div>
    
    

    
    
    </div>
    <!--mainright end-->
    
    
    </div>



</body>
<script type="text/javascript">
	setWidth();
	$(window).resize(function(){
		setWidth();	
	});
	function setWidth(){
		var width = ($('.leftinfos').width()-12)/2;
		$('.infoleft,.inforight').width(width);
	}
</script>
</html>
