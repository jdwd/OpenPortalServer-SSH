﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TOP</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="js/jquery.js"></script>
<script type="text/javascript">
$(function(){	
	//顶部导航切换
	$(".nav li a").click(function(){
		$(".nav li a.selected").removeClass("selected")
		$(this).addClass("selected");
	})	
})	
</script>

<script type="text/javascript" language="javascript">  
    $(document).ready(function () {  
      setInterval("startRequest()", 5000);  
      startRequest();
    });  
    function startRequest() {  
    	jQuery.getJSON('${pageContext.request.contextPath}/ajax_msgCount.action', null,
                function(data) {

                    var member = eval("("+data+")");    //包数据解析为json 格式    
                    
                    $('#msgCount').html("<a href=\"${pageContext.request.contextPath}/message_listIn.action?state=1\" title=\"你有"+member.msgCount+"条未读消息！！\" target=\"rightFrame\">"+member.msgCount+"</a>");
                    });
	    
		
    }  
</script>  

</head>

<body style="background:url(images/topbg.gif) repeat-x;">

    <div class="topleft">
    <a href="javascript: window.parent.rightFrame.location.reload(true);"><img src="images/logo.png" title="系统首页" /></a>
    </div>
        
    <ul class="nav">
    <li><a href="${pageContext.request.contextPath}/home_right.action" target="rightFrame" class="selected"><img src="images/icon01.png" title="主页" /><h2>主页</h2></a></li>
    <li><a href="${pageContext.request.contextPath}/role_list.action" target="rightFrame"><img src="images/icon02.png" title="角色管理" /><h2>角色管理</h2></a></li>
    <li><a href="${pageContext.request.contextPath}/department_list.action"  target="rightFrame"><img src="images/icon03.png" title="部门管理" /><h2>部门管理</h2></a></li>
    <li><a href="${pageContext.request.contextPath}/user_list.action"  target="rightFrame"><img src="images/icon04.png" title="系统用户" /><h2>系统用户</h2></a></li>
    <li><a href="${pageContext.request.contextPath}/account_list.action" target="rightFrame"><img src="images/icon05.png" title="接入用户" /><h2>接入用户</h2></a></li>
    <li><a href="${pageContext.request.contextPath}/config_show.action"  target="rightFrame"><img src="images/icon06.png" title="系统设置" /><h2>系统设置</h2></a></li>
    </ul>
            
    <div class="topright">    
    <ul>
    <li><span><img src="images/help.png" title="帮助"  class="helpimg"/></span><a href="#">帮助</a></li>
    <li><a href="#">关于</a></li>
    <li><a href="${pageContext.request.contextPath}/user_logout.action" target="_parent">退出</a></li>
    </ul>
     
    <div class="user">
    <span>${user.name }</span>
    <i>欢迎您！</i>
    <b id="msgCount"><a href="${pageContext.request.contextPath}/message_listIn.action?state=1" title="你有${msgCount }条未读消息！！">${msgCount }</a></b>
    </div>    
    
    </div>

</body>
</html>
