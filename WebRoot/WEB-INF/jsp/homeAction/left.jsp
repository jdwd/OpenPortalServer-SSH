﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>导航菜单</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="js/jquery.js"></script>

<script type="text/javascript">
$(function(){	
	//导航切换
	$(".menuson li").click(function(){
		$(".menuson li.active").removeClass("active")
		$(this).addClass("active");
	});
	
	$('.title').click(function(){
		var $ul = $(this).next('ul');
		$('dd').find('ul').slideUp();
		if($ul.is(':visible')){
			$(this).next('ul').slideUp();
		}else{
			$(this).next('ul').slideDown();
		}
	});
})	
</script>


</head>

<body style="background:#f0f9fd;">
	<div class="lefttop"><span></span>导航菜单</div>
    
    <dl class="leftmenu">
    <%-- 显示一级菜单 --%>
				<s:iterator value="#application.topPrivilegeList">
					<s:if test="#session.user.hasPrivilegeByName(name)">    
    <dd>
    <div class="title">
    <span><img src="images/leftico01.png" /></span>${name}
    </div>
    	<ul class="menuson">
    	<%-- 显示二级菜单 --%>
							<s:iterator value="children">
								<s:if test="#session.user.hasPrivilegeByName(name)">
        <li><cite></cite><a href="${pageContext.request.contextPath}${url}.action" target="rightFrame">${name}</a><i></i></li>
        <%-- <li class="active"><cite></cite><a href="right.html" target="rightFrame">数据列表</a><i></i></li> --%>
        </s:if>
							</s:iterator>
        </ul>    
    </dd>
        
    
    </s:if>
				</s:iterator>  
    
    </dl>
    
</body>
</html>
