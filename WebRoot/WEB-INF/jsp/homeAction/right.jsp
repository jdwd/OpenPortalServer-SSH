<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>主界面</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jsapi.js"></script>
<script type="text/javascript" src="js/format+zh_CN,default,corechart.I.js"></script>		

<script type="text/javascript" src="js/jquery.ba-resize.min.js"></script>
<script type="text/javascript" src="js/jquery-mini.js"></script>


<!--  

<script type="text/javascript" language="javascript">  
    $(document).ready(function () {  
      //  setInterval("startRequest()", 3000);  
      startRequest();
    });  
    function startRequest() {  
    	jQuery.getJSON('${pageContext.request.contextPath}/ajax_getLogs.action', null,
                function(data) {

                    var member = eval("("+data+")");    //包数据解析为json 格式    
                    $.each(member,function(i,value) {
                    	//alert(i); 
                    	//alert(value); 
                    	$("#operationRecords").append("<li><a href=\"#\">" + i + "</a><b>"+ value +"</b></li>");  
                    	});
                    });
	    
		
    }  
</script>  


-->


<s:if test="#config==1">
<script type="text/javascript">
            $(function () {
                $('#container1').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: true,
                        marginRight: 5,
                        marginLeft: 5,
                        marginBottom: 20,
                        width: 274,
    					height: 238,
                        events: {
                            load: function() {

                                // 若有第3条线，则添加
                                // var series_other_property = this.series[2]
                                // 并在 series: [] 中添加相应的 (name, data) 对
                                var accounts = this.series[0];

                                // 定时器，每隔1000ms刷新曲线一次
                                setInterval(function() {

                                    // 使用JQuery从后台Servlet获取
                                    // JSON格式的数据，
                                    // 如 "{"online_count": 80,"mem": 10}"
                                    jQuery.getJSON('${pageContext.request.contextPath}/ajax_fenleiCount.action', null,
                                    function(data) {

                                        
                                        var member = eval("("+data+")");    //包数据解析为json 格式 
                                        

                                        //定义一个数组
                                        browsers = [],
                        
                                        browsers.push(['在线：'+member.online_count+'人',member.online_count]);
                                        browsers.push(['离线：'+member.outline_count+'人',member.outline_count]);
                        //设置数据
                        accounts.setData(browsers);
                        
                        
                        $('#li1').html("<i>系统用户总数：  </i> "+member.acc_count+" 人");
                        $('#li2').html("<i>正常用户：  </i> "+member.true_count+" 人");
                        $('#li3').html("<i>锁定用户：  </i> "+member.lock_count+" 人");
                        $('#li4').html("<i>在线：  </i> "+member.online_count+" 人");
                        $('#li5').html("<i>离线：  </i> "+member.outline_count+" 人");
                                        
                                    });
                                },
                                2000/*启动间隔，单位ms*/
                                );
                            }
                        }
                    },
                    colors:[
                        'red',//第一个颜色，欢迎加入Highcharts学习交流群294191384
                        'blue',//第二个颜色
                        'yellow',//第三个颜色
                       '#1aadce', //。。。。
                           '#492970',
                           '#f28f43', 
                           '#77a1e5', 
                           '#c42525', 
                           '#a6c96a'
                      ],
                    title: {
                        text: '用户情况统计'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: '用户情况分析',
                        data: [
                            ['在线：${online_count }人',   ${online_count }],
                            ['离线：${outline_count }人',  ${outline_count }],
                        ]
                    }]
                });
            });
        </script>
</s:if>
<s:else>
<script type="text/javascript">
            $(function () {
                $('#container1').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: true,
                        marginRight: 5,
                        marginLeft: 5,
                        marginBottom: 20,
                        width: 274,
    					height: 238,
                        events: {
                            load: function() {

                                // 若有第3条线，则添加
                                // var series_other_property = this.series[2]
                                // 并在 series: [] 中添加相应的 (name, data) 对
                                var accounts = this.series[0];

                                // 定时器，每隔1000ms刷新曲线一次
                                setInterval(function() {

                                    // 使用JQuery从后台Servlet获取
                                    // JSON格式的数据，
                                    // 如 "{"online_count": 80,"mem": 10}"
                                    jQuery.getJSON('${pageContext.request.contextPath}/ajax_fenleiCount.action', null,
                                    function(data) {

                                        
                                        var member = eval("("+data+")");    //包数据解析为json 格式 
                                        

                                        //定义一个数组
                                        browsers = [],
                        
                                        browsers.push(['外接在线：'+member.online_count+'人',member.online_count]);
                        //设置数据
                        accounts.setData(browsers);
                        
                        
                        
                        $('#li1').html("<i>系统用户总数：  </i> "+member.acc_count+" 人");
                        $('#li2').html("<i>正常用户：  </i> "+member.true_count+" 人");
                        $('#li3').html("<i>锁定用户：  </i> "+member.lock_count+" 人");
                        $('#li4').html("<i>外接在线用户：  </i> "+member.online_count+" 人");
                        $('#li5').html("<i></i>");
                                        
                                    });
                                },
                                2000/*启动间隔，单位ms*/
                                );
                            }
                        }
                    },
                    colors:[
                        'red',//第一个颜色，欢迎加入Highcharts学习交流群294191384
                        'blue',//第二个颜色
                        'yellow',//第三个颜色
                       '#1aadce', //。。。。
                           '#492970',
                           '#f28f43', 
                           '#77a1e5', 
                           '#c42525', 
                           '#a6c96a'
                      ],
                    title: {
                        text: '用户情况统计'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                            }
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: '用户情况分析',
                        data: [['外接在线：${online_count }人',   ${online_count }],]
                    }]
                });
            });
        </script>
</s:else>


<script type="text/javascript">
            $(function() {
                $('#container').highcharts({
                    chart: {
                        type:'spline',
                        marginRight: 150,
                        marginLeft: 50,
                        marginBottom: 25,
                        width: 664,
    					height: 250,
                        animation: Highcharts.svg,
                        
                        events: {
                            load: function() {

                                // 若有第3条线，则添加
                                // var series_other_property = this.series[2]
                                // 并在 series: [] 中添加相应的 (name, data) 对
                                var series_online_count = this.series[0];

                                // 定时器，每隔1000ms刷新曲线一次
                                setInterval(function() {

                                    // 使用JQuery从后台Servlet获取
                                    // JSON格式的数据，
                                    // 如 "{"online_count": 80,"mem": 10}"
                                    jQuery.getJSON('${pageContext.request.contextPath}/ajax_onlineCount.action', null,
                                    function(data) {

                                        // 当前时间，为x轴数据
                                        var x = (new Date()).getTime();

                                        // y轴数据
                                        //alert(data);  
                                        var member = eval("("+data+")");    //包数据解析为json 格式    
                                        var online_count = member.online_count;
                                        

                                        // 更新曲线数据
                                        series_online_count.addPoint([x, online_count], true, true);
                                        
                                    });
                                },
                                2000/*启动间隔，单位ms*/
                                );
                            }
                        }
                    },
                    title: {
                        text: '在线用户数',
                        x: -20
                    },
                    xAxis: {
                        type: 'datetime',
                        tickPixelInterval: 150
                    },
                    yAxis: {
                        title: {
                            text: '在线用户数'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        valueSuffix: '人'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -10,
                        y: 100,
                        borderWidth: 0
                    },
                    series: [
                    // 第1条曲线的(name, data)对
                    {
                        name: '在线用户数',
                        data: (function() {
                            var data = [],
                            time = (new Date()).getTime(),
                            i;

                            // 给曲线y值赋初值0
                            for (i = -9; i <= 0; i++) {
                                data.push({
                                    x: time + i * 1000,
                                    y: 0
                                });
                            }
                            return data;
                        })()
                    }]
                });
            });
        </script>
        
        
        



</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/home_right.action">首页</a></li>
    <li><a href="${pageContext.request.contextPath}/home_right.action">后台主界面</a></li>
    </ul>
    </div>
    
    
    <div class="mainbox">
    
    <div class="mainleft">
    
    
    <div class="leftinfo">
    <div class="listtitle"><a href="${pageContext.request.contextPath}/home_right.action" class="more1">刷新</a>数据统计</div>
        
    <div class="maintj">  
    <table id='myTable5'>
	<div id="container" style="width:100%;height:250px;margin:0 auto;"></div>
	
	
    </table>  
    </div>
    
    </div>
    <!--leftinfo end-->
    
    
    <div class="leftinfos">
    
   
    <div class="infoleft">
    
    <div class="listtitle"><a href="${pageContext.request.contextPath}/home_right.action" class="more1">刷新</a>用户日志</div>    
    <ul class="newlist" id="operationRecords">
    <s:iterator value="#operationRecords">
    <li><a href="#">${info }</a><b>${rec_date }</b></li>
    </s:iterator>
    
    
    </ul>   
    
    </div>
    
    
    <div class="inforight">
    <div class="listtitle"><a href="${pageContext.request.contextPath}/home_right.action" class="more1">刷新</a>快捷图标</div>
    
    <ul class="tooli">
    <li><a href="${pageContext.request.contextPath}/config_show.action"><span><img src="images/d01.png" /></span><p>系统设置</p></a></li>
    <li><a href="${pageContext.request.contextPath}/department_list.action"><span><img src="images/d02.png" /></span><p>分类管理</p></a></li>
    <li><a href="${pageContext.request.contextPath}/role_list.action"><span><img src="images/d03.png" /></span><p>角色管理</p></a></li>
    <li><a href="${pageContext.request.contextPath}/user_list.action"><span><img src="images/d04.png" /></span><p>系统用户</p></a></li>
    <li><a href="${pageContext.request.contextPath}/account_list.action"><span><img src="images/d05.png" /></span><p>接入用户</p></a></li>
    <li><a href="${pageContext.request.contextPath}/user_logout.action" target="_parent"><span><img src="images/d07.png" /></span><p>退出</p></a></li>    
    </ul>
    
    </div>
    
    
    </div>
    
    
    </div>
    <!--mainleft end-->
    
    
    <div class="mainright">
    
    
    <div class="dflist">
    <div class="listtitle"><a href="${pageContext.request.contextPath}/home_right.action" class="more1">刷新</a>用户状态情况</div>    
    <table id='myTable51'>
	<div id="container1" style="width:100%;height:250px;margin:0 auto;"></div>
	
	
    </table>     
    </div>
    
    
    <div class="dflist1">
    <div class="listtitle"><a href="${pageContext.request.contextPath}/home_right.action" class="more1">刷新</a>本地接入用户统计</div>    
    <ul class="newlist">
    
    <li id="li1"><i>系统用户总数： </i> ${acc_count }人</li>
    <li id="li2"><i>正常用户： </i> ${true_count }人</li>
    <li id="li3"><i>锁定用户： </i> ${lock_count }人</li>
    <li id="li4"><i>在线： </i> ${online_count }人</li>
    <li id="li5"><i>离线： </i> ${outline_count }人</li>
    
    </ul>        
    </div>
    
    

    
    
    </div>
    <!--mainright end-->
    
    
    </div>



</body>
<script type="text/javascript">
	setWidth();
	$(window).resize(function(){
		setWidth();	
	});
	function setWidth(){
		var width = ($('.leftinfos').width()-12)/2;
		$('.infoleft,.inforight').width(width);
	}
</script>

<script type="text/javascript" src="js/highcharts.js"></script>
<script type="text/javascript" src="js/exporting.js"></script>



</html>
