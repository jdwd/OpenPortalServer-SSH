﻿<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>欢迎登录OpenPortalServer后台管理系统</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="js/jquery.js"></script>
<script src="js/cloud.js" type="text/javascript"></script>

<script language="javascript">
	$(function(){
    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
	$(window).resize(function(){  
    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
    })  
});  
</script> 
<script type="text/javascript">
		$(function(){
			document.forms[0].loginName.focus();
		});
		
		// 在被嵌套时就刷新上级窗口
		if(window.parent != window){
			window.parent.location.reload(true);
		}
	</script>
	
</head>

<body style="background-color:#1c77ac; background-image:url(images/light.png); background-repeat:no-repeat; background-position:center top; overflow:hidden;">



    <div id="mainBody">
      <div id="cloud1" class="cloud"></div>
      <div id="cloud2" class="cloud"></div>
    </div>  


<div class="logintop">    
    <span>OpenPortalServer后台管理系统</span>    
    <ul>
    <li><a href="#">回首页</a></li>
    <li><a href="#">帮助</a></li>
    <li><a href="#">关于</a></li>
    </ul>    
    </div>
    
    <div class="loginbody">
    
    <span class="systemlogo"></span> 
    <span style= "text-align:center; margin-top:35px;"><font color="red"><s:fielderror/></font></span>    
    <div class="loginbox">
    
    <s:form action="user_login" focusElement="loginNameInput">
    <ul>
    <li><s:textfield name="loginName"  id="loginNameInput" cssClass="loginuser"/></li>
    <li><s:password name="password" showPassword="false" cssClass="loginpwd"/></li>
    <li><s:submit cssClass="loginbtn" value="登录" name="登录"></s:submit><label><input name="" type="checkbox" value="" checked="checked" />记住密码</label><label><a href="#">忘记密码？</a></label></li>
    
    </ul>
    </s:form>
    
    </div>
    
    </div>
    
    
    
    <div class="loginbm">&copy; 2015 LeeSon 版权所有  <a href="http://25901875.qq.com">QQ:25901875</a>  OpenPortalServer</div>
	
    
</body>

</html>
