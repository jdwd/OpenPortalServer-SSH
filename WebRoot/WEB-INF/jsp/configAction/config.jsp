<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>系统设置</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>


  
<script type="text/javascript">
$(document).ready(function(e) {
    $(".select1").uedSelect({
		width : 345			  
	});
	$(".select2").uedSelect({
		width : 167  
	});
	$(".select3").uedSelect({
		width : 100
	});
});
</script>
</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/home_right.action">首页</a></li>
    <li><a href="${pageContext.request.contextPath}/config_show.action">系统设置</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
    <li><a href="#tab1" class="selected">接入设置</a></li>
  	</ul>
    </div> 
    
  	<div id="tab1" class="tabson">
    
    <div class="formtext"><b>本页面为系统对接设置，请保证与AC BAS等设备配置信息一致！</b></div>
    
    <s:form action="config_saveConfig">
        <s:hidden name="id"></s:hidden>
        
    <ul class="forminfo">
    <li><label>BAS IP地址<b>*</b></label>
    <s:textfield  name="bas_ip" cssClass="dfinput" style="width:400px;"/></li>
   
    <li><label>BAS 端口号<b>*</b></label>
    <s:textfield  name="bas_port" cssClass="dfinput" style="width:400px;"/></li>
    
    <li><label>服务器 端口号<b>*</b></label>
    <s:textfield  name="portal_port" cssClass="dfinput" style="width:400px;"/></li>
    
    <li><label>共享密钥<b>*</b></label>
    <s:textfield  name="sharedSecret" cssClass="dfinput" style="width:400px;"/></li>
    
    <li><label>设备品牌<b>*</b></label>
    <div class="vocation">
    <s:select name="bas" cssClass="select1" 
    list="#{0:'华为',1:'H3C'}"  listKey="key" listValue="value"  headerKey="0" headerValue="请选择设备类型"/>
    </div>
    </li>
    
    <li><label>portal版本<b>*</b></label>
    <div class="vocation">
    <s:select name="portalVer" cssClass="select1" 
    list="#{1:'Portal V1',2:'Portal V2'}"  listKey="key" listValue="value"  headerKey="2" headerValue="请选择版本号"/>
    </div>
    </li>
    
    <li><label>认证方式<b>*</b></label>
     <div class="vocation">
    <s:select name="authType" cssClass="select1" 
    list="#{1:'CHAP',0:'PAP'}"  listKey="key" listValue="value"  headerKey="1" headerValue="请选择认证方式"/>
    </div>
    
    
    </li>
    
    <li><label>超时设置<b>*</b></label>
    <div class="vocation">
    <s:select name="timeoutSec" cssClass="select1" 
    list="#{1:'1秒',2:'2秒',3:'3秒',4:'4秒',5:'5秒',6:'6秒',7:'7秒',8:'8秒',9:'9秒',10:'10秒'}"  listKey="key" listValue="value"  headerKey="3" headerValue="请选择时长"/>
    </div>
    </li>
    
    <li><label>日志记录<b>*</b></label>
    <div class="vocation">
    <s:select name="isdebug" cssClass="select1" 
    list="#{1:'打开',0:'关闭'}"  listKey="key" listValue="value"  headerKey="1" headerValue="请选择"/>
    </div>
    </li>
    
    <li><label>验证码设置<b>*</b></label>
    <div class="vocation">
    <s:select name="verifyCode" cssClass="select1" 
    list="#{1:'打开',0:'关闭'}"  listKey="key" listValue="value"  headerKey="1" headerValue="请选择"/>
    </div>
    </li>
    
    <li><label>用户心跳<b>*</b></label>
    <div class="vocation">
    <s:select name="userHeart" cssClass="select1" 
    list="#{1:'打开',0:'关闭'}"  listKey="key" listValue="value"  headerKey="1" headerValue="请选择"/>
    </div>
    </li>
    
   <li><label>超时重复次数<b>*</b></label>
    <div class="vocation">
    <s:select name="userHeartCount" cssClass="select1" 
    list="#{1:'1次',2:'2次',3:'3次',4:'4次',5:'5次',6:'6次',7:'7次',8:'8次',9:'9次'}"  listKey="key" listValue="value"  headerKey="3" headerValue="请选择"/>
    </div>
    </li>
    
    <li><label>计费检测周期<b>*</b></label>
    <div class="vocation">
    <s:select name="userHeartTime" cssClass="select1" 
    list="#{10:'测试10秒',300:'5分钟',600:'10分钟',1200:'20分钟',1800:'30分钟',3600:'1小时',7200:'2小时',10800:'3小时',14400:'4小时',21600:'6小时',43200:'12小时',86400:'24小时'}"  listKey="key" listValue="value"  headerKey="30" headerValue="请选择"/>
    </div>【重启后生效】
    </li>
    
    <li><label>认证方式<b>*</b></label>
    
    <div class="vocation">
    <s:select name="auth_interface" cssClass="select1" 
    list="#{0:'页面展示',1:'本地用户',2:'外部Radius',3:'APP【未实现】',4:'手机短信【未实现】',5:'微信关注【未实现】'}"  listKey="key" listValue="value"  headerKey="0" headerValue="请选择"/>
    </div>
    </li>
    
    
    <li><label>自助注册开关<b>*</b></label>
    <div class="vocation">
    <s:select name="accountAdd" cssClass="select1" 
    list="#{1:'打开',0:'关闭'}"  listKey="key" listValue="value"  headerKey="0" headerValue="请选择"/>
    </div>
    </li>
    
    
    <li><label>设备账号<b>*</b></label>
    <s:textfield  name="bas_user" cssClass="dfinput" style="width:400px;"/></li>
    
    <li><label>设备密码<b>*</b></label>
    <s:textfield  name="bas_pwd" cssClass="dfinput" style="width:400px;"/></li>
    
    <li><label>&nbsp;</label>
    <s:submit cssClass="btn" value="保存" name="保存"></s:submit></li>
    </ul>
    </s:form>
    </div> 
    
    
    
    
    
   
       
	</div> 
 
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    
    
    
    </div>


</body>

</html>