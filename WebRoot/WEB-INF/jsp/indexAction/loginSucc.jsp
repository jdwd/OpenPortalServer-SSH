<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%
String portalPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/";
String radiusPath = path+"/customer_login.action";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<script src="indexjs/jquery.js" type="text/javascript"></script>
<title>用户登录成功</title>
<link href="indexcss/wm_7.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
function _submit() {
	document.getElementById("msg").innerHTML = "正在请求认证，请稍等....";
	return true;
}
</script>


<script type="text/javascript">
var time_out=2;//自动提交的等待时间，单位：秒
var call_me="请联系管理员。";//忘记密码、没有账号的对话框文本
var weburl="";//登录成功后打开的第1个通告页面，网站必须带http://
var save_time=72;//保存账号信息的时间，单位：小时

function cWH(){
var isMobile = !!navigator.userAgent.match(/AppleWebKit.*Mobile.*/) && !!navigator.userAgent.match(/AppleWebKit/);

var pageWidth = document.body.clientWidth > document.documentElement.clientWidth ? document.body.clientWidth : document.documentElement.clientWidth;
var mobileMaxWidth = 768, pcMaxWidth = 480, pcMinWidth = 320;	
var mainWidth = isMobile ? (pageWidth>mobileMaxWidth ? mobileMaxWidth : pageWidth) : (pageWidth>pcMaxWidth ? pcMaxWidth : (pageWidth<pcMinWidth ? pcMinWidth : pageWidth));	
$("#main").width(mainWidth-2-2);

var pageHeight = document.body.clientHeight > document.documentElement.clientHeight ? document.body.clientHeight : document.documentElement.clientHeight;
var mainTop = pageHeight-$("#main").height();
	mainTop = mainTop<0 ? 0 : mainTop;
	mainTop = parseInt(mainTop/2,10);
	mainTop = (isMobile && mainTop>10) ? 10 : mainTop;
$("#main").css("margin-top",mainTop+"px");	
}


var ld="";
var init=0;
$(function(){
init = 1;
cWH();
setInterval(cWH,200);


$("#usr").focus();
});
</script>
    
</head>
<%
    String username=(String)session.getAttribute("username");
    String password=(String)session.getAttribute("password");
    String ip=(String)session.getAttribute("ip");
    String message="";
    String msg=(String)request.getAttribute("msg");
    if(msg!=null){
    	message=msg;
    }
    
    if(username==null){
    	request.setAttribute("msg", "您还没有登录，请先登录！");
    	request.getRequestDispatcher("/index.jsp").forward(request, response);
    	return;
    }
    else{
    %>
<body>
<div id="wrapper">
<div class="logo"></div>
     <div class="header">
     	<div class="title">OpenPortalServer 网络接入认证系统</div>
	      <div class="msg" id="msg"><%=message%></div>
		  <div class="column">
		  <form id="loginForm" action="<%=path%>/LoginOut" method="post"  onsubmit="_submit()">
		       <div class="login">
		       <div class="login_left">
		            <div>您已登录成功，可以连接网络，请不要关闭该窗口！！</div>
					<div><label>欢迎您：</label><font color="red"><b><%=username%></b></font></div>
	                <div><label>IP地址：</label><font color="red"><b><%=ip%></b></font></div>
		           
		       </div>
		       
		       
			   <div class="login_right">
			     <input type="submit" name="Submit" value="" class="submitout" />
			   </div>
			   <div class="clear"></div>
		  </div>
		  </form>
		<form id="Form" action="<%=radiusPath%>" method="post" target="_blank">
		  <p class="ckp" style="margin-top:18px;">
		 <input name="username" type="hidden" value="<%=username%>" />
         <input name="password" type="hidden" value="<%=password%>" />
		 <input type="submit" name="Submit2" value="" class="submit02" />
		  </p>
		  </form>
		  <p class="bq"><a href="<%=basePath%>home_index.action">【后台管理】</a>——技术支持：OpenPortalServer 网络接入认证系统  QQ:25901875</p>
		  </div>
		  
	 </div>
	 <div class="footer"><a href="#"><img src="<%=basePath%>images/banner.jpg" border="0" /></a></div>
</div>
</body>
<%} %>
</html>
