<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%
String portalPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/";

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<script src="indexjs/jquery.js" type="text/javascript"></script>
<title>OpenPortalServer 网络接入认证系统</title>
<link href="indexcss/wm_7.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
function _change() {
	/*
	1. 得到img元素
	2. 修改其src为/day11_3/VerifyCodeServlet
	*/
	var imgEle = document.getElementById("img");
	imgEle.src = "<%=path%>/VCodeServlet?a=" + new Date().getTime();
}
function _submit() {
	document.getElementById("msg").innerHTML = "正在请求认证，请稍等....";
	return true;
}
</script>


<script type="text/javascript">
var time_out=2;//自动提交的等待时间，单位：秒
var call_me="请联系管理员。";//忘记密码、没有账号的对话框文本
var weburl="";//登录成功后打开的第1个通告页面，网站必须带http://
var save_time=72;//保存账号信息的时间，单位：小时

function cWH(){
var isMobile = !!navigator.userAgent.match(/AppleWebKit.*Mobile.*/) && !!navigator.userAgent.match(/AppleWebKit/);

var pageWidth = document.body.clientWidth > document.documentElement.clientWidth ? document.body.clientWidth : document.documentElement.clientWidth;
var mobileMaxWidth = 768, pcMaxWidth = 480, pcMinWidth = 320;	
var mainWidth = isMobile ? (pageWidth>mobileMaxWidth ? mobileMaxWidth : pageWidth) : (pageWidth>pcMaxWidth ? pcMaxWidth : (pageWidth<pcMinWidth ? pcMinWidth : pageWidth));	
$("#main").width(mainWidth-2-2);

var pageHeight = document.body.clientHeight > document.documentElement.clientHeight ? document.body.clientHeight : document.documentElement.clientHeight;
var mainTop = pageHeight-$("#main").height();
	mainTop = mainTop<0 ? 0 : mainTop;
	mainTop = parseInt(mainTop/2,10);
	mainTop = (isMobile && mainTop>10) ? 10 : mainTop;
$("#main").css("margin-top",mainTop+"px");	
}


var ld="";
var init=0;
$(function(){
init = 1;
cWH();
setInterval(cWH,200);


$("#usr").focus();
});




function call(){
alert(call_me);
//window.location.href = "forum_show.action?id=${id}&pageNum=" + pageNum;
return true;
}
</script>
    
</head>
<% 
  String VerifyCode=(String)session.getAttribute("VerifyCode");
  String username=(String)session.getAttribute("username");
  String ip=(String)session.getAttribute("ip");
  if(username!=null&&ip!=null){
  	request.getRequestDispatcher("/WEB-INF/jsp/indexAction/loginSucc.jsp").forward(request, response);
  	return;
  }else{
  %>
	<%
    String uname="";
    Cookie[] cs=request.getCookies();
    if(cs!=null){
    	for(Cookie c:cs){
    		if("uname".equals(c.getName())){
    			uname=c.getValue();
    		}
    		
    	}
    }
    
    
    String message="";
    String msgApp=(String)application.getAttribute("msg");
    String msg=(String)request.getAttribute("msg");
    if(msg!=null){
    	message=msg;
    }
    if(msgApp!=null){
    	message=msgApp; 
    }
    %>
<body>
<div id="wrapper">
<div class="logo"></div>
     <div class="header">
     <form id="loginForm" action="<%=path%>/Login" method="post" onsubmit="_submit()">
	      <div class="title">OpenPortalServer 网络接入认证系统</div>
	      <div class="msg" id="msg"><%=message%></div>
		  <div class="column">
		       <div class="login">
		       <div class="login_left">
					<div><label>用户名：</label><input id="usr" type="text" name="username" class="text_1"  value="<%=uname%>" /></div>
	                <div><label>密码：</label><input id="pwd" type="password" name="password" class="text_1"/></div>
		           <%if("1".equals(VerifyCode)){ %> 
		           <div><label>验证码：</label><input id="vcode"  name="vcode" value="" class="text_1"/></div> 
		           <%}%>       
		       </div>
		       
		       
			   <div class="login_right">
			     <input type="submit" name="Submit" value="" class="submit01" />
			     <%if("1".equals(VerifyCode)){ %> 
			     <br/>
			     <p style="margin-top:2px;"/>
			     <a href="javascript:_change()"><img id="img" alt="请输入验证码"
			src="<%=path%>/VCodeServlet" /></a>
			<%}%>
			   </div>
			   <div class="clear"></div>
		  </div>
		 <%if("1".equals(VerifyCode)){ %>
		 <p class="ckp" style="margin-top:18px;">
		  <%}else{%>
		  <p class="ckp" style="margin-top:36px;">
		  <%}%>
		    <a href="<%=path%>/message_sendUI.action" onClick="call()" target="_blank"><img src="<%=path%>/images/sendMSG.png" width="18px"  height="18px"  alt="给管理员发送消息" />&nbsp;&nbsp;联系管理员</a>
		    &nbsp;&nbsp;<a href="<%=path%>/customer_login.action" target="_blank">用户自助中心</a>
		  </p>
		  <p class="bq"><a href="<%=basePath%>home_index.action">【后台管理】</a>——技术支持：OpenPortalServer 网络接入认证系统  QQ:25901875</p>
		  </div>
		  </form>
	 </div>
	 <div class="footer"><a href="#"><img src="<%=basePath%>images/banner.jpg" border="0" /></a></div>
</div>
</body>
<%} %>
</html>
