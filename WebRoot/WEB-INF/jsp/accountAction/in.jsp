<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>接入账户文件导入</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>




<link rel="stylesheet" href="leeson/css/bootstrap.min.css">
  <link rel="stylesheet" href="leeson/css/custom.css">
  <link rel="stylesheet" href="leeson/css/iosOverlay.css">
  <link rel="stylesheet" href="leeson/css/prettify.css">
  <script src="leeson/js/modernizr-2.0.6.min.js"></script>
  
  
</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/home_right.action">首页</a></li>
    <li><a href="${pageContext.request.contextPath}/account_list.action">接入账户管理</a></li>
    <li><a href="${pageContext.request.contextPath}/account_in.action">接入账户文件导入</a></li>
    </ul>
    </div>
    
    <div class="mainindex">
    
    
    <div class="welinfo">
    <span><img src="images/t03.png" alt="错误提示" /></span>
    <b>${msg}</b>
    </div>
    
    <div class="welinfo">
    <span><img src="images/time.png" alt="时间" /></span>
    <i>接入账户导入模板Excel文件下载</i> （下载地址：<a href="${pageContext.request.contextPath}/ExcelIn/demo.xls" target="_blank">请点这里</a>）
    </div>
    
    <div class="xline"></div>
    
    <ul class="iconlist">
    
    <li><a href="${pageContext.request.contextPath}/ExcelIn/demo.xls" target="_blank"><img src="images/ico02.png" /><p>模板下载</p></a></li>
    
    
            
    </ul>
     
    
    
    <div class="xline"></div>
    <div class="box"></div>
    
    <div class="welinfo">
    <span><img src="images/dp.png" alt="选择Execl文件" /></span>
    <b>请选择Execl文件</b>
    </div>
    <form action="account_doIn.action" method="post" enctype="multipart/form-data" >
    <ul class="infolist">
    <li><span>请选择要导入的文件</span><a class="ibtn"><input name="file" accept="xls|xlsx" type="file"/></a></li>
    
    </ul>
    <div class="ibox">
    <button id="loadToSuccess" class="btn">提交</button></div>
    <div class="xline"></div>
    
    
    
    </form>
    </div>
 <script src="leeson/js/jquery.min.js"></script>
  <script src="leeson/js/iosOverlay.js"></script>
  <script src="leeson/js/spin.min.js"></script>
  <script src="leeson/js/prettify.js"></script>
  
  
     
 <script type="text/javascript">
	$(document).on("click", "#loadToSuccess", function(e) {
		var opts = {
			lines: 13, // The number of lines to draw
			length: 11, // The length of each line
			width: 5, // The line thickness
			radius: 17, // The radius of the inner circle
			corners: 1, // Corner roundness (0..1)
			rotate: 0, // The rotation offset
			color: '#FFF', // #rgb or #rrggbb
			speed: 1, // Rounds per second
			trail: 60, // Afterglow percentage
			shadow: false, // Whether to render a shadow
			hwaccel: false, // Whether to use hardware acceleration
			className: 'spinner', // The CSS class to assign to the spinner
			zIndex: 2e9, // The z-index (defaults to 2000000000)
			top: 'auto', // Top position relative to parent in px
			left: 'auto' // Left position relative to parent in px
		};
		var target = document.createElement("div");
		document.body.appendChild(target);
		var spinner = new Spinner(opts).spin(target);
		var overlay = iosOverlay({
			text: "正在处理文件！",
			spinner: spinner
		});

		window.setTimeout(function() {
			overlay.update({
				icon: "leeson/img/check.png",
				text: "加载完成！"
			});
			
		}, 3e3);

		window.setTimeout(function() {
			overlay.hide();
			document.forms[0].submit();
		}, 5e3);
		
		return false;
	});
</script>   



<s:if test="#err==0">
<script type="text/javascript">
cross();
function cross(){
    	iosOverlay({
    		text: "${msg}",
    		duration: 2e3,
    		icon: "leeson/img/cross.png"
    	});
    }

</script>   
</s:if>


</body>
</html>
