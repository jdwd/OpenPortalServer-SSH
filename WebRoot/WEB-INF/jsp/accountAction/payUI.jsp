<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>充值</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>

  
<script type="text/javascript">
$(document).ready(function(e) {
    $(".select1").uedSelect({
		width : 345			  
	});
	$(".select2").uedSelect({
		width : 167  
	});
	$(".select3").uedSelect({
		width : 100
	});
	
	
	
	
	$(function(){                            
        $("#payTime").keydown(function(e){

// 注意此处不要用keypress方法，否则不能禁用　Ctrl+V 与　Ctrl+V,具体原因请自行查找keyPress与keyDown区分，十分重要，请细查

                if ($.browser.msie) {  // 判断浏览器

                       if ( ((event.keyCode > 47) && (event.keyCode < 58)) || (event.keyCode == 8) ) {
                    	   // 判断键值  

                              return true;  
                        } else {  
                              return false;  
                       }
                 } else {  
                    if ( ((e.which > 47) && (e.which < 58)) || (e.which == 8) || (event.keyCode == 17) || ((event.keyCode >= 96) && (event.keyCode <= 105)) ) {  
                             return true;  
                     } else {  
                             return false;  
                     }  
                 }}).focus(function() {
                         this.style.imeMode='disabled';   // 禁用输入法,禁止输入中文字符

        });
});
	
	
});
</script>

</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/home_right.action">首页</a></li>
    <li><a href="${pageContext.request.contextPath}/account_list.action">接入账户管理</a></li>
    <li><a href="${pageContext.request.contextPath}/account_payUI.action?id=${id}">账户充值</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
    <li><a href="#tab1" class="selected">接入账户充值</a></li>
  	</ul>
    </div> 
    
  	<div id="tab1" class="tabson">
    
    <div class="formtext">
    
    <b>为户 【${loginName}】 充值！</b>
    
    </div>
    
    <s:form action="account_pay">
       <s:hidden name="id"></s:hidden>
        
    <ul class="forminfo">
    <li><label>登录名<b></b></label>
    <input type="text" name="" value="${loginName}" disabled="true" Class="dfinput" style="width:400px;"/>
    </li>
    
    <li><label>姓  名<b></b></label>
    <input type="text" name="" value="${name}" disabled="true" Class="dfinput" style="width:400px;"/>
    </li>
    
    
    <li><label>充值类型<b>*</b></label>
    <div class="vocation">
    <s:select name="payType" cssClass="select1" 
    list="#{2:'计时',3:'买断'}"  listKey="key" listValue="value"/>
    </div>
    </li>
    
    <li><label>充值时长<b>(小时)</b></label>
    <input type="text" name="payTime" id="payTime" Class="dfinput" style="width:400px;"/>
    </li>
    
    <li><label>&nbsp;</label>
    <s:submit cssClass="btn" value="保存" name="保存"></s:submit></li>
    </ul>
    </s:form>
    </div> 
    
    
    </div> 
 
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    
    
    
    </div>


</body>

</html>