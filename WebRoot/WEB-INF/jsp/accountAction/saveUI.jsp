<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>用户信息</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>


  
<script type="text/javascript">
$(document).ready(function(e) {
    $(".select1").uedSelect({
		width : 345			  
	});
	$(".select2").uedSelect({
		width : 167  
	});
	$(".select3").uedSelect({
		width : 100
	});
});
</script>
</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/home_right.action">首页</a></li>
    <li><a href="${pageContext.request.contextPath}/account_list.action">接入账户管理</a></li>
    <li><a href="${pageContext.request.contextPath}/account_addUI.action">账户添加</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
    <li><a href="#tab1" class="selected">接入用户添加</a></li>
  	</ul>
    </div> 
    
  	<div id="tab1" class="tabson">
    
    <div class="formtext">
    
    <b>本页面为接入用户添加页面！</b>
    <b>${msg }</b>
    
    </div>
    
    <s:form action="account_%{id == null ? 'add' : 'edit'}">
       <s:hidden name="id"></s:hidden>
        
    <ul class="forminfo">
    <li><label>登录名<b>*</b></label>
    <s:textfield  name="loginName" cssClass="dfinput" style="width:400px;"/></li>
   
    <li><label>密码<b>*</b></label>
    <s:password   name="password" cssClass="dfinput" style="width:400px;"/></li>
    
    <li><label>姓名<b></b></label>
    <s:textfield  name="name" cssClass="dfinput" style="width:400px;"/></li>
    
    <li><label>性别<b></b></label>
    <div class="vocation">
    <s:select name="gender" cssClass="select1" 
    list="#{1:'男',0:'女'}"  listKey="key" listValue="value"  headerKey="" headerValue="请选择性别"/>
    </div>
    </li>
    
    <li><label>电话号码<b></b></label>
    <s:textfield  name="phoneNumber" cssClass="dfinput" style="width:400px;"/></li>
    
    <li><label>电子邮件<b>*</b></label>
    <s:textfield  name="email" cssClass="dfinput" style="width:400px;"/></li>
    
    <li><label>详细信息<b></b></label>
    <s:textarea name="description" cssClass="textinput"></s:textarea></li>
    
    <li><label>类型<b>*</b></label>
    <div class="vocation">
    <s:select name="state" cssClass="select1" 
    list="#{0:'停用',1:'免费',2:'计时',3:'买断'}"  listKey="key" listValue="value"  headerKey="1" headerValue="请选择状态"/>
    </div>
    </li>
    
    
    
    <li><label>&nbsp;</label>
    <s:submit cssClass="btn" value="保存" name="保存"></s:submit></li>
    </ul>
    </s:form>
    </div> 
    
    
    </div> 
 
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    
    
    
    </div>


</body>

</html>