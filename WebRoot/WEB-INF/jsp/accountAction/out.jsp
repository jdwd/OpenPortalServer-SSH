<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>接入账户文件导出</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>

</head>
<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/home_right.action">首页</a></li>
    <li><a href="${pageContext.request.contextPath}/account_list.action">接入账户管理</a></li>
    <li><a href="${pageContext.request.contextPath}/account_out.action">接入账户导出</a></li>
    </ul>
    </div>
   
   
   
   <div class="formbody">
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
    <li><a href="#tab1" class="selected">接入账户文件导出</a></li>
  	</ul>
    </div> 
    
  	<div id="tab1" class="tabson">
    
    <div class="formtext">
    <s:if test="#downUrl==0">
    <b>${msg}</b>
    </s:if>
    <s:else>
    <b>接入账户文件生成完毕——>&nbsp;${downUrl}&nbsp;请在下方列表中点击下载</b>
    </s:else>
    </div>
    
    
    <table class="tablelist">
    <thead>
    	<tr>
        <th width="35%">名称</th>
        <th width="35%">修改日期</th>
        <th width="20%">类型</th>
        <th width="10%">大小</th>
        </tr>    	
    </thead>
    <tbody>
    
    	<tr>
        <td>
        <a href="${pageContext.request.contextPath}/ExcelOut/${downUrl}" target="_blank">
        <img src="images/f05.png" />${downUrl}
        </a>
        </td>
        <td>
        <a href="${pageContext.request.contextPath}/ExcelOut/${downUrl}" target="_blank">
        ${creatDate}
        </a>
        </td>
        <td>
        <a href="${pageContext.request.contextPath}/ExcelOut/${downUrl}" target="_blank">
                        电子表格
        </a>
        </td>
        <td class="tdlast">
        <a href="${pageContext.request.contextPath}/ExcelOut/${downUrl}" target="_blank">
        20 KB
        </a>
        </td>
        </tr>
        
        
        <tr>
        <td>
        <a href="${pageContext.request.contextPath}/ExcelOut/${downUrl}" target="_blank">
        <img src="images/f05.png" />${downUrl}
        </a>
        </td>
        <td>
        <a href="${pageContext.request.contextPath}/ExcelOut/${downUrl}" target="_blank">
        ${creatDate}
        </a>
        </td>
        <td>
        <a href="${pageContext.request.contextPath}/ExcelOut/${downUrl}" target="_blank">
                        电子表格
        </a>
        </td>
        <td class="tdlast">
        <a href="${pageContext.request.contextPath}/ExcelOut/${downUrl}" target="_blank">
        20 KB
        </a>
        </td>
        </tr>
        
        
     </tbody>
    </table>
    
    
    
    
    </div>
    </div>
    </div>
    
     
    
 
    
    
    
   

</body>

</html>
