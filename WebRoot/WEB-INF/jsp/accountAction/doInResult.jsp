<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>导入失败用户列表</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript">
    KE.show({
        id : 'content7',
        cssPath : './index.css'
    });
  </script>
  
<script type="text/javascript">
$(document).ready(function(e) {
    $(".select1").uedSelect({
		width : 345			  
	});
	$(".select2").uedSelect({
		width : 167  
	});
	$(".select3").uedSelect({
		width : 100
	});
});
</script>
<script type="text/javascript">
		$(function(){
			// 指定事件处理函数
			$("[name=choose]").click(function(){
				
				// 当选中或取消一个权限时，也同时选中或取消所有的下级权限
				$(this).siblings("tr").find("input").attr("checked", this.checked);
				
				// 当选中一个权限时，也要选中所有的直接上级权限
				if(this.checked == true){
					$(this).parents("table").children("th").children("input").attr("checked", true);
				}
				
			});
		});
	</script>
<script type="text/javascript">
		function _changeState(a) {
			location="${pageContext.request.contextPath}/account_editState.action?id=" + a;
		}
	</script>

</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/home_right.action">首页</a></li>
    <li><a href="${pageContext.request.contextPath}/account_list.action">接入账户管理</a></li>
    <li><a href="${pageContext.request.contextPath}/account_list.action">账户列表</a></li>
    </ul>
    </div>
    
    <div class="formbody">
   
    <div id="tab1" class="tabson">
        
    
    
    
    <div class="tools">
    
    	<ul class="toolbar">
        
        
        <li><span><img src="images/t03.png" /></span>导入失败用户列表：以下用户登录名已存在，导入失败！！！！</li>
        
        
       
        </ul>
        
        
        <ul class="toolbar1">
        <li><span><img src="images/t03.png" /></span>导入失败用户列表：以下用户登录名已存在，导入失败！！！！</li>
        </ul>
    
    </div>
    <table class="tablelist">
    	<thead>
    	<tr>
        <th>编号<i class="sort"><img src="images/px.gif" /></i></th>
        <th>登录名</th>
        <th>姓名</th>
        <th>性别</th>
        <th>电话号码</th>
        <th>电子邮件</th>
        <th>到期时间</th>
        <th>剩余时长</th>
        <th>用户类型</th>
        
        </tr>
        </thead>
        <tbody>
        <s:iterator value="unInAccounts" status="index">
        <tr>
        
        <td><s:property value="#index.index"/></td>
        
        <td>${loginName}</td>
        <td>${name}</td>
        <td><s:if test="gender==1">男</s:if>
        				<s:elseif test="gender==0">女</s:elseif>
        				<s:else>未知</s:else></td>
        <td>${phoneNumber}</td>
        <td>${email}</td>
        <td>${date}</td>
        <td>
        <s:if test="time==0">${time}分钟</s:if>
        <s:else><s:property value="time/1000/60"/>分钟</s:else>
        </td>
        <td <s:if test="#session.user.hasPrivilegeByUrl('/account_edit')">title="点击改变用户状态" onclick="_changeState(${id})"</s:if>>
        <s:if test="state==1">免费</s:if>
        <s:elseif test="state==2">计时</s:elseif>
        <s:elseif test="state==3">买断</s:elseif>
        <s:else>停用</s:else>
		</td>
        
        </tr> 
        </s:iterator>
                
        </tbody>
    </table>
    
    </div>
   
    
    
   
    
   
    
    
    <script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    </div>
    

</body>

</html>
