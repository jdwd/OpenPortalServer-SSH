<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>接入用户列表</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript">
    KE.show({
        id : 'content7',
        cssPath : './index.css'
    });
  </script>
  
<script type="text/javascript">
$(document).ready(function(e) {
    $(".select1").uedSelect({
		width : 345			  
	});
	$(".select2").uedSelect({
		width : 167  
	});
	$(".select3").uedSelect({
		width : 100
	});
});
</script>
<script type="text/javascript">
		$(function(){
			// 指定事件处理函数
			$("[name=choose]").click(function(){
				
				// 当选中或取消一个权限时，也同时选中或取消所有的下级权限
				$(this).siblings("tr").find("input").attr("checked", this.checked);
				
				// 当选中一个权限时，也要选中所有的直接上级权限
				if(this.checked == true){
					$(this).parents("table").children("th").children("input").attr("checked", true);
				}
				
			});
		});
	</script>
<script type="text/javascript">
		function _changeState(a) {
			location="${pageContext.request.contextPath}/account_editState.action?id=" + a;
		}
	</script>

</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/home_right.action">首页</a></li>
    <li><a href="${pageContext.request.contextPath}/account_list.action">接入账户管理</a></li>
    <li><a href="${pageContext.request.contextPath}/account_list.action">账户列表</a></li>
    </ul>
    </div>
    
    <div class="formbody">
   
    <div id="tab1" class="tabson">
        
    <s:form action="account_list">   
    <ul class="seachform">
    
    <li><label>编号</label>
    <s:textfield  name="id" cssClass="scinput"/></li>
    <li><label>登录名</label>
    <s:textfield  name="loginName" cssClass="scinput"/></li>
    
    <li><label>姓名</label>  
    <s:textfield  name="name" cssClass="scinput"/></li>
    
    <li><label>状态</label>  
    <div class="vocation">
    <s:select name="state" cssClass="select3" 
    list="#{0:'停用',1:'免费',2:'计时',3:'买断'}"  listKey="key" listValue="value"  headerKey="" headerValue="请选择状态"/>
    </div>
    </li>
    
    <li><label>&nbsp;</label>
    <s:submit cssClass="scbtn" value="查询" name=""/>
    </ul>
    </s:form>
    
    <s:form action="account_deleteChoose">
    <div class="tools">
    
    	<ul class="toolbar">
        <s:a action="account_addUI"><li><span><img src="images/t01.png" /></span>添加</li></s:a>
        <s:if test="#session.user.hasPrivilegeByUrl('/account_delete')">
        <s:a href="javascript: gotoDelete()" onclick="return window.confirm('您确定要删除已选择的用户吗？')"><li><span><img src="images/t03.png" /></span>删除</li></s:a>
        </s:if>
        
        <li><a href="${pageContext.request.contextPath}/account_list.action"><span><img src="images/t04.png" /></span>统计</a></li>
        </ul>
        
        
        <ul class="toolbar1">
        <li><a href="${pageContext.request.contextPath}/account_list.action"><span><img src="images/t05.png" /></span>刷新</a></li>
        </ul>
    
    </div>
    <table class="tablelist">
    	<thead>
    	<tr>
        <th><input name="chooseAll" type="checkbox" onClick="$('[name=choose]').attr('checked', this.checked)"/></th>
        <th>编号<i class="sort"><img src="images/px.gif" /></i></th>
        
        <th>登录名</th>
        <th>姓名</th>
        <th>性别</th>
        <th>电话号码</th>
        <th>电子邮件</th>
        <th>到期时间</th>
        <th>剩余时长</th>
        <th>用户类型</th>
        <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="recordList">
        <tr>
        <td><input name="choose" type="checkbox" value="${id}" <s:property value="%{id in choose ? 'checked' : ''}"/>/></td>
        <td>${id}</td>
        
        <td title="点击查看连接记录"><a href="${pageContext.request.contextPath}/linkRecord_list.action?loginName=${loginName}">${loginName}</a></td>
        <td>${name}</td>
        <td><s:if test="gender==1">男</s:if>
        				<s:elseif test="gender==0">女</s:elseif>
        				<s:else>未知</s:else></td>
        <td>${phoneNumber}</td>
        <td>${email}</td>
        <td><fmt:formatDate value="${date}" pattern="yyyy-MM-dd hh:mm:ss"/></td>
        <td>
        <s:if test="time==0">${time}分钟</s:if>
        <s:else><s:property value="time/1000/60"/>分钟</s:else>
        </td>
        <td <s:if test="#session.user.hasPrivilegeByUrl('/account_edit')">title="点击改变用户状态" onclick="_changeState(${id})"</s:if>>
        <s:if test="state==1">免费</s:if>
        <s:elseif test="state==2">计时</s:elseif>
        <s:elseif test="state==3">买断</s:elseif>
        <s:else>停用</s:else>
		</td>
        <td><s:a cssClass="tablelink" action="account_payUI?id=%{id}">充值</s:a>     <s:a cssClass="tablelink" action="account_editUI?id=%{id}">修改</s:a>     <s:a cssClass="tablelink" action="account_delete?id=%{id}" onclick="return window.confirm('您确定要删除该用户吗？')">删除</s:a>     <s:if test="#session.user.hasPrivilegeByUrl('/account_edit')"><s:a cssClass="tablelink" action="account_initPassword?id=%{id}" onclick="return window.confirm('您确定要初始化密码为1234吗？')"> 初始化密码</s:a></s:if></td>
        </tr> 
        </s:iterator>
                
        </tbody>
    </table>
    </s:form>
    </div>
   
    <div class="pagin">
    	<div class="message">共&nbsp;<i class="blue">${recordCount }</i>&nbsp;条记录，当前显示第&nbsp;<i class="blue">${currentPage }</i>&nbsp;页,共&nbsp;<i class="blue">${pageCount }</i>&nbsp;页,每页显示&nbsp;<i class="blue">${pageSize }</i>&nbsp;条记录</div>
        <ul class="paginList">
        <li class="paginItem"><a href="javascript: gotoPage(1)" title="首页">首</a></li>
        <li class="paginItem"><a href="javascript: gotoPage(
        <s:if test="1 == currentPage">
				${currentPage}
			</s:if>
			<s:else>
				${currentPage}-1
			</s:else>
        )"><span class="pagepre"></span></a></li>
        
        <s:iterator begin="%{beginPageIndex}" end="%{endPageIndex}" var="num">
			<s:if test="#num == currentPage"> <%-- 当前页 --%>
				 <li class="paginItem current"><a href="#">${num}</a></li>
			</s:if>
			<s:else> <%-- 非当前页 --%>
				<li class="paginItem"><a href="javascript: gotoPage(${num})">${num}</a></li>
			</s:else>
		</s:iterator>
		
		<li class="paginItem"><a href="javascript: gotoPage(
        <s:if test="pageCount == currentPage">
				${currentPage}
			</s:if>
			<s:else>
				${currentPage}+1
			</s:else>
        )"><span class="pagenxt"></span></a></li>
        <li class="paginItem"><a href="javascript: gotoPage(${pageCount})" title="最后一页">尾</a></li>
        </ul>
    </div>
    
    <script type="text/javascript">
	function gotoPage( pageNum ){
		// window.location.href = "forum_show.action?id=${id}&pageNum=" + pageNum;
		
		$(document.forms[0]).append("<input type='hidden' name='pageNum' value='" + pageNum +"'>");
		document.forms[0].submit();
	}
	
	function gotoDelete(){
		document.forms[1].submit();
	}
</script>
    
   
    
    
    <script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    </div>
    

</body>

</html>
