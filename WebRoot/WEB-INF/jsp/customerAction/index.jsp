<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html lang="zh-cn">
<head>
<base href="<%=basePath%>"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="customer/favicon.ico">
<title>OpenPortalServer用户自助服务中心</title>
<link href="customer/css/bootstrap.min.css" rel="stylesheet">
<link href="customer/css/customer.css" rel="stylesheet">
<script src="customer/js/jquery-1.11.0.min.js"></script>
<script src="customer/js/bootstrap.min.js"></script>  
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]>
<script src="/static/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="/static/js/html5shiv.min.js"></script>
<script src="/static/js/respond.min.js"></script>
<![endif]-->
<script>
function reactive(){
    $.post("/email/reactive",{},function(ev){
        alert(ev.msg);
    },"json");
}   
</script>

</head>
<body>
<div class="navbar  navbar-inverse navbar-static-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">OpenPortalServer用户自助服务中心</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="${pageContext.request.contextPath}/customer_login.action">OpenPortalServer用户自助服务中心</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="${pageContext.request.contextPath}/customer_login.action">首页</a></li>
        <li><a href="${pageContext.request.contextPath}/customer_logout.action">退出</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-3">
    

    
<div class="sidebar-box">
<h4 class="list-group-item"><span class="glyphicon glyphicon-cog"></span> 我的服务</h4>
<hr>
<div class="list-group">
  <a href="${pageContext.request.contextPath}/customer_login.action" class="list-group-item">用户账号信息</a>  
  <a href="${pageContext.request.contextPath}/customer_payUI.action?id=${id }" class="list-group-item">用户自助充值</a>    
  <a href="${pageContext.request.contextPath}/customer_payRecord.action?uid=${id }" class="list-group-item">充值记录查询</a>
  <a href="${pageContext.request.contextPath}/customer_linkRecord.action?uid=${id }" class="list-group-item">上网记录查询</a>
  <a href="http://www.tmall.com/" target="_blank" class="list-group-item">充值卡购买</a>   
</div>
</div>

    
<div class="sidebar-box">
<h4 class="list-group-item"><span class="glyphicon glyphicon-comment"></span> 客服信息</h4>
<hr>
<div class="list-group">
  <a href="javascript:void();" class="list-group-item">客服QQ:25901875</a>   
  <a href="javascript:void();" class="list-group-item">客服电话:15692593456</a>   
</div>
</div>


    
<div class="sidebar-box">
  <h4 class="list-group-item"><span class="glyphicon glyphicon-qrcode"></span> OpenPortalServer</h4>
  <hr>
  <div class="img-center"><img src="customer/img/logo.jpg"></div>
</div>

</div>
        <div class="col-md-9">
<div class="panel panel-default">
    <div class="panel-heading"> <span class="glyphicon glyphicon-user"></span> 用户信息
    </div>
    <div class="panel-body posts">
    <p id="alert" class="alert alert-warning">${msg }</p>
        <div class="pull-right">
            <a href="${pageContext.request.contextPath}/customer_editUI.action?id=${id }" class="btn btn-sm btn-info">修改基本资料</a>
        </div>
        <table class="table ">
                <thead>
                    <tr>
                        <th colspan="6"> 基本信息</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>用户姓名</td>
                        <td>${name }</td>
                        <td>用户登陆名</td>
                        <td>${loginName }</td>  
                        <td>用户状态</td>
                        <td>
						<s:if test="state==1">免费</s:if>
        				<s:elseif test="state==2">计时</s:elseif>
        				<s:elseif test="state==3">买断</s:elseif>
       					<s:else>停用</s:else>
						</td>               
                    </tr>
                    <tr>
                   		<td>性别</td>
                        <td>
                        <s:if test="gender==1">男</s:if>
        				<s:elseif test="gender==0">女</s:elseif>
        				<s:else>未知</s:else>
        				</td>
                        <td>联系电话</td>
                        <td>${phoneNumber }</td>
                        <td>E-Mail</td>
                        <td>${email }</td>
                    </tr>      
                    <tr>
                        <td>详细信息</td>
                        <td colspan="5">${description }</td>                  
                    </tr>                                                                                         
            </tbody>
        </table>
        
        <table class="table table-striped ">
            <thead>
                <tr>
                    <th colspan="9"> 账号列表</th>
                </tr>
            </thead>
            <tbody>
            <tr class="active">
                <th>账号</th>
                <th>类型</th>
                <th>状态</th>
                <th>买断到期</th>
                <th>计时剩余</th>
                <th>在线?</th>
                <th></th>
            </tr>            
            <tr>
                <td><a href="detail?account_number=111">${loginName }</a></td>
                <td>
                		<s:if test="state==1">免费</s:if>
        				<s:elseif test="state==2">计时</s:elseif>
        				<s:elseif test="state==3">买断</s:elseif>
       					<s:else>停用</s:else>
       			</td>
                <td>
                		<s:if test="state==0">锁定</s:if>
        				<s:else>正常</s:else>
       			</td>
                <td><fmt:formatDate value="${date }" pattern="yyyy-MM-dd hh:mm:ss"/></td>
                <td><s:property value="time/1000/60"/>分钟</td>
                <td>${isOnline }</td>
                <td>
                         <a class="opt-btn btn-default" href="${pageContext.request.contextPath}/customer_payUI.action?id=${id }">充值</a>
                     <a class="opt-btn btn-default" href="${pageContext.request.contextPath}/customer_editUI.action?id=${id }">修改密码</a>
                 </td>
            </tr>
            </tbody>
        </table>  
       
    </div>
</div>
</div>
    </div>
</div>

<div id="footer">
  <div class="container">
    <ul>
    </ul>
    <div class="copy">
        <p class="pull-left">© 2015 OpenPortalServer用户自助服务中心</p> 
    </div>
  </div>
</div>

   

</body></html>