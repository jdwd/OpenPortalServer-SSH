<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html lang="zh-cn">
<head>
<base href="<%=basePath%>"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="customer/favicon.ico">
<title>OpenPortalServer用户自助服务中心</title>
<link href="customer/css/bootstrap.min.css" rel="stylesheet">
<link href="customer/css/customer.css" rel="stylesheet">
<script src="customer/js/jquery-1.11.0.min.js"></script>
<script src="customer/js/bootstrap.min.js"></script>  
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]>
<script src="/static/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="/static/js/html5shiv.min.js"></script>
<script src="/static/js/respond.min.js"></script>
<![endif]-->
<script src="customer/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="customer/js/bootstrap-datetimepicker.min.css">
   <script>
    $(document).ready(function(){
        $("#query_begin_time").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss',autoclose: true,todayBtn: true});
        $("#query_end_time").datetimepicker({format: 'yyyy-mm-dd hh:ii:ss',autoclose: true,todayBtn: true});
    });

   </script>

<script>
function reactive(){
    $.post("/email/reactive",{},function(ev){
        alert(ev.msg);
    },"json");
}   
</script>

</head>
<body>
<div class="navbar  navbar-inverse navbar-static-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">OpenPortalServer用户自助服务中心</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="${pageContext.request.contextPath}/customer_login.action">OpenPortalServer用户自助服务中心</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="${pageContext.request.contextPath}/customer_login.action">首页</a></li>
        <li><a href="${pageContext.request.contextPath}/customer_logout.action">退出</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-3">
    

    
<div class="sidebar-box">
<h4 class="list-group-item"><span class="glyphicon glyphicon-cog"></span> 我的服务</h4>
<hr>
<div class="list-group">
  <a href="${pageContext.request.contextPath}/customer_login.action" class="list-group-item">用户账号信息</a>  
  <a href="${pageContext.request.contextPath}/customer_payUI.action?id=${id }" class="list-group-item">用户自助充值</a>    
  <a href="${pageContext.request.contextPath}/customer_payRecord.action?uid=${id }" class="list-group-item">充值记录查询</a>
  <a href="${pageContext.request.contextPath}/customer_linkRecord.action?uid=${id }" class="list-group-item">上网记录查询</a>
  <a href="http://www.tmall.com/" target="_blank" class="list-group-item">充值卡购买</a>   
</div>
</div>

    
<div class="sidebar-box">
<h4 class="list-group-item"><span class="glyphicon glyphicon-comment"></span> 客服信息</h4>
<hr>
<div class="list-group">
  <a href="javascript:void();" class="list-group-item">客服QQ:25901875</a>   
  <a href="javascript:void();" class="list-group-item">客服电话:15692593456</a>   
</div>
</div>


    
<div class="sidebar-box">
  <h4 class="list-group-item"><span class="glyphicon glyphicon-qrcode"></span> OpenPortalServer</h4>
  <hr>
  <div class="img-center"><img src="customer/img/logo.jpg"></div>
</div>

</div>
        <div class="col-md-9">
<div class="panel panel-default">
    <div class="panel-heading"><span class="glyphicon glyphicon-th"></span>充值记录查询</div>
    <div class="panel-body">
        <form id="query_form" class="form-horizontal" role="form" action="${pageContext.request.contextPath}/customer_payRecord.action" method="post">   
            <s:hidden name="uid"></s:hidden>
            
            <div class="form-group">
                <label for="query_begin_time" class="col-md-2 control-label">开始时间</label>

                <div class="col-md-3">
                    <input type="text" id="query_begin_time" name="query_begin_time" value="${query_begin_time}" class="form-control form_datetime">
                </div>
                 <label for="query_end_time" class="col-md-2 control-label">结束时间</label>
                <div class="col-md-3">
                    <input type="text" id="query_end_time" name="query_end_time" value="${query_end_time}" class="form-control form_datetime">
                </div>
            </div>
            
            <div class="form-group">
            	<label for="query_end_time" class="col-md-2 control-label">充值类型</label>
                <div class="col-md-3">
                <s:select name="payType" cssClass="form-control" 
    list="#{2:'计时',3:'买断'}"  listKey="key" listValue="value"  headerKey="" headerValue="请选择类型"/>
                </div>
                <label for="account_number" class="col-md-2 control-label">计时类型</label>
                <div class="col-md-3">
                <s:select name="categoryType" cssClass="form-control" 
    list="#{0:'包时卡',1:'日卡',2:'月卡',3:'年卡'}"  listKey="key" listValue="value"  headerKey="" headerValue="请选择分类"/>
                </div> 
                
            </div>  
            
             <div class="form-group">
                <div class="col-md-10">
                    <button type="submit" class="btn btn-primary">查询</button>
                </div>
             </div>   
                   

        </form>
        <hr>

        <table class="table table-hover">
            <thead>
            <tr>
                <th>名称</th>
        		<th>描述</th>
        		<th>充值类型</th>
       			<th>分类</th>
        		<th>CD-KEY</th>
        		<th>时长</th>
        		<th>充值日期</th>
            </tr>
            </thead>
            <tbody>
               <s:iterator value="recordList">
        <tr>
        
        <td>${name}</td>
        <td>${description}</td>
        
        <td>
        <s:if test="payType==2">计时</s:if>
        <s:elseif test="payType==3">买断</s:elseif>
        <s:else>错误</s:else>
		</td>
		
		<td>
        <s:if test="categoryType==0">包时卡</s:if>
        <s:elseif test="categoryType==1">日卡</s:elseif>
        <s:elseif test="categoryType==2">月卡</s:elseif>
        <s:elseif test="categoryType==3">年卡</s:elseif>
        <s:else>错误</s:else>
		</td>
		
		<td>${cdKey}</td>
		
        <td>
        <s:if test="categoryType==0"><s:property value="payTime/1000/60/60"/>小时</s:if>
        <s:elseif test="categoryType==1"><s:property value="payTime/1000/60/60/24"/>天</s:elseif>
        <s:elseif test="categoryType==2"><s:property value="payTime/1000/60/60/24/31"/>月</s:elseif>
        <s:elseif test="categoryType==3"><s:property value="payTime/1000/60/60/24/31/12"/>年</s:elseif>
        <s:else>错误</s:else>
        </td>
        <td><fmt:formatDate value="${payDate}" pattern="yyyy-MM-dd hh:mm:ss"/></td>
        
        </tr> 
        </s:iterator>    
            
            </tbody>
        </table>
 <hr>
        
        <div class="form-group">
            <div class="col-md-10">
                    <div class="pagin">
    	<div class="message">共&nbsp;<i class="blue">${recordCount }</i>&nbsp;条记录，当前显示第&nbsp;<i class="blue">${currentPage }</i>&nbsp;页,共&nbsp;<i class="blue">${pageCount }</i>&nbsp;页,每页显示&nbsp;<i class="blue">${pageSize }</i>&nbsp;条记录</div>
        <div class="paginList"  style="float:right;">
        <a href="javascript: gotoPage(1)" title="首页">首</a>
        <a href="javascript: gotoPage(
        <s:if test="1 == currentPage">
				${currentPage}
			</s:if>
			<s:else>
				${currentPage}-1
			</s:else>
        )"><span class="pagepre">上一页</span></a>
        
        <s:iterator begin="%{beginPageIndex}" end="%{endPageIndex}" var="num">
			<s:if test="#num == currentPage"> <%-- 当前页 --%>
				 ${num}
			</s:if>
			<s:else> <%-- 非当前页 --%>
				<a href="javascript: gotoPage(${num})">${num}</a>
			</s:else>
		</s:iterator>
		
		<a href="javascript: gotoPage(
        <s:if test="pageCount == currentPage">
				${currentPage}
			</s:if>
			<s:else>
				${currentPage}+1
			</s:else>
        )"><span class="pagenxt">下一页</span></a>
        <a href="javascript: gotoPage(${pageCount})" title="最后一页">尾</a>
        </div>
    </div>
    
   
   </div>
       </div>
 <script type="text/javascript">
	function gotoPage( pageNum ){
		// window.location.href = "forum_show.action?id=${id}&pageNum=" + pageNum;
		
		$(document.forms[0]).append("<input type='hidden' name='pageNum' value='" + pageNum +"'>");
		document.forms[0].submit();
	}
	
</script>         
    </div>
</div>
</div>
    </div>
</div>

<div id="footer">
  <div class="container">
    <ul>
    </ul>
    <div class="copy">
        <p class="pull-left">© 2015 OpenPortalServer用户自助服务中心</p> 
    </div>
  </div>
</div>

   

<div class="datetimepicker datetimepicker-dropdown-bottom-right dropdown-menu" style="left: 553.90625px; z-index: 1040;"><div class="datetimepicker-minutes" style="display: none;"><table class=" table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-left"></i> </th><th colspan="5" class="switch">4 八月 2015</th><th class="next" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-right"></i> </th></tr></thead><tbody><tr><td colspan="7"><span class="minute">6:00</span><span class="minute">6:05</span><span class="minute">6:10</span><span class="minute">6:15</span><span class="minute">6:20</span><span class="minute">6:25</span><span class="minute">6:30</span><span class="minute">6:35</span><span class="minute">6:40</span><span class="minute">6:45</span><span class="minute">6:50</span><span class="minute active">6:55</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today">Today</th></tr></tfoot></table></div><div class="datetimepicker-hours" style="display: none;"><table class=" table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-left"></i> </th><th colspan="5" class="switch">4 八月 2015</th><th class="next" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-right"></i> </th></tr></thead><tbody><tr><td colspan="7"><span class="hour">0:00</span><span class="hour">1:00</span><span class="hour">2:00</span><span class="hour">3:00</span><span class="hour">4:00</span><span class="hour">5:00</span><span class="hour active">6:00</span><span class="hour">7:00</span><span class="hour">8:00</span><span class="hour">9:00</span><span class="hour">10:00</span><span class="hour">11:00</span><span class="hour">12:00</span><span class="hour">13:00</span><span class="hour">14:00</span><span class="hour">15:00</span><span class="hour">16:00</span><span class="hour">17:00</span><span class="hour">18:00</span><span class="hour">19:00</span><span class="hour">20:00</span><span class="hour">21:00</span><span class="hour">22:00</span><span class="hour">23:00</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today">Today</th></tr></tfoot></table></div><div class="datetimepicker-days" style="display: block;"><table class=" table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-left"></i> </th><th colspan="5" class="switch">八月 2015</th><th class="next" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-right"></i> </th></tr><tr><th class="dow">日</th><th class="dow">一</th><th class="dow">二</th><th class="dow">三</th><th class="dow">四</th><th class="dow">五</th><th class="dow">六</th></tr></thead><tbody><tr><td class="day old">26</td><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td></tr><tr><td class="day">2</td><td class="day">3</td><td class="day active">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td></tr><tr><td class="day">9</td><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td></tr><tr><td class="day">16</td><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td></tr><tr><td class="day">23</td><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td></tr><tr><td class="day">30</td><td class="day">31</td><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td></tr></tbody><tfoot><tr><th colspan="7" class="today">Today</th></tr></tfoot></table></div><div class="datetimepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-left"></i> </th><th colspan="5" class="switch">2015</th><th class="next" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-right"></i> </th></tr></thead><tbody><tr><td colspan="7"><span class="month">一月</span><span class="month">二月</span><span class="month">三月</span><span class="month">四月</span><span class="month">五月</span><span class="month">六月</span><span class="month">七月</span><span class="month active">八月</span><span class="month">九月</span><span class="month">十月</span><span class="month">十一月</span><span class="month">十二月</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today">Today</th></tr></tfoot></table></div><div class="datetimepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-left"></i> </th><th colspan="5" class="switch">2010-2019</th><th class="next" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-right"></i> </th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year active">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year">2019</span><span class="year old">2020</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today">Today</th></tr></tfoot></table></div></div><div class="datetimepicker datetimepicker-dropdown-bottom-right dropdown-menu" style="left: 906.1875px; z-index: 1080;"><div class="datetimepicker-minutes" style="display: none;"><table class=" table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-left"></i> </th><th colspan="5" class="switch">4 八月 2015</th><th class="next" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-right"></i> </th></tr></thead><tbody><tr><td colspan="7"><span class="minute">6:00</span><span class="minute">6:05</span><span class="minute">6:10</span><span class="minute">6:15</span><span class="minute">6:20</span><span class="minute">6:25</span><span class="minute">6:30</span><span class="minute">6:35</span><span class="minute">6:40</span><span class="minute">6:45</span><span class="minute">6:50</span><span class="minute active">6:55</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today">Today</th></tr></tfoot></table></div><div class="datetimepicker-hours" style="display: none;"><table class=" table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-left"></i> </th><th colspan="5" class="switch">4 八月 2015</th><th class="next" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-right"></i> </th></tr></thead><tbody><tr><td colspan="7"><span class="hour">0:00</span><span class="hour">1:00</span><span class="hour">2:00</span><span class="hour">3:00</span><span class="hour">4:00</span><span class="hour">5:00</span><span class="hour active">6:00</span><span class="hour">7:00</span><span class="hour">8:00</span><span class="hour">9:00</span><span class="hour">10:00</span><span class="hour">11:00</span><span class="hour">12:00</span><span class="hour">13:00</span><span class="hour">14:00</span><span class="hour">15:00</span><span class="hour">16:00</span><span class="hour">17:00</span><span class="hour">18:00</span><span class="hour">19:00</span><span class="hour">20:00</span><span class="hour">21:00</span><span class="hour">22:00</span><span class="hour">23:00</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today">Today</th></tr></tfoot></table></div><div class="datetimepicker-days" style="display: block;"><table class=" table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-left"></i> </th><th colspan="5" class="switch">八月 2015</th><th class="next" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-right"></i> </th></tr><tr><th class="dow">日</th><th class="dow">一</th><th class="dow">二</th><th class="dow">三</th><th class="dow">四</th><th class="dow">五</th><th class="dow">六</th></tr></thead><tbody><tr><td class="day old">26</td><td class="day old">27</td><td class="day old">28</td><td class="day old">29</td><td class="day old">30</td><td class="day old">31</td><td class="day">1</td></tr><tr><td class="day">2</td><td class="day">3</td><td class="day active">4</td><td class="day">5</td><td class="day">6</td><td class="day">7</td><td class="day">8</td></tr><tr><td class="day">9</td><td class="day">10</td><td class="day">11</td><td class="day">12</td><td class="day">13</td><td class="day">14</td><td class="day">15</td></tr><tr><td class="day">16</td><td class="day">17</td><td class="day">18</td><td class="day">19</td><td class="day">20</td><td class="day">21</td><td class="day">22</td></tr><tr><td class="day">23</td><td class="day">24</td><td class="day">25</td><td class="day">26</td><td class="day">27</td><td class="day">28</td><td class="day">29</td></tr><tr><td class="day">30</td><td class="day">31</td><td class="day new">1</td><td class="day new">2</td><td class="day new">3</td><td class="day new">4</td><td class="day new">5</td></tr></tbody><tfoot><tr><th colspan="7" class="today">Today</th></tr></tfoot></table></div><div class="datetimepicker-months" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-left"></i> </th><th colspan="5" class="switch">2015</th><th class="next" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-right"></i> </th></tr></thead><tbody><tr><td colspan="7"><span class="month">一月</span><span class="month">二月</span><span class="month">三月</span><span class="month">四月</span><span class="month">五月</span><span class="month">六月</span><span class="month">七月</span><span class="month active">八月</span><span class="month">九月</span><span class="month">十月</span><span class="month">十一月</span><span class="month">十二月</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today">Today</th></tr></tfoot></table></div><div class="datetimepicker-years" style="display: none;"><table class="table-condensed"><thead><tr><th class="prev" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-left"></i> </th><th colspan="5" class="switch">2010-2019</th><th class="next" style="visibility: visible;"><i class="glyphicon glyphicon-arrow-right"></i> </th></tr></thead><tbody><tr><td colspan="7"><span class="year old">2009</span><span class="year">2010</span><span class="year">2011</span><span class="year">2012</span><span class="year">2013</span><span class="year">2014</span><span class="year active">2015</span><span class="year">2016</span><span class="year">2017</span><span class="year">2018</span><span class="year">2019</span><span class="year old">2020</span></td></tr></tbody><tfoot><tr><th colspan="7" class="today">Today</th></tr></tfoot></table></div></div></body></html>