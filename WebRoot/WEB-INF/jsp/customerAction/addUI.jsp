<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html lang="zh-cn">
<head>
<base href="<%=basePath%>"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="customer/favicon.ico">
<title>OpenPortalServer用户自助服务中心</title>
<link href="customer/css/bootstrap.min.css" rel="stylesheet">
<link href="customer/css/customer.css" rel="stylesheet">
<script src="customer/js/jquery-1.11.0.min.js"></script>
<script src="customer/js/bootstrap.min.js"></script>  
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]>
<script src="/customer/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="/customer/js/html5shiv.min.js"></script>
<script src="/customer/js/respond.min.js"></script>
<![endif]-->


</head>
<body>
<div class="navbar  navbar-inverse navbar-customer-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">OpenPortalServer用户自助服务中心</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="${pageContext.request.contextPath}/customer_login.action">OpenPortalServer用户自助服务中心</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="${pageContext.request.contextPath}/">首页</a></li>
        <li><a href="${pageContext.request.contextPath}/customer_loginUI.action">登陆</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-3">
    
<div class="sidebar-box">
    <div class="login">
        <p><small></small></p>
        <s:if test="accountAdd==0">
        <a class="btn btn-default btn-md" href="${pageContext.request.contextPath}/message_sendUI.action" role="button" target="_blank">立即注册</a> 
        </s:if>
        <s:else>
        <a class="btn btn-default btn-md" href="${pageContext.request.contextPath}/customer_addUI.action" role="button">立即注册</a> 
        </s:else>
        <p><small>已有账号请<a  href="${pageContext.request.contextPath}/customer_loginUI.action">登陆</a></small> <small><a href="${pageContext.request.contextPath}/message_sendUI.action" target="_blank">忘记密码</a></small></p>
    </div>
</div>

    
<div class="sidebar-box">
<h4 class="list-group-item"><span class="glyphicon glyphicon-comment"></span> 客服信息</h4>
<hr>
<div class="list-group">
  <a href="javascript:void();" class="list-group-item">客服QQ:25901875</a>   
  <a href="javascript:void();" class="list-group-item">客服电话:15692593456</a>   
</div>
</div>


    
<div class="sidebar-box">
  <h4 class="list-group-item"><span class="glyphicon glyphicon-qrcode"></span> OpenPortalServer</h4>
  <hr>
  <div class="img-center"><img src="customer/img/logo.jpg"></div>
</div>

</div>
        <div class="col-md-9">
<div class="panel panel-default">
    <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> 用户申请
    <div class="bar pull-right">
        <a href="javascript:history.go(-1);">返回上页</a>
    </div>
    </div>
        <div class="panel-body">
        <p id="alert" class="alert alert-warning">${msg }</p>
            <form class="form-horizontal" role="form" action="${pageContext.request.contextPath}/customer_add.action" method="post">
            
            
    
    <div class="form-group">
        <label class="col-sm-4 control-label" id="lab_loginName" for="loginName">用户登陆名</label>
        <div class="col-sm-6">
        <input name="loginName" placeholder="用户登陆名" value="" id="member_name" type="text" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label" id="lab_realname" for="realname">用户姓名</label>
        <div class="col-sm-6">
        <input name="name" placeholder="用户姓名" value="" id="realname" type="text" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label" id="lab_new_password" for="new_password">用户登陆密码</label>
        <div class="col-sm-6">
        <input name="password" placeholder="用户登陆密码(留空不修改), 长度必须为0到128" value="" id="new_password" type="password" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label" id="lab_gender" for="gender">性别</label>
        <div class="col-sm-6">
        <s:select name="gender" cssClass="form-control"
    list="#{1:'男',0:'女'}"  listKey="key" listValue="value"  headerKey="1" headerValue="请选择性别"/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label" id="lab_address" for="address">联系电话</label>
        <div class="col-sm-6">
        <input name="phoneNumber" placeholder="联系电话" value="" id="phoneNumber" type="text" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label" id="lab_email" for="email">电子邮箱</label>
        <div class="col-sm-6">
        <input name="email" placeholder="电子邮箱, 长度必须为0到128" value="" id="email" type="text" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label" id="lab_description" for="description">详细说明</label>
        <div class="col-sm-6">
        <s:textarea name="description" cssClass="form-control"></s:textarea>
        </div>
    </div>
    
<hr>
    <div class="form-group">
        <label class="col-sm-4 control-label" id="lab_submit" for="submit"></label>
        <div class="col-sm-6">
        <button name="submit" placeholder="submit" id="submit" type="submit" class="btn btn-primary"><b>提交</b></button>
        </div>
    </div>

        </form>
    </div>
</div>
</div>
    </div>
</div>

<div id="footer">
  <div class="container">
    <ul>
    </ul>
    <div class="copy">
        <p class="pull-left">© 2015 OpenPortalServer用户自助服务中心</p> 
    </div>
  </div>
</div>

   

</body></html>