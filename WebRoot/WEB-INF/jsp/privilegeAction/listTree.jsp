<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<html>
<head>
    <title>权限树状列表</title>
    <%@ include file="/WEB-INF/jsp/public/commons.jspf" %>
    <style type="text/css">
    	.disabled{
    		color: gray;
    		cursor: pointer;
    	}
    </style>
</head>
<body>

<div id="Title_bar">
    <div id="Title_bar_Head">
        <div id="Title_Head"></div>
        <div id="Title"><!--页面标题-->
            <img border="0" width="13" height="13" src="${pageContext.request.contextPath}/style/images/title_arrow.gif"/> 权限树状列表
        </div>
        <div id="Title_End"></div>
    </div>
</div>

<div id="MainArea">
    <table cellspacing="0" cellpadding="0" class="TableStyle">
       
        <!-- 表头-->
        <thead>
            <tr align="CENTER" valign="MIDDLE" id="TableTitle">
            	<td width="50px">编号</td>
            	<td width="250px">权限名称</td>
                <td width="300px">访问地址</td>
                <td>相关操作</td>
            </tr>
        </thead>

		<!--显示数据列表-->
        <tbody id="TableData" class="dataContainer" datakey="forumList">
        
        <s:iterator value="#privilegeList" status="status">
			<tr class="TableDetail1 template">
				<td>${id}&nbsp;</td>
				<td>${name}&nbsp;</td>
				<td>${url}&nbsp;</td>
				<td>
					<s:a action="privilege_delete?id=%{id}" onclick="return delConfirm()">删除</s:a>
					<s:a action="privilege_editUI?id=%{id}">修改</s:a>
					
					<!-- 最上面的不能上移 -->
					<s:if test="#status.first">
						<span class="disabled">上移</span>
					</s:if>
					<s:else>
						<s:if test="getParent()==null">
						<s:a action="privilege_moveUp?id=%{id}&parentId=">上移</s:a>
						</s:if>
						<s:else>
						<s:a action="privilege_moveUp?id=%{id}&parentId=%{getParent().getId()}">上移</s:a>
						</s:else>
					</s:else>
					
					<!-- 最下面的不能下移 -->
					<s:if test="#status.last">
						<span class="disabled">下移</span>
					</s:if>
					<s:else>
						<s:if test="getParent()==null">
						<s:a action="privilege_moveDown?id=%{id}&parentId=">下移</s:a>
						</s:if>
						<s:else>
						<s:a action="privilege_moveDown?id=%{id}&parentId=%{getParent().getId()}">下移</s:a>
						</s:else>
					</s:else>
					&nbsp;&nbsp;&nbsp;&nbsp;<s:a action="privilege_addUI?parentId=%{id}">添加子权限</s:a>
				</td>
			</tr>
        </s:iterator>

        </tbody>
    </table>
    
    <!-- 其他功能超链接 -->
    <div id="TableTail">
        <div id="TableTail_inside">
            <s:a action="privilege_addUI"><img src="${pageContext.request.contextPath}/style/images/createNew.png" /></s:a>
        </div>
    </div>
</div>

<div class="Description">
	说明：<br />
	1，显示的列表按其位置排列。<br />
	2，可以通过上移与下移功能调整顺序。最上面的不能上移，最下面的不能下移。<br />
</div>

</body>
</html>
