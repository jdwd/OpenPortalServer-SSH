<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>权限管理</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript">
    KE.show({
        id : 'content7',
        cssPath : './index.css'
    });
  </script>
  
<script type="text/javascript">
$(document).ready(function(e) {
    $(".select1").uedSelect({
		width : 345			  
	});
	$(".select2").uedSelect({
		width : 167  
	});
	$(".select3").uedSelect({
		width : 100
	});
});
</script>
<script type="text/javascript">
		$(function(){
			// 指定事件处理函数
			$("[name=choose]").click(function(){
				
				// 当选中或取消一个权限时，也同时选中或取消所有的下级权限
				$(this).siblings("tr").find("input").attr("checked", this.checked);
				
				// 当选中一个权限时，也要选中所有的直接上级权限
				if(this.checked == true){
					$(this).parents("table").children("th").children("input").attr("checked", true);
				}
				
			});
		});
	</script>


</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/home_right.action">首页</a></li>
    <li><a href="${pageContext.request.contextPath}/privilege_list.action">权限管理</a></li>
    <li><a href="${pageContext.request.contextPath}/privilege_list.action">权限列表</a></li>
    </ul>
    </div>
    
    <div class="formbody">
   
    <div id="tab1" class="tabson">
        
    <s:form action="privilege_list">   
    <ul class="seachform">
    
    <li><label>编号</label>
    <s:textfield  name="id" cssClass="scinput"/></li>
    <li><label>权限名称</label>
    <s:textfield  name="name" cssClass="scinput"/></li>
    
    <li><label>上级权限名称</label> 
    <div class="vocation">
    <s:select list="privilegeList"  cssClass="select2" 
                        listKey="id" listValue="name" 
                        headerKey="" headerValue="请选择上级权限" 
                        name="parentId" cssclass="SelectStyle"/>
   </div></li>
    
    
    <li><label>权限说明</label>  
    <s:textfield  name="description" cssClass="scinput"/></li>
    
    <li><label>&nbsp;</label>
    <s:submit cssClass="scbtn" value="查询" name=""/>
    </ul>
    </s:form>
    
    <s:form action="privilege_deleteChoose">
    <div class="tools">
    
    	<ul class="toolbar">
        <s:a action="privilege_addUI?parentId=%{parentId}"><li><span><img src="images/t01.png" /></span>添加</li></s:a>
        <s:if test="#session.user.hasPrivilegeByUrl('/privilege_delete')">
        <s:a href="javascript: gotoDelete()"  onclick="return window.confirm('您确定要删除已选择的权限吗？')"><li><span><img src="images/t03.png" /></span>删除</li></s:a>
        </s:if>
        
        <li><a href="${pageContext.request.contextPath}/privilege_listView.action"><span><img src="images/t04.png" /></span>树状列表</a></li>
        </ul>
        
        
        <ul class="toolbar1">
        <li><a href="${pageContext.request.contextPath}/privilege_list.action"><span><img src="images/t05.png" /></span>刷新</a></li>
        </ul>
    
    </div>
    <table class="tablelist">
    	<thead>
    	<tr>
        <th><input name="chooseAll" type="checkbox" onClick="$('[name=choose]').attr('checked', this.checked)"/></th>
        <th>编号</th>
        <th>权限名称</th>
        <th>上级权限名称</th>
        <th>访问地址</th>
        <th>相关操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="recordList" status="status">
        <tr>
        <td><input name="choose" type="checkbox" value="${id}" <s:property value="%{id in choose ? 'checked' : ''}"/>/></td>
        <td>${id}</td>
        <td title="点击查看下级权限"><s:a action="privilege_list?parentId=%{id}">${name}</s:a></td>
        
        <s:if test="%{#parent.parent!=null}">
        <td title="点击查看上级权限"><s:a action="privilege_list?parentId=%{#parent.parent.Id}">${parent.name}</s:a></td>
        </s:if>
        <s:else>
        <td title="点击查看顶级权限"><s:a action="privilege_list">${parent.name}</s:a></td>
        </s:else>
        
        <td>${url}</td>
       
        <td><s:a cssClass="tablelink" action="privilege_addUI?parentId=%{id}">添加子权限</s:a>     <s:a cssClass="tablelink" action="privilege_editUI?id=%{id}">修改</s:a>     <s:a cssClass="tablelink" action="privilege_delete?id=%{id}" onclick="return window.confirm('您确定要删除该权限吗？这将删除所有的下级权限，您确定要删除吗？')">删除</s:a></td>
        </tr> 
        </s:iterator>
                
        </tbody>
    </table>
    </s:form>
    </div>
   
    <div class="pagin">
    	<div class="message">共&nbsp;<i class="blue">${recordCount }</i>&nbsp;条记录</div>
        
    </div>
    
    <script type="text/javascript">
	
	
	function gotoDelete(){
		document.forms[1].submit();
	}
</script>
    
   
    
    
    <script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    </div>
    

</body>

</html>
