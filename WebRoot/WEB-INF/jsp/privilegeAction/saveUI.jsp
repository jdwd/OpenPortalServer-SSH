<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>权限添加</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>

<script type="text/javascript">
    KE.show({
        id : 'content7',
        cssPath : './index.css'
    });
  </script>
  
<script type="text/javascript">
$(document).ready(function(e) {
    $(".select1").uedSelect({
		width : 345			  
	});
	$(".select2").uedSelect({
		width : 167  
	});
	$(".select3").uedSelect({
		width : 100
	});
});
</script>
</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/home_right.action">首页</a></li>
    <li><a href="${pageContext.request.contextPath}/privilege_list.action">权限管理</a></li>
    <li><a href="${pageContext.request.contextPath}/privilege_addUI.action">权限添加</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
    <li><a href="#tab1" class="selected">角色添加</a></li>
  	</ul>
    </div> 
    
  	<div id="tab1" class="tabson">
    
    <div class="formtext"><b>本页面为角色添加页面！</b></div>
    
    <s:form action="privilege_%{id == null ? 'add' : 'edit'}">
    	<s:hidden name="id"></s:hidden>   	
    
    <ul class="forminfo">
    
    <li><label>上级权限<b>*</b></label>
    <div class="vocation">
    <s:select list="privilegeList"  cssClass="select1" 
                        listKey="id" listValue="name" 
                        headerKey="" headerValue="请选择上级权限" 
                        name="parentId" cssclass="SelectStyle"/>
   </div>
    【不选择则为顶级权限】</li>
    
    <li><label>权限名称<b>*</b></label>
    <s:textfield  name="name" cssClass="dfinput" style="width:400px;"/></li>
   
    
    
    <li><label>访问地址<b>*</b></label>
    <s:textarea name="url" cssClass="textinput"></s:textarea></li>
    
    
    
    
    
    <li><label>&nbsp;</label>
    <s:submit cssClass="btn" value="保存" name="保存"></s:submit></li>
    </ul>
    </s:form>
    </div> 
    
    
    </div> 
 
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    
    
    
    </div>


</body>

</html>