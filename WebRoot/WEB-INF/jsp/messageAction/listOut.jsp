<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>消息列表</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript">
    KE.show({
        id : 'content7',
        cssPath : './index.css'
    });
  </script>
  
<script type="text/javascript">
$(document).ready(function(e) {
    $(".select1").uedSelect({
		width : 345			  
	});
	$(".select2").uedSelect({
		width : 167  
	});
	$(".select3").uedSelect({
		width : 100
	});
});
</script>
<script type="text/javascript">
		$(function(){
			// 指定事件处理函数
			$("[name=choose]").click(function(){
				
				// 当选中或取消一个权限时，也同时选中或取消所有的下级权限
				$(this).siblings("tr").find("input").attr("checked", this.checked);
				
				// 当选中一个权限时，也要选中所有的直接上级权限
				if(this.checked == true){
					$(this).parents("table").children("th").children("input").attr("checked", true);
				}
				
			});
		});
	</script>


</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="${pageContext.request.contextPath}/home_right.action">首页</a></li>
    <li><a href="${pageContext.request.contextPath}/message_listOut.action">发件箱</a></li>
    </ul>
    </div>
    
    <div class="formbody">
   
    <div id="tab1" class="tabson">
        
    <s:form action="message_listOut">   
    <ul class="seachform">
    
    <li><label>编号</label>
    <s:textfield  name="id" cssClass="scinput"/></li>
    
    <li><label>标题</label>  
    <s:textfield  name="title" cssClass="scinput"/></li>
    
    <li><label>内容</label>  
    <s:textfield  name="description" cssClass="scinput"/></li>
    
    <li><label>状态</label>  
    <div class="vocation">
    <s:select name="state" cssClass="select3" 
    list="#{1:'未读',0:'已读'}"  listKey="key" listValue="value"  headerKey="" headerValue="请选择状态"/>
    </div>
    </li>
    
    <li><label>&nbsp;</label>
    <s:submit cssClass="scbtn" value="查询" name=""/>
    </ul>
    </s:form>
    
    <s:form action="message_deleteChooseOut">
    <div class="tools">
    
    	<ul class="toolbar">
        <s:a action="message_addUI"><li><span><img src="images/t01.png" /></span>发送新消息</li></s:a>
        <s:if test="#session.user.hasPrivilegeByUrl('/message_deleteOut')">
        <s:a href="javascript: gotoDelete()" onclick="return window.confirm('您确定要删除已选择的消息吗？')"><li><span><img src="images/t03.png" /></span>删除</li></s:a>
        </s:if>
        
        <li><a href="${pageContext.request.contextPath}/message_listOut.action"><span><img src="images/t04.png" /></span>统计</a></li>
        </ul>
        
        
        <ul class="toolbar1">
        <li><a href="${pageContext.request.contextPath}/message_listOut.action"><span><img src="images/t05.png" /></span>刷新</a></li>
        </ul>
    
    </div>
    <table class="tablelist">
    	<thead>
    	<tr>
        <th><input name="chooseAll" type="checkbox" onClick="$('[name=choose]').attr('checked', this.checked)"/></th>
        <th>编号<i class="sort"><img src="images/px.gif" /></i></th>
        
        <th>标题</th>
        <th>内容</th>
        <th>发送时间</th>
        <th>发送给</th>
        <th>状态</th>
        <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <s:iterator value="recordList">
        <tr>
        <td><input name="choose" type="checkbox" value="${id}" <s:property value="%{id in choose ? 'checked' : ''}"/>/></td>
        <td>${id}</td>
        
       
        <td>${title}</td>
        <td>${description}</td>
        <td>${date}</td>
        <td>${toname}</td>
        <td><s:property value="state==1?'未读':'已读'"/></td>
        <td><s:a cssClass="tablelink" action="message_deleteOut?id=%{id}" onclick="return window.confirm('您确定要删除该消息吗？')">删除</s:a></td>
        </tr> 
        </s:iterator>
                
        </tbody>
    </table>
    </s:form>
    </div>
   
    <div class="pagin">
    	<div class="message">共&nbsp;<i class="blue">${recordCount }</i>&nbsp;条记录，当前显示第&nbsp;<i class="blue">${currentPage }</i>&nbsp;页,共&nbsp;<i class="blue">${pageCount }</i>&nbsp;页,每页显示&nbsp;<i class="blue">${pageSize }</i>&nbsp;条记录</div>
        <ul class="paginList">
        <li class="paginItem"><a href="javascript: gotoPage(1)" title="首页">首</a></li>
        <li class="paginItem"><a href="javascript: gotoPage(
        <s:if test="1 == currentPage">
				${currentPage}
			</s:if>
			<s:else>
				${currentPage}-1
			</s:else>
        )"><span class="pagepre"></span></a></li>
        
        <s:iterator begin="%{beginPageIndex}" end="%{endPageIndex}" var="num">
			<s:if test="#num == currentPage"> <%-- 当前页 --%>
				 <li class="paginItem current"><a href="#">${num}</a></li>
			</s:if>
			<s:else> <%-- 非当前页 --%>
				<li class="paginItem"><a href="javascript: gotoPage(${num})">${num}</a></li>
			</s:else>
		</s:iterator>
		
		<li class="paginItem"><a href="javascript: gotoPage(
        <s:if test="pageCount == currentPage">
				${currentPage}
			</s:if>
			<s:else>
				${currentPage}+1
			</s:else>
        )"><span class="pagenxt"></span></a></li>
        <li class="paginItem"><a href="javascript: gotoPage(${pageCount})" title="最后一页">尾</a></li>
        </ul>
    </div>
    
    <script type="text/javascript">
	function gotoPage( pageNum ){
		// window.location.href = "forum_show.action?id=${id}&pageNum=" + pageNum;
		
		$(document.forms[0]).append("<input type='hidden' name='pageNum' value='" + pageNum +"'>");
		document.forms[0].submit();
	}
	
	function gotoDelete(){
		document.forms[1].submit();
	}
</script>
    
   
    
    
    <script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    </div>
    

</body>

</html>
