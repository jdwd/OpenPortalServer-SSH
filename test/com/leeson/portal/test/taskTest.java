/*  
* Copyright (c) 2015-2026 LeeSon Ltd. All Rights Reserved.  
* LeeSon  QQ:25901875
*/ 
package com.leeson.portal.test;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class taskTest {
	public static void main(String[] args) {
	    Runnable runnable = new Runnable() {
	      public void run() {
	        // task to run goes here
	        System.out.println("Hello !!"+new Date());
	      }
	    };
	    ScheduledExecutorService service = Executors
	                    .newSingleThreadScheduledExecutor();
	    service.scheduleAtFixedRate(runnable, 0, 2, TimeUnit.SECONDS);
	  }
}
